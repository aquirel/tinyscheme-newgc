# Makefile for TinyScheme
# Time-stamp: <2002-06-24 14:13:27 gildea>

# Windows/2000
#CC = cl -nologo
#DEBUG= -W3 -Z7 -MD
#DL_FLAGS=
#SYS_LIBS=
#Osuf=obj
#SOsuf=dll
#LIBsuf=.lib
#EXE_EXT=.exe
#LD = link -nologo
#LDFLAGS = -debug -map -dll -incremental:no
#LIBPREFIX =
#OUT = -out:$@
#RM= -del
#AR= echo

# Unix, generally
CC = gcc
DEBUG=-fpic -std=gnu11 -pthread -Wall -Wextra -fdiagnostics-color=always -Wno-char-subscripts -Wno-unused-label -D_GNU_SOURCE -D_MULTI_THREADED -DSYNCHRONIZED_TRACING
Osuf=o
SOsuf=so
LIBsuf=a
EXE_EXT=
LIBPREFIX=lib
OUT = -o $@
RM= -rm -f
AR= ar crs

ifdef RELEASE
DEBUG := $(DEBUG) -O3 -DNDEBUG -DNO_DEBUG
else
ifdef PROFILE
DEBUG := $(DEBUG) -g3 -O0 -DNDEBUG -DNO_DEBUG
else
DEBUG := $(DEBUG) -g3 -O0
endif
endif

# Linux
LD = gcc
LDFLAGS = -shared -fpic
#DEBUG=-g -Wall -Wno-char-subscripts -O0
SYS_LIBS= -ldl -lm
#PLATFORM_FEATURES= -DSUN_DL=1
PLATFORM_FEATURES=

# Cygwin
#PLATFORM_FEATURES = -DUSE_STRLWR=0

# MinGW/MSYS
#SOsuf=dll
#PLATFORM_FEATURES = -DUSE_STRLWR=0

# Mac OS X
#LD = gcc
#LDFLAGS = --dynamiclib
#DEBUG=-g -Wno-char-subscripts -O
#SYS_LIBS= -ldl
#PLATFORM_FEATURES= -DUSE_STRLWR=1 -D__APPLE__=1 -DOSX=1


# Solaris
#SYS_LIBS= -ldl -lc
#Osuf=o
#SOsuf=so
#EXE_EXT=
#LD = ld
#LDFLAGS = -G -Bsymbolic -z text
#LIBPREFIX = lib
#OUT = -o $@

FEATURES = $(PLATFORM_FEATURES) -DUSE_DL=1 -DUSE_MATH=1 -DUSE_ASCII_NAMES=1 -DUSE_TRACING

OBJS = scheme.$(Osuf) dynload.$(Osuf)

LIBTARGET = $(LIBPREFIX)tinyscheme.$(SOsuf)
STATICLIBTARGET = $(LIBPREFIX)tinyscheme.$(LIBsuf)

all: $(LIBTARGET) $(STATICLIBTARGET) scheme$(EXE_EXT)

%.$(Osuf): %.c
	$(CC) -I. -c $(DEBUG) $(FEATURES) $(DL_FLAGS) $<

$(LIBTARGET): $(OBJS) gc/libgc.a
	$(LD) $(LDFLAGS) $(OUT) $(OBJS) $(SYS_LIBS) gc/libgc.a

scheme$(EXE_EXT): $(OBJS) gc/libgc.a
	$(CC) -o $@ $(DEBUG) $(OBJS) $(SYS_LIBS) gc/libgc.a

$(STATICLIBTARGET): $(OBJS) gc/libgc.a
	$(AR) $@ $(OBJS)

$(OBJS): scheme.h scheme-private.h opdefines.h
dynload.$(Osuf): dynload.h

clean:
	$(RM) $(OBJS) $(LIBTARGET) $(STATICLIBTARGET) scheme$(EXE_EXT)
	$(RM) tinyscheme.ilk tinyscheme.map tinyscheme.pdb tinyscheme.exp
	$(RM) scheme.ilk scheme.map scheme.pdb scheme.lib scheme.exp
	$(RM) *~

TAGS_SRCS = scheme.h scheme.c dynload.h dynload.c

tags: TAGS
TAGS: $(TAGS_SRCS)
	etags $(TAGS_SRCS)
