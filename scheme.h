/* SCHEME.H */

#ifndef _SCHEME_H
#define _SCHEME_H

#include <stdio.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*
 * Default values for #define'd symbols
 */

#ifndef _MSC_VER
#define USE_STRCASECMP 1
#ifndef USE_STRLWR
#define USE_STRLWR 1
#endif
#define SCHEME_EXPORT
#else
#define USE_STRCASECMP 0
#define USE_STRLWR 0
#ifdef _SCHEME_SOURCE
#define SCHEME_EXPORT __declspec(dllexport)
#else
#define SCHEME_EXPORT __declspec(dllimport)
#endif
#endif

#if USE_NO_FEATURES
#define USE_MATH 0
#define USE_CHAR_CLASSIFIERS 0
#define USE_ASCII_NAMES 0
#define USE_STRING_PORTS 0
#define USE_ERROR_HOOK 0
#define USE_TRACING 0
#define USE_COLON_HOOK 0
#define USE_DL 0
#define USE_PLIST 0
#endif

#if USE_DL
#define USE_INTERFACE 1
#endif


#ifndef USE_MATH                /* If math support is needed */
#define USE_MATH 1
#endif

#ifndef USE_CHAR_CLASSIFIERS    /* If char classifiers are needed */
#define USE_CHAR_CLASSIFIERS 1
#endif

#ifndef USE_ASCII_NAMES         /* If extended escaped characters are needed */
#define USE_ASCII_NAMES 1
#endif

#ifndef USE_STRING_PORTS        /* Enable string ports */
#define USE_STRING_PORTS 1
#endif

#ifndef USE_TRACING
#define USE_TRACING 1
#endif

#ifndef USE_PLIST
#define USE_PLIST 0
#endif

/* To force system errors through user-defined error handling (see *error-hook*) */
#ifndef USE_ERROR_HOOK
#define USE_ERROR_HOOK 1
#endif

#ifndef USE_COLON_HOOK          /* Enable qualified qualifier */
#define USE_COLON_HOOK 1
#endif

#ifndef USE_STRCASECMP          /* stricmp for Unix */
#define USE_STRCASECMP 0
#endif

#ifndef USE_STRLWR
#define USE_STRLWR 1
#endif

#ifndef STDIO_ADDS_CR           /* Define if DOS/Windows */
#define STDIO_ADDS_CR 0
#endif

#ifndef INLINE
#define INLINE
#endif

#ifndef USE_INTERFACE
#define USE_INTERFACE 0
#endif

#ifndef SHOW_ERROR_LINE         /* Show error line in file */
#define SHOW_ERROR_LINE 1
#endif

typedef struct scheme scheme;
typedef struct ObjectHeader *pointer;
typedef struct GC GC;

typedef void *(*func_alloc)(size_t);
typedef void (*func_dealloc)(void *);

/* num, for generic arithmetic */
typedef struct num
{
    char is_fixnum;
    union
    {
        long ivalue;
        double rvalue;
    } value;
} num;

SCHEME_EXPORT bool scheme_init_new(GC *gc, func_alloc malloc_handler, func_dealloc free_handler, pointer *scheme_object_out);
SCHEME_EXPORT void scheme_deinit(pointer scheme_object);
void scheme_set_input_port_file(pointer scheme_object, FILE *fin);
void scheme_set_input_port_string(pointer scheme_object, char *start, char *past_the_end);
SCHEME_EXPORT void scheme_set_output_port_file(pointer scheme_object, FILE *fout);
void scheme_set_output_port_string(pointer scheme_object, char *start, char *past_the_end);
SCHEME_EXPORT void scheme_load_file(pointer scheme_object, FILE *fin);
SCHEME_EXPORT void scheme_load_named_file(pointer _scheme_object, FILE *fin, const char *filename);
SCHEME_EXPORT void scheme_load_string(pointer _scheme_object, const char *cmd);
SCHEME_EXPORT pointer scheme_apply0(pointer scheme_object, const char *procname);
SCHEME_EXPORT pointer scheme_call(pointer _scheme_object, pointer _func, pointer _args);
SCHEME_EXPORT pointer scheme_eval(pointer _scheme_object, pointer _obj);
void scheme_set_external_data(scheme *sc, void *p);
SCHEME_EXPORT void scheme_define(pointer scheme_object, pointer env, pointer symbol, pointer value);

typedef pointer(*foreign_func) (pointer, pointer);

pointer mk_integer(GC *gc, long num);
pointer mk_real(GC *gc, double num);
pointer mk_symbol(pointer scheme_object, const char *name);
pointer gensym(pointer scheme_object);
pointer mk_string(pointer scheme_object, const char *str);
pointer mk_counted_string(pointer _scheme_object, const char *str, int len);
pointer mk_empty_string(pointer _scheme_object, int len, char fill);
pointer mk_character(GC *gc, int c);
pointer mk_foreign_func(GC *gc, foreign_func f);
void putstr(pointer scheme_object, const char *s);
int list_length(pointer scheme_object, pointer a);

#if defined(NO_DEBUG)
int eqv(pointer a, pointer b); // Safe.
#define eqv(gc, a, b) eqv(a, b)
#else
int eqv(GC *gc, pointer a, pointer b); // Safe.
#endif // defined(NO_DEBUG).


#if USE_INTERFACE
struct scheme_interface
{
    void (*scheme_define) (pointer scheme_object, pointer env, pointer symbol, pointer value);
    pointer(*cons) (pointer scheme_object, pointer a, pointer b);
    pointer(*immutable_cons) (pointer scheme_object, pointer a, pointer b);
    pointer(*mk_integer) (GC *gc, long num);
    pointer(*mk_real) (GC *gc, double num);
    pointer(*mk_symbol) (pointer scheme_object, const char *name);
    pointer(*gensym) (pointer scheme_object);
    pointer(*mk_string) (pointer scheme_object, const char *str);
    pointer(*mk_counted_string) (pointer _scheme_object, const char *str, int len);
    pointer(*mk_character) (GC *gc, int c);
    pointer(*mk_vector) (pointer scheme_object, int len);
    pointer(*mk_foreign_func) (GC *gc, foreign_func f);
    void (*putstr) (pointer scheme_object, const char *s);
    void (*putcharacter) (pointer scheme_object, int c);

    #if defined(NO_DEBUG)
    int (*is_string) (pointer p);
    #else
    int (*is_string) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    char *(*string_value) (pointer p);

    #if defined(NO_DEBUG)
    int (*is_number) (pointer p);
    #else
    int (*is_number) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    num(*nvalue) (pointer p);

    #if defined(NO_DEBUG)
    long (*ivalue) (pointer p);
    double (*rvalue) (pointer p);
    #else
    long (*ivalue) (GC *gc, pointer p);
    double (*rvalue) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    #if defined(NO_DEBUG)
    int (*is_integer) (pointer p);
    int (*is_real) (pointer p);
    int (*is_character) (pointer p);
    #else
    int (*is_integer) (GC *gc, pointer p);
    int (*is_real) (GC *gc, pointer p);
    int (*is_character) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    long (*charvalue) (pointer p);
    int (*is_list) (pointer scheme_object, pointer p);

    #if defined(NO_DEBUG)
    int (*is_vector) (pointer p);
    #else
    int (*is_vector) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    int (*list_length) (pointer scheme_object, pointer vec);
    long (*vector_length) (pointer vec);
    void (*fill_vector) (GC *gc, pointer vec, pointer obj);

    #if defined(NO_DEBUG)
    pointer(*vector_elem) (pointer vec, int ielem);
    #else
    pointer(*vector_elem) (GC *gc, pointer vec, int ielem);
    #endif // defined(NO_DEBUG).

    pointer(*set_vector_elem) (GC *gc, pointer vec, int ielem, pointer newel);

    #if defined(NO_DEBUG)
    int (*is_port) (pointer p);
    int (*is_pair) (pointer p);
    #else
    int (*is_port) (GC *gc, pointer p);
    int (*is_pair) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    #if defined(NO_DEBUG)
    pointer(*pair_car) (pointer p);
    pointer(*pair_cdr) (pointer p);
    #else
    pointer(*pair_car) (GC *gc, pointer p);
    pointer(*pair_cdr) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    pointer(*set_car) (GC *gc, pointer p, pointer q);
    pointer(*set_cdr) (GC *gc, pointer p, pointer q);

    #if defined(NO_DEBUG)
    int (*is_symbol) (pointer p);
    #else
    int (*is_symbol) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    #if defined(NO_DEBUG)
    char *(*symname) (pointer p);
    #else
    char *(*symname) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    int (*is_syntax) (pointer p);
    int (*is_proc) (pointer p);
    int (*is_foreign) (pointer p);

    #if defined(NO_DEBUG)
    char *(*syntaxname) (pointer p);
    #else
    char *(*syntaxname) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    int (*is_closure) (pointer p);
    int (*is_macro) (pointer p);

    #if defined(NO_DEBUG)
    pointer(*closure_code) (pointer p);
    pointer(*closure_env) (pointer p);
    #else
    pointer(*closure_code) (GC *gc, pointer p);
    pointer(*closure_env) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    int (*is_continuation) (pointer p);
    int (*is_promise) (pointer p);

    #if defined(NO_DEBUG)
    int (*is_environment) (pointer p);
    #else
    int (*is_environment) (GC *gc, pointer p);
    #endif // defined(NO_DEBUG).

    int (*is_immutable) (pointer p);
    void (*setimmutable) (GC *gc, pointer p);
    void (*load_file) (pointer scheme_object, FILE * fin);
    void (*load_string) (pointer scheme_object, const char *input);
};
#endif

typedef struct scheme_registerable
{
    foreign_func f;
    const char *name;
}
scheme_registerable;

void scheme_register_foreign_func_list(pointer scheme_object, scheme_registerable *list, int n);

#ifdef __cplusplus
}
#endif
#endif
