/* T I N Y S C H E M E    1 . 4 1
 *   Dimitrios Souflis (dsouflis@acm.org)
 *   Based on MiniScheme (original credits follow)
 * (MINISCM)               coded by Atsushi Moriwaki (11/5/1989)
 * (MINISCM)           E-MAIL :  moriwaki@kurims.kurims.kyoto-u.ac.jp
 * (MINISCM) This version has been modified by R.C. Secrist.
 * (MINISCM)
 * (MINISCM) Mini-Scheme is now maintained by Akira KIDA.
 * (MINISCM)
 * (MINISCM) This is a revised and modified version by Akira KIDA.
 * (MINISCM)    current version is 0.85k4 (15 May 1994)
 */

#define _SCHEME_SOURCE
#define GC_EXPORT_INTERNALS
#include "gc/gc.h"
#include "gc/debug.h"
#include "scheme-private.h"
#ifndef WIN32
#include <unistd.h>
#include <signal.h>
#endif
#ifdef WIN32
#define snprintf _snprintf
#endif
#if USE_DL
#include "dynload.h"
#endif
#if USE_MATH
#include <math.h>
#endif

#include <limits.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <ctype.h>
#include <pthread.h>

#if USE_STRCASECMP
#include <strings.h>
#ifndef __APPLE__
#define stricmp strcasecmp
#endif
#endif

// Scoping support.
#define __scope(type, initial_value, initialization, initializer_body, finalizer_body, ...) \
{ \
    __label__ __scope_exit; \
    enum { __scope_is_finalized, ## __VA_ARGS__, __scope_size }; \
    type __scope_objects[__scope_size] = { initial_value }; \
    initialization; \
    inline void __scope_object_initializer(type *object) \
    { \
        initializer_body; \
    } \
    inline void __scope_object_finalizer(type *object) \
    { \
        finalizer_body; \
    } \
    inline void __scope_finalizer() \
    { \
        bool *scope_is_finalized = (bool *) &local(__scope_is_finalized); \
        if (!(*scope_is_finalized)) \
        { \
            for (size_t i = 1; i < __scope_size; ++i) \
            { \
                __scope_object_finalizer(&local(i)); \
            } \
            *scope_is_finalized = true; \
        } \
    } \
    for (size_t i = 1; i < __scope_size; ++i) \
    { \
        __scope_object_initializer(&local(i)); \
    } \
    (void) (0);

#define local(object) __scope_objects[object]

#define endscope \
    __scope_exit: \
    __scope_finalizer(); \
     (void) (0); \
}

#define scope_break \
    __scope_finalizer(); \
    break

#define scope_continue \
    __scope_finalizer(); \
    continue

#define scope_return \
    __scope_finalizer(); \
    return

#define scope_leave \
    __scope_finalizer(); \
    goto __scope_exit

#define scope_finalize \
    __scope_finalizer()

#define __vm_scope_initialization(scheme_object, sc, local_gc_name) \
    local(scheme_object) = _scheme_object; \
    scheme *sc = *((scheme **) object_get_byte(local(scheme_object), 0)); \
    GC *local_gc_name = sc->gc; \
    inline pointer get(pointer object, size_t slot_index) \
    { \
        return object_get_slot(local_gc_name, object, slot_index); \
    } \
    inline pointer set(pointer object, size_t slot_index, pointer value) \
    { \
        return object_set_slot(local_gc_name, object, slot_index, value); \
    } \
    inline void *get_byte(pointer object, size_t byte_index) \
    { \
        return object_get_byte(object, byte_index); \
    }

#define __vm_scope_initializer_body(gc) \
    gc_track_reference(gc, object, REFERENCE_TRACKING_MODE_HANDLE)

#define __vm_scope_finalizer_body(gc) \
    gc_untrack_reference(gc, object)

#define scope(scheme_object, sc, gc, ...) \
    __scope(pointer, \
            NULL, \
            __vm_scope_initialization(scheme_object, sc, gc), \
            __vm_scope_initializer_body(gc), \
            __vm_scope_finalizer_body(gc), \
            scheme_object, \
            ## __VA_ARGS__)

// Intelligent slots.
typedef struct __tracking_slot
{
    pointer *object;
    size_t slot_index;
} __tracking_slot;

#define defslot(object, slot_index, name) __tracking_slot name = { &object, slot_index }
#define slot(name) object_get_slot(gc, *name.object, name.slot_index)
#define setslot(name, value) set(*name.object, name.slot_index, value)

/* Used for documentation purposes, to signal functions in 'interface' */
#define INTERFACE

#define TOK_EOF         (-1)
#define TOK_LPAREN      0
#define TOK_RPAREN      1
#define TOK_DOT         2
#define TOK_ATOM        3
#define TOK_QUOTE       4
#define TOK_COMMENT     5
#define TOK_DQUOTE      6
#define TOK_BQUOTE      7
#define TOK_COMMA       8
#define TOK_ATMARK      9
#define TOK_SHARP       10
#define TOK_SHARP_CONST 11
#define TOK_VEC         12

#define BACKQUOTE '`'
#define DELIMITERS  "()\";\f\t\v\n\r "

/*
 *  Basic memory allocation units
 */

#define banner "TinyScheme 1.41"

#include <string.h>
#include <stdlib.h>

#ifdef __APPLE__
static int stricmp(const char *s1, const char *s2)
{
    unsigned char c1, c2;

    do
    {
        c1 = tolower(*s1);
        c2 = tolower(*s2);
        if (c1 < c2)
            return -1;
        else if (c1 > c2)
            return 1;
        s1++, s2++;
    } while (c1 != 0);
    return 0;
}
#endif /* __APPLE__ */

#if USE_STRLWR
static const char *strlwr(char *s)
{
    const char *p = s;

    while (*s)
    {
        *s = tolower(*s);
        s++;
    }
    return p;
}
#endif

#ifndef prompt
#define prompt "ts> "
#endif

#ifndef InitFile
#define InitFile "init.scm"
#endif

enum scheme_types
{
    T_STRING           = 1,
    T_NUMBER           = 2,
    T_SYMBOL           = 3,
    T_PROC             = 4,
    T_PAIR             = 5,
    T_CLOSURE          = 6,
    T_CONTINUATION     = 7,
    T_FOREIGN          = 8,
    T_CHARACTER        = 9,
    T_PORT             = 10,
    T_VECTOR           = 11,
    T_MACRO            = 12,
    T_PROMISE          = 13,
    T_ENVIRONMENT      = 14,
    T_MUTEX            = 15,
    T_SCHEME           = 16,
    T_LAST_SYSTEM_TYPE = 16
};

// ObjectHeader has 8 bits for flags.
#define T_MASKTYPE  31 /* 00011111 */
#define T_SYNTAX    32 /* 00100000 */
#define T_IMMUTABLE 64 /* 01000000 */

static num num_add(num a, num b);
static num num_mul(num a, num b);
static num num_div(num a, num b);
static num num_intdiv(num a, num b);
static num num_sub(num a, num b);
static num num_rem(num a, num b);
static num num_mod(num a, num b);
static int num_eq(num a, num b);
static int num_gt(num a, num b);
static int num_ge(num a, num b);
static int num_lt(num a, num b);
static int num_le(num a, num b);

#if USE_MATH
static double round_per_R5RS(double x);
#endif
static int is_zero_double(double x);

#if defined(NO_DEBUG)
static INLINE int num_is_integer(pointer p) // Safe.
#define num_is_integer(gc, p) num_is_integer(p)
#else
static INLINE int num_is_integer(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    if (NULL != p && gc_object_get_size_in_bytes(gc, p) >= (OBJECT_SLOT_SIZE + sizeof(num)))
    {
        num *n = object_get_byte(p, 0);
        return n->is_fixnum;
    }

    return false;
}

#if defined(NO_DEBUG)
static INLINE int num_is_real(pointer p) // Safe.
#define num_is_real(gc, p) num_is_real(p)
#else
static INLINE int num_is_real(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    if (NULL != p && gc_object_get_size_in_bytes(gc, p) >= (OBJECT_SLOT_SIZE + sizeof(num)))
    {
        num *n = object_get_byte(p, 0);
        return !n->is_fixnum;
    }

    return false;
}

static num num_zero;
static num num_one;

/* macros for cell operations */
#define typeflag(p)      (gc_object_get_header_field(p, OBJECT_HEADER_FIELD_FLAGS))
#define settypeflag(p, flag) (gc_update_object_header(gc, p, 1, OBJECT_HEADER_FIELD_FLAGS, (unsigned) (flag)))
#define type(p)          (typeflag(p) & T_MASKTYPE)

#if defined(NO_DEBUG)
#undef is_string
INTERFACE INLINE int is_string(pointer p) // Safe.
#define is_string(gc, p) is_string(p)
#else
INTERFACE INLINE int is_string(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_STRING);
}

#define strvalue(p) *((char **) object_get_byte(p, sizeof(size_t)))
#define strlength(p) (*((size_t *) object_get_byte(p, 0)))

#if defined(NO_DEBUG)
#undef is_vector
INTERFACE INLINE int is_vector(pointer p) // Safe.
#define is_vector(gc, p) is_vector(p)
#else
INTERFACE INLINE int is_vector(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_VECTOR);
}

INTERFACE static void fill_vector(GC *gc, pointer vec, pointer obj); // Safe.

#if defined(NO_DEBUG)
INTERFACE static pointer vector_elem(pointer vec, int ielem); // Safe.
#define vector_elem(gc, vec, ielem) vector_elem(vec, ielem)
#else
INTERFACE static pointer vector_elem(GC *gc, pointer vec, int ielem); // Safe.
#endif // defined(NO_DEBUG).

long vector_length(pointer vec)
{
    return gc_object_get_header_field(vec, OBJECT_HEADER_FIELD_SLOT_COUNT);
}

INTERFACE static pointer set_vector_elem(GC *gc, pointer vec, int ielem, pointer a); // Safe.

#if defined(NO_DEBUG)
#undef is_number
INTERFACE INLINE int is_number(pointer p) // Safe.
#define is_number(gc, p) is_number(p)
#else
INTERFACE INLINE int is_number(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_NUMBER);
}

#if defined(NO_DEBUG)
#undef is_integer
INTERFACE INLINE int is_integer(pointer p) // Safe.
#define is_integer(gc, p) is_integer(p)
#else
INTERFACE INLINE int is_integer(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    if (!is_number(gc, p))
        return 0;
    if (num_is_integer(gc, p) || (double) ivalue(gc, p) == rvalue(gc, p))
        return 1;
    return 0;
}

#if defined(NO_DEBUG)
#undef is_real
INTERFACE INLINE int is_real(pointer p) // Safe.
#define is_real(gc, p) is_real(p)
#else
INTERFACE INLINE int is_real(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return is_number(gc, p) && num_is_real(gc, p);
}

#if defined(NO_DEBUG)
#undef is_character
INTERFACE INLINE int is_character(pointer p) // Safe.
#define is_character(gc, p) is_character(p)
#else
INTERFACE INLINE int is_character(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_CHARACTER);
}

INTERFACE INLINE char *string_value(pointer p) // Safe.
{
    return strvalue(p);
}

INLINE num nvalue(pointer p) // Safe.
{
    num *n = (num *) object_get_byte(p, 0);
    return *n;
}

#if defined(NO_DEBUG)
#undef ivalue
INTERFACE long ivalue(pointer p) // Safe.
#define ivalue(gc, p) ivalue(p)
#else
INTERFACE long ivalue(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    num *n = object_get_byte(p, 0);
    return (num_is_integer(gc, p) ? n->value.ivalue : (long) n->value.rvalue);
}

#if defined(NO_DEBUG)
#undef rvalue
INTERFACE double rvalue(pointer p) // Safe.
#define rvalue(gc, p) rvalue(p)
#else
INTERFACE double rvalue(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    num *n = object_get_byte(p, 0);
    return (!num_is_integer(gc, p) ? n->value.rvalue : (double) n->value.ivalue);
}

#define ivalue_unchecked(p) (((struct num *) object_get_byte(p, 0))->value.ivalue)
#define rvalue_unchecked(p) (((struct num *) object_get_byte(p, 0))->value.rvalue)

#define set_num_integer(p) ((struct num *) object_get_byte(p, 0))->is_fixnum = 1
#define set_num_real(p) ((struct num *) object_get_byte(p, 0))->is_fixnum = 0

INTERFACE long charvalue(pointer p) // Safe.
{
    return ivalue_unchecked(p);
}

#if defined(NO_DEBUG)
#undef is_port
INTERFACE INLINE int is_port(pointer p) // Safe.
#define is_port(gc, p) is_port(p)
#else
INTERFACE INLINE int is_port(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_PORT);
}

#if defined(NO_DEBUG)
INTERFACE INLINE int is_scheme(pointer p) // Safe.
#define is_scheme(gc, p) is_scheme(p)
#else
INTERFACE INLINE int is_scheme(GC *gc, pointer p) // Safe.
#endif // #if defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_SCHEME);
}

#if defined(NO_DEBUG)
INTERFACE INLINE int is_mutex(pointer p) // Safe.
#define is_mutex(gc, p) is_mutex(p)
#else
INTERFACE INLINE int is_mutex(GC *gc, pointer p) // Safe.
#endif // #if defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_MUTEX);
}

#if defined(NO_DEBUG)
INTERFACE INLINE int is_inport(pointer p) // Safe.
#define is_inport(gc, p) is_inport(p)
#else
INTERFACE INLINE int is_inport(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    if (!is_port(gc, p))
    {
        return 0;
    }

    port *p_port = *((port **) object_get_byte(p, 0));
    return 0 != (p_port->kind & port_input);
}

#if defined(NO_DEBUG)
INTERFACE INLINE int is_outport(pointer p) // Safe.
#define is_outport(gc, p) is_outport(p)
#else
INTERFACE INLINE int is_outport(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    if (!is_port(gc, p))
    {
        return 0;
    }

    port *p_port = *((port **) object_get_byte(p, 0));
    return 0 != (p_port->kind & port_output);
}

#if defined(NO_DEBUG)
#undef is_pair
INTERFACE INLINE int is_pair(pointer p) // Safe.
#define is_pair(gc, p) is_pair(p)
#else
INTERFACE INLINE int is_pair(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_PAIR);
}

#define PAIR_CAR_SLOT 0
#define PAIR_CDR_SLOT 1
#define car(pair) get(pair, PAIR_CAR_SLOT)
#define cdr(pair) get(pair, PAIR_CDR_SLOT)

#if defined(NO_DEBUG)
#undef pair_car
INTERFACE pointer pair_car(pointer p) // Safe.
#define pair_car(gc, p) pair_car(p)
#else
INTERFACE pointer pair_car(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return object_get_slot(gc, p, PAIR_CAR_SLOT);
}

#if defined(NO_DEBUG)
#undef pair_cdr
INTERFACE pointer pair_cdr(pointer p) // Safe.
#define pair_cdr(gc, p) pair_cdr(p)
#else
INTERFACE pointer pair_cdr(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return object_get_slot(gc, p, PAIR_CDR_SLOT);
}

INTERFACE pointer set_car(GC *gc, pointer p, pointer q) // Safe.
{
    return object_set_slot(gc, p, PAIR_CAR_SLOT, q);
}

INTERFACE pointer set_cdr(GC *gc, pointer p, pointer q) // Safe.
{
    return object_set_slot(gc, p, PAIR_CDR_SLOT, q);
}

#if defined(NO_DEBUG)
#undef is_symbol
INTERFACE INLINE int is_symbol(pointer p) // Safe.
#define is_symbol(gc, p) is_symbol(p)
#else
INTERFACE INLINE int is_symbol(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_SYMBOL);
}

#if defined(NO_DEBUG)
#undef symname
INTERFACE INLINE char *symname(pointer p) // Safe.
#define symname(gc, p) symname(p)
#else
INTERFACE INLINE char *symname(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return strvalue(pair_car(gc, p));
}

#if USE_PLIST
SCHEME_EXPORT INLINE int hasprop(pointer p) // Safe.
{
    return (typeflag(p) & T_SYMBOL);
}

#define symprop(p) cdr(p)
#endif

INTERFACE INLINE int is_syntax(pointer p) // Safe.
{
    return (typeflag(p) & T_SYNTAX);
}

INTERFACE INLINE int is_proc(pointer p) // Safe.
{
    return (type(p) == T_PROC);
}

INTERFACE INLINE int is_foreign(pointer p) // Safe.
{
    return (type(p) == T_FOREIGN);
}

#if defined(NO_DEBUG)
#undef syntaxname
INTERFACE INLINE char *syntaxname(pointer p) // Safe.
#define syntaxname(gc, p) syntaxname(p)
#else
INTERFACE INLINE char *syntaxname(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return strvalue(pair_car(gc, p));
}

#define procnum(gc, p) ivalue(gc, p) // Safe.

#if defined(NO_DEBUG)
static const char *procname(pointer x); // Safe.
#define procname(gc, x) procname(x)
#else
static const char *procname(GC *gc, pointer x); // Safe.
#endif // defined(NO_DEBUG).

INTERFACE INLINE int is_closure(pointer p) // Safe.
{
    return (type(p) == T_CLOSURE);
}

INTERFACE INLINE int is_macro(pointer p) // Safe.
{
    return (type(p) == T_MACRO);
}

#if defined(NO_DEBUG)
#undef closure_code
INTERFACE INLINE pointer closure_code(pointer p) // Safe.
#define closure_code(gc, p) closure_code(p)
#else
INTERFACE INLINE pointer closure_code(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return pair_car(gc, p);
}

#if defined(NO_DEBUG)
#undef closure_env
INTERFACE INLINE pointer closure_env(pointer p) // Safe.
#define closure_env(gc, p) closure_env(p)
#else
INTERFACE INLINE pointer closure_env(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return pair_cdr(gc, p);
}

INTERFACE INLINE int is_continuation(pointer p) // Safe.
{
    return (type(p) == T_CONTINUATION);
}

#define cont_dump(p) cdr(p) // Safe.

/* TODO: promise should be forced ONCE only. */
INTERFACE INLINE int is_promise(pointer p) // Safe.
{
    return (type(p) == T_PROMISE);
}

#if defined(NO_DEBUG)
#undef is_environment
INTERFACE INLINE int is_environment(pointer p) // Safe.
#define is_environment(gc, p) is_environment(p)
#else
INTERFACE INLINE int is_environment(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return (type(p) == T_ENVIRONMENT);
}

#define setenvironment(gc, p) gc_update_object_header(gc, p, 1, OBJECT_HEADER_FIELD_FLAGS, (unsigned) T_ENVIRONMENT) // Safe.

INTERFACE INLINE int is_immutable(pointer p) // Safe.
{
    return (typeflag(p) & T_IMMUTABLE);
}

/*#define setimmutable(p)  typeflag(p) |= T_IMMUTABLE*/

INTERFACE INLINE void setimmutable(GC *gc, pointer p) // Safe.
{
    gc_update_object_header(gc, p, 1, OBJECT_HEADER_FIELD_FLAGS, (unsigned) (typeflag(p) | T_IMMUTABLE));
}

#define caar(p)   car(car(p))
#define cadr(p)   car(cdr(p))
#define cdar(p)   cdr(car(p))
#define cddr(p)   cdr(cdr(p))
#define cadar(p)  car(cdr(car(p)))
#define caddr(p)  car(cdr(cdr(p)))
#define cdaar(p)  cdr(car(car(p)))
#define cadaar(p) car(cdr(car(car(p))))
#define cadddr(p) car(cdr(cdr(cdr(p))))
#define cddddr(p) cdr(cdr(cdr(cdr(p))))

#if USE_CHAR_CLASSIFIERS
static INLINE int Cisalpha(int c)
{
    return isascii(c) && isalpha(c);
}

static INLINE int Cisdigit(int c)
{
    return isascii(c) && isdigit(c);
}

static INLINE int Cisspace(int c)
{
    return isascii(c) && isspace(c);
}

static INLINE int Cisupper(int c)
{
    return isascii(c) && isupper(c);
}

static INLINE int Cislower(int c)
{
    return isascii(c) && islower(c);
}
#endif

#if USE_ASCII_NAMES
static const char *charnames[32] = {
    "nul",
    "soh",
    "stx",
    "etx",
    "eot",
    "enq",
    "ack",
    "bel",
    "bs",
    "ht",
    "lf",
    "vt",
    "ff",
    "cr",
    "so",
    "si",
    "dle",
    "dc1",
    "dc2",
    "dc3",
    "dc4",
    "nak",
    "syn",
    "etb",
    "can",
    "em",
    "sub",
    "esc",
    "fs",
    "gs",
    "rs",
    "us"
};

static int is_ascii_name(const char *name, int *pc)
{
    int i;

    for (i = 0; i < 32; i++)
    {
        if (stricmp(name, charnames[i]) == 0)
        {
            *pc = i;
            return 1;
        }
    }
    if (stricmp(name, "del") == 0)
    {
        *pc = 127;
        return 1;
    }
    return 0;
}

#endif

static int file_push(pointer scheme_object, const char *fname);
static void file_pop(pointer scheme_object);
static int file_interactive(pointer scheme_object);
static INLINE int is_one_of(char *s, int c);
static long binary_decode(const char *s);
static INLINE pointer get_cell(pointer _scheme_object, pointer _a, pointer _b, unsigned flags);
static void finalize_cell(GC *gc, void *finalizer_data, pointer a);
static pointer find_slot_in_env(pointer scheme_object, pointer env, pointer sym, int all);
static pointer mk_number(GC *gc, num n);
static char *store_string(scheme *sc, int len_str, const char *str, char fill);
static pointer mk_vector(pointer scheme_object, int len);
static pointer mk_atom(pointer _scheme_object, char *q);
static pointer mk_sharp_const(pointer scheme_object, char *name);
static pointer mk_port(GC *gc, port *p);
static pointer port_from_filename(pointer scheme_object, const char *fn, int prop);
static pointer port_from_file(pointer scheme_object, FILE *f, int prop);
static pointer port_from_string(pointer scheme_object, char *start, char *past_the_end, int prop);
static port *port_rep_from_filename(scheme *sc, const char *fn, int prop);
static port *port_rep_from_file(scheme *sc, FILE *, int prop);
static port *port_rep_from_string(scheme *sc, char *start, char *past_the_end, int prop);
static void port_close(scheme *sc, pointer p, int flag);
static int basic_inchar(GC *gc, port *pt);
static int inchar(pointer scheme_object);
static void backchar(pointer scheme_object, int c);
static char *readstr_upto(pointer scheme_object, char *delim);
static pointer readstrexp(pointer scheme_object);
static INLINE int skipspace(pointer scheme_object);
static int token(pointer scheme_object);
static void printslashstring(pointer scheme_object, char *s, int len);
static void atom2str(pointer scheme_object, pointer l, int f, char **pp, int *plen);
static void printatom(pointer scheme_object, pointer l, int f);
static pointer mk_proc(pointer scheme_object, enum scheme_opcodes op);
static pointer reverse(pointer scheme_object, pointer a);
static pointer reverse_in_place(pointer scheme_object, pointer term, pointer list);
static pointer revappend(pointer _scheme_object, pointer a, pointer b);
static int is_list(pointer scheme_object, pointer a);
static pointer opexe_0(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_1(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_2(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_3(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_4(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_5(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_6(pointer _scheme_object, enum scheme_opcodes op);
static pointer opexe_7(pointer _scheme_object, enum scheme_opcodes op);
bool scheme_unregister_vm_thread(DynamicArray *vm_threads, scheme *thread);
pointer scheme_create_thread(pointer scheme_object, pointer thread_func);
void *scheme_thread(void *data);
static void Eval_Cycle(pointer _scheme_object, enum scheme_opcodes op);
static void assign_syntax(pointer scheme_object, char *name);

#if defined(NO_DEBUG)
static int syntaxnum(pointer p);
#define syntaxnum(gc, p) syntaxnum(p)
#else
static int syntaxnum(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

static void assign_proc(pointer _scheme_object, enum scheme_opcodes, char *name);

#define num_ivalue(n) (n.is_fixnum?(n).value.ivalue:(long)(n).value.rvalue)
#define num_rvalue(n) (!n.is_fixnum?(n).value.rvalue:(double)(n).value.ivalue)

static num num_add(num a, num b)
{
    num ret;
    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    if (ret.is_fixnum)
    {
        ret.value.ivalue = a.value.ivalue + b.value.ivalue;
    }
    else
    {
        ret.value.rvalue = num_rvalue(a) + num_rvalue(b);
    }
    return ret;
}

static num num_mul(num a, num b)
{
    num ret;
    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    if (ret.is_fixnum)
    {
        ret.value.ivalue = a.value.ivalue * b.value.ivalue;
    }
    else
    {
        ret.value.rvalue = num_rvalue(a) * num_rvalue(b);
    }
    return ret;
}

static num num_div(num a, num b)
{
    num ret;
    ret.is_fixnum = a.is_fixnum && b.is_fixnum && a.value.ivalue % b.value.ivalue == 0;
    if (ret.is_fixnum)
    {
        ret.value.ivalue = a.value.ivalue / b.value.ivalue;
    }
    else
    {
        ret.value.rvalue = num_rvalue(a) / num_rvalue(b);
    }
    return ret;
}

static num num_intdiv(num a, num b)
{
    num ret;
    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    if (ret.is_fixnum)
    {
        ret.value.ivalue = a.value.ivalue / b.value.ivalue;
    }
    else
    {
        ret.value.rvalue = num_rvalue(a) / num_rvalue(b);
    }
    return ret;
}

static num num_sub(num a, num b)
{
    num ret;
    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    if (ret.is_fixnum)
    {
        ret.value.ivalue = a.value.ivalue - b.value.ivalue;
    }
    else
    {
        ret.value.rvalue = num_rvalue(a) - num_rvalue(b);
    }
    return ret;
}

static num num_rem(num a, num b)
{
    num ret;
    long e1, e2, res;

    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    e1 = num_ivalue(a);
    e2 = num_ivalue(b);
    res = e1 % e2;
    /* remainder should have same sign as second operand */
    if (res > 0)
    {
        if (e1 < 0)
        {
            res -= labs(e2);
        }
    }
    else if (res < 0)
    {
        if (e1 > 0)
        {
            res += labs(e2);
        }
    }
    ret.value.ivalue = res;
    return ret;
}

static num num_mod(num a, num b)
{
    num ret;
    long e1, e2, res;

    ret.is_fixnum = a.is_fixnum && b.is_fixnum;
    e1 = num_ivalue(a);
    e2 = num_ivalue(b);
    res = e1 % e2;
    /* modulo should have same sign as second operand */
    if (res * e2 < 0)
    {
        res += e2;
    }
    ret.value.ivalue = res;
    return ret;
}

static int num_eq(num a, num b)
{
    int ret;
    int is_fixnum = a.is_fixnum && b.is_fixnum;

    if (is_fixnum)
    {
        ret = a.value.ivalue == b.value.ivalue;
    }
    else
    {
        ret = num_rvalue(a) == num_rvalue(b);
    }
    return ret;
}


static int num_gt(num a, num b)
{
    int ret;
    int is_fixnum = a.is_fixnum && b.is_fixnum;

    if (is_fixnum)
    {
        ret = a.value.ivalue > b.value.ivalue;
    }
    else
    {
        ret = num_rvalue(a) > num_rvalue(b);
    }
    return ret;
}

static int num_ge(num a, num b)
{
    return !num_lt(a, b);
}

static int num_lt(num a, num b)
{
    int ret;
    int is_fixnum = a.is_fixnum && b.is_fixnum;

    if (is_fixnum)
    {
        ret = a.value.ivalue < b.value.ivalue;
    }
    else
    {
        ret = num_rvalue(a) < num_rvalue(b);
    }
    return ret;
}

static int num_le(num a, num b)
{
    return !num_gt(a, b);
}

#if USE_MATH
/* Round to nearest. Round to even if midway */
static double round_per_R5RS(double x)
{
    double fl = floor(x);
    double ce = ceil(x);
    double dfl = x - fl;
    double dce = ce - x;

    if (dfl > dce)
    {
        return ce;
    }
    else if (dfl < dce)
    {
        return fl;
    }
    else
    {
        if (fmod(fl, 2.0) == 0.0)
        {                       /* I imagine this holds */
            return fl;
        }
        else
        {
            return ce;
        }
    }
}
#endif

static int is_zero_double(double x)
{
    return x < DBL_MIN && x > -DBL_MIN;
}

static long binary_decode(const char *s)
{
    long x = 0;

    while (*s != 0 && (*s == '1' || *s == '0'))
    {
        x <<= 1;
        x += *s - '0';
        s++;
    }

    return x;
}

static pointer get_cell(pointer _scheme_object, pointer _a, pointer _b, unsigned flags)
scope(scheme_object, sc, gc, a, b)
{
    pointer sc_nil = get(local(scheme_object), SCHEME_NIL);
    local(a) = _a ? _a : sc_nil;
    local(b) = _b ? _b : sc_nil;

    // 2 slots: car and cdr.
    pointer cell = gc_alloc(gc, false, flags, 2, 0);

    /* For right now, include "a" and "b" in "cell" so that gc doesn't
     * think they are garbage.
     */
    set(cell, PAIR_CAR_SLOT, local(a));
    set(cell, PAIR_CDR_SLOT, local(b));

    // push_recent_alloc(sc, cell, sc->NIL);
    scope_return cell;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer get_vector_object(pointer _scheme_object, int len, pointer _init)
scope(scheme_object, sc, gc, init)
{
    local(init) = _init ? _init : get(local(scheme_object), SCHEME_NIL);
    pointer cells = gc_alloc(gc, false, (unsigned) T_VECTOR, len, 0);
    fill_vector(gc, cells, local(init));
    // push_recent_alloc(sc, cells, sc->NIL);
    scope_return cells;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static INLINE void print_gc_statistics(pointer scheme_object)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    GCStatistics *gc_statistics = gc_get_statistics(gc);
    snprintf(sc->strbuff,
             STRBUFFSIZE,
             "GC Statistics:\n"
             "Live: %zu, %zub, %0.4lf.\n"
             "Garbage: %zu, %zub, %0.4lf.\n"
             "Last Collection: %zu, %zub, %0.4lf, %zu moves.\n"
             "Duration: %0.4lfs.\n"
             "SIGSEGVs: %zu (total %zu).\n"
             "Peak Usage: %zub, %0.4lf.\n"
             "Current Usage: %zub, %0.4lf.\n",
             gc_statistics->last_collection_alive_objects,
             gc_statistics->last_collection_used_size,
             gc_statistics->last_collection_used_ratio,
             gc_statistics->last_collection_garbage_objects,
             gc_statistics->last_collection_garbage_size,
             gc_statistics->last_collection_garbage_ratio,
             gc_statistics->last_collected_garbage_objects,
             gc_statistics->last_collected_size,
             gc_statistics->last_collected_ratio,
             gc_statistics->last_collection_move_count,
             (double) gc_statistics->last_collection_time / 1000000.0,
             gc_statistics->last_collection_sigsegv_count,
             gc_statistics->total_sigsegv_count,
             gc_statistics->peak_usage_size,
             gc_statistics->peak_usage_ratio,
             gc_statistics->current_usage_size,
             gc_statistics->current_usage_ratio);
    putstr(scheme_object, sc->strbuff);
}

static INLINE void ok_to_freely_gc(pointer scheme_object, bool force_gc)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    gc_wait_for_collect_completed(gc, !force_gc);
}

/* Medium level cell allocation */

/* get new cons cell */
#define _cons(scheme_object, a, b, immutable) \
get_cell((scheme_object), (a), (b), (immutable) ? (unsigned) (T_PAIR | T_IMMUTABLE) : (unsigned) T_PAIR)

#define cons(scheme_object,a,b) _cons(scheme_object,a,b,0)
#define immutable_cons(scheme_object,a,b) _cons(scheme_object,a,b,1)

/* ========== oblist implementation  ========== */

static pointer oblist_initial_value(pointer scheme_object) // Safe.
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).
    return object_get_slot(gc, scheme_object, SCHEME_NIL);
}

static INLINE pointer oblist_find_by_name(pointer scheme_object, const char *name) // Safe.
{
    pointer x;
    char *s;

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    #if !defined(NO_DEBUG)
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    check_return(tracing_mutex_lock(&sc->global_scheme->global_lock, ""), "pthread_mutex_lock() failed.");

    pointer sc_oblist = object_get_slot(gc, scheme_object, SCHEME_OBLIST),
            sc_nil    = object_get_slot(gc, scheme_object, SCHEME_NIL);

    if (is_scheme(gc, sc_oblist))
    {
        sc_oblist = object_get_slot(gc, sc_oblist, SCHEME_OBLIST); // Get SCHEME_OBLIST of main scheme object.
    }

    for (x = sc_oblist; x != sc_nil; x = pair_cdr(gc, x))
    {
        s = symname(gc, pair_car(gc, x));
        /* case-insensitive, per R5RS section 2. */
        if (stricmp(name, s) == 0)
        {
            check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
            return pair_car(gc, x);
        }
    }

    check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_unmutex_lock() failed.");
    return sc_nil;
    error:
    abort();
    return NULL;
}

/* returns the new symbol */
static pointer oblist_add_by_name(pointer _scheme_object, const char *name)
scope(scheme_object, sc, gc, sc_nil, sc_oblist, x, target_scheme_object)
{
    local(sc_nil)    = get(local(scheme_object), SCHEME_NIL);
    local(sc_oblist) = get(local(scheme_object), SCHEME_OBLIST);

    pointer new_symbol_string = mk_string(local(scheme_object), name);
    setimmutable(gc, new_symbol_string);

    local(x) = immutable_cons(local(scheme_object), new_symbol_string, local(sc_nil));
    gc_update_object_header(gc, local(x), 1, OBJECT_HEADER_FIELD_FLAGS, (unsigned) T_SYMBOL);

    check_return(tracing_mutex_lock(&sc->global_scheme->global_lock, ""), "pthread_mutex_lock() failed.");

    if (is_scheme(gc, local(sc_oblist)))
    {
        local(target_scheme_object) = local(sc_oblist);
        local(sc_oblist) = get(local(sc_oblist), SCHEME_OBLIST); // Get SCHEME_OBLIST of main scheme object.
    }
    else
    {
        local(target_scheme_object) = local(scheme_object);
    }

    pointer t = immutable_cons(local(scheme_object), local(x), local(sc_oblist));
    local(sc_oblist) = t;
    set(local(target_scheme_object), SCHEME_OBLIST, local(sc_oblist));

    check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
    scope_return local(x);

    error:
    abort();
    scope_return NULL;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer oblist_all_symbols(pointer scheme_object) // Safe.
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).
    pointer sc_oblist = object_get_slot(gc, scheme_object, SCHEME_OBLIST);
    if (is_scheme(gc, sc_oblist))
    {
        sc_oblist = object_get_slot(gc, sc_oblist, SCHEME_OBLIST);
    }
    return sc_oblist;
}

static pointer mk_port(GC *gc, port *p)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_PORT, 0, sizeof(port *));
    gc_lock_object(gc, x);
    port **x_port = (port **) object_get_byte(x, 0);
    *x_port = p;
    gc_unlock_object(gc, x);
    return (x);
}

pointer mk_foreign_func(GC *gc, foreign_func f)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_FOREIGN, 0, sizeof(foreign_func));
    gc_lock_object(gc, x);
    foreign_func *x_ff = (foreign_func *) object_get_byte(x, 0);
    *x_ff = f;
    gc_unlock_object(gc, x);
    return (x);
}

INTERFACE pointer mk_character(GC *gc, int c)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_CHARACTER, 0, sizeof(num));
    gc_lock_object(gc, x);
    num *n = (num *) object_get_byte(x, 0);
    n->value.ivalue = c;
    set_num_integer(x);
    gc_unlock_object(gc, x);
    return (x);
}

/* get number atom (integer) */
INTERFACE pointer mk_integer(GC *gc, long num)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_NUMBER, 0, sizeof(struct num));
    gc_lock_object(gc, x);
    struct num *n = (struct num *) object_get_byte(x, 0);
    n->value.ivalue = num;
    set_num_integer(x);
    gc_unlock_object(gc, x);
    return (x);
}

INTERFACE pointer mk_real(GC *gc, double n)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_NUMBER, 0, sizeof(struct num));
    gc_lock_object(gc, x);
    struct num *x_num = (struct num *) object_get_byte(x, 0);
    x_num->value.rvalue = n;
    set_num_real(x);
    gc_unlock_object(gc, x);
    return (x);
}

static pointer mk_number(GC *gc, num n)
{
    if (n.is_fixnum)
    {
        return mk_integer(gc, n.value.ivalue);
    }
    else
    {
        return mk_real(gc, n.value.rvalue);
    }
}

/* allocate name to string area */
static char *store_string(scheme *sc, int len_str, const char *str, char fill) // Safe.
{
    char *q;

    q = (char *) sc->malloc(len_str + 1);
    if (q == 0)
    {
        sc->global_scheme->no_memory = 1;
        return sc->strbuff;
    }
    if (str != 0)
    {
        snprintf(q, len_str + 1, "%s", str);
    }
    else
    {
        memset(q, fill, len_str);
        q[len_str] = 0;
    }
    return (q);
}

/* get new string */
INTERFACE pointer mk_string(pointer scheme_object, const char *str)
{
    return mk_counted_string(scheme_object, str, strlen(str));
}

INTERFACE pointer mk_counted_string(pointer _scheme_object, const char *str, int len)
scope(scheme_object, sc, gc)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_STRING, 0, sizeof(size_t) + sizeof(char *));
    gc_lock_object(gc, x);
    void *x_raw = get_byte(x, 0);
    memcpy(x_raw, &len, sizeof(size_t));
    char *stored_string = store_string(sc, len, str, 0);
    memcpy(x_raw + sizeof(size_t), &stored_string, sizeof(char *));
    gc_unlock_object(gc, x);
    scope_return (x);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

INTERFACE pointer mk_empty_string(pointer _scheme_object, int len, char fill)
scope(scheme_object, sc, gc)
{
    pointer x = gc_alloc(gc, false, (unsigned) T_STRING, 0, sizeof(size_t) + sizeof(char *));
    gc_lock_object(gc, x);
    void *x_raw = object_get_byte(x, 0);
    memcpy(x_raw, &len, sizeof(size_t));
    char *stored_string = store_string(sc, len, 0, fill);
    memcpy(x_raw + sizeof(size_t), &stored_string, sizeof(char *));
    gc_unlock_object(gc, x);
    scope_return (x);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

INTERFACE static pointer mk_vector(pointer scheme_object, int len)
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // defined(NO_DEBUG).
    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    return get_vector_object(scheme_object, len, sc_nil);
}

INTERFACE static void fill_vector(GC *gc, pointer vec, pointer obj) // Safe.
{
    object_fill_slots(gc, vec, obj);
}

INTERFACE static pointer vector_elem(GC *gc, pointer vec, int ielem) // Safe.
{
    return object_get_slot(gc, vec, ielem);
}

INTERFACE static pointer set_vector_elem(GC *gc, pointer vec, int ielem, pointer a) // Safe.
{
    return object_set_slot(gc, vec, ielem, a);
}

/* get new symbol */
INTERFACE pointer mk_symbol(pointer scheme_object, const char *name)
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    pointer x;
    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);

    /* first check oblist */
    x = oblist_find_by_name(scheme_object, name);
    if (x != sc_nil)
    {
        return (x);
    }
    else
    {
        x = oblist_add_by_name(scheme_object, name);
        return (x);
    }
}

INTERFACE pointer gensym(pointer scheme_object)
{
    pointer x;
    char name[40];

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    pointer sc_nil = object_get_slot(sc->gc, scheme_object, SCHEME_NIL);

    check_return(tracing_mutex_lock(&sc->global_scheme->global_lock, ""), "pthread_mutex_lock() failed.");

    long *gensym_cnt = &sc->global_scheme->gensym_cnt;

    while (*gensym_cnt < LONG_MAX)
    {
        snprintf(name, 40, "gensym-%ld", *gensym_cnt);

        /* first check oblist */
        x = oblist_find_by_name(scheme_object, name);

        if (x != sc_nil)
        {
            (*gensym_cnt)++;
            continue;
        }
        else
        {
            x = oblist_add_by_name(scheme_object, name);
            check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
            return (x);
        }
    }

    check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
    return sc_nil;

    error:
    abort();
    return NULL;
}

/* make symbol or number atom from string */
static pointer mk_atom(pointer _scheme_object, char *q)
scope(scheme_object, sc, gc, sc_nil, sc_colon_hook, sc_quote, new_atom, new_symbol, p1, p2, p3, p4)
{
    char c, *p;
    int has_dec_point = 0;
    int has_fp_exp = 0;

#if USE_COLON_HOOK
    if ((p = strstr(q, "::")) != 0)
    {
        *p = 0;

        local(sc_nil)        = get(local(scheme_object), SCHEME_NIL),
        local(sc_colon_hook) = get(local(scheme_object), SCHEME_COLON_HOOK),
        local(sc_quote)      = get(local(scheme_object), SCHEME_QUOTE);
        local(new_atom)      = mk_atom(local(scheme_object), p + 2);
        local(new_symbol)    = mk_symbol(local(scheme_object), strlwr(q));
        local(p1)            = cons(local(scheme_object), local(new_atom), local(sc_nil));
        local(p2)            = cons(local(scheme_object), local(sc_quote), local(p1));
        local(p3)            = cons(local(scheme_object), local(new_symbol), local(sc_nil));
        local(p4)            = cons(local(scheme_object), local(p2), local(p3));

        local(p4) = cons(local(scheme_object), local(sc_colon_hook), local(p4));
        scope_return(local(p4));
    }
#endif

    p = q;
    c = *p++;
    if ((c == '+') || (c == '-'))
    {
        c = *p++;
        if (c == '.')
        {
            has_dec_point = 1;
            c = *p++;
        }
        if (!isdigit(c))
        {
            scope_return (mk_symbol(local(scheme_object), strlwr(q)));
        }
    }
    else if (c == '.')
    {
        has_dec_point = 1;
        c = *p++;
        if (!isdigit(c))
        {
            scope_return (mk_symbol(local(scheme_object), strlwr(q)));
        }
    }
    else if (!isdigit(c))
    {
        scope_return (mk_symbol(local(scheme_object), strlwr(q)));
    }

    for (; (c = *p) != 0; ++p)
    {
        if (!isdigit(c))
        {
            if (c == '.')
            {
                if (!has_dec_point)
                {
                    has_dec_point = 1;
                    continue;
                }
            }
            else if ((c == 'e') || (c == 'E'))
            {
                if (!has_fp_exp)
                {
                    has_dec_point = 1;  /* decimal point illegal
                                         * from now on */
                    p++;
                    if ((*p == '-') || (*p == '+') || isdigit(*p))
                    {
                        continue;
                    }
                }
            }
            scope_return (mk_symbol(local(scheme_object), strlwr(q)));
        }
    }
    if (has_dec_point)
    {
        scope_return mk_real(gc, atof(q));
    }
    scope_return (mk_integer(gc, atol(q)));
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

/* make constant */
static pointer mk_sharp_const(pointer scheme_object, char *name)
{
    long x;
    char tmp[STRBUFFSIZE];

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    if (!strcmp(name, "t"))
        return object_get_slot(gc, scheme_object, SCHEME_HASH_T);
    else if (!strcmp(name, "f"))
        return object_get_slot(gc, scheme_object, SCHEME_HASH_F);
    else if (*name == 'o')
    {                           /* #o (octal) */
        snprintf(tmp, STRBUFFSIZE, "0%s", name + 1);
        sscanf(tmp, "%lo", (long unsigned *) &x);
        return (mk_integer(gc, x));
    }
    else if (*name == 'd')
    {                           /* #d (decimal) */
        sscanf(name + 1, "%ld", (long int *) &x);
        return (mk_integer(gc, x));
    }
    else if (*name == 'x')
    {                           /* #x (hex) */
        snprintf(tmp, STRBUFFSIZE, "0x%s", name + 1);
        sscanf(tmp, "%lx", (long unsigned *) &x);
        return (mk_integer(gc, x));
    }
    else if (*name == 'b')
    {                           /* #b (binary) */
        x = binary_decode(name + 1);
        return (mk_integer(gc, x));
    }
    else if (*name == '\\')
    {                           /* #\w (character) */
        int c = 0;

        if (stricmp(name + 1, "space") == 0)
        {
            c = ' ';
        }
        else if (stricmp(name + 1, "newline") == 0)
        {
            c = '\n';
        }
        else if (stricmp(name + 1, "return") == 0)
        {
            c = '\r';
        }
        else if (stricmp(name + 1, "tab") == 0)
        {
            c = '\t';
        }
        else if (name[1] == 'x' && name[2] != 0)
        {
            int c1 = 0;

            if (sscanf(name + 2, "%x", (unsigned int *) &c1) == 1 && c1 < UCHAR_MAX)
            {
                c = c1;
            }
            else
            {
                return object_get_slot(gc, scheme_object, SCHEME_NIL);
            }
#if USE_ASCII_NAMES
        }
        else if (is_ascii_name(name + 1, &c))
        {
            /* nothing */
#endif
        }
        else if (name[2] == 0)
        {
            c = name[1];
        }
        else
        {
            return object_get_slot(gc, scheme_object, SCHEME_NIL);
        }
        return mk_character(gc, c);
    }
    else
        return object_get_slot(gc, scheme_object, SCHEME_NIL);
}

static pointer mk_mutex(scheme *sc)
{
    GC *gc = sc->gc;

    pointer result = gc_alloc(gc, false, (unsigned) T_MUTEX, 0, sizeof(pthread_mutex_t *));

    pthread_mutex_t *mutex = sc->malloc(sizeof(pthread_mutex_t));
    check_mem(mutex);

    memcpy(object_get_byte(result, 0), &mutex, sizeof(pthread_mutex_t *));

    pthread_mutexattr_t mutex_attributes;
    memset(&mutex_attributes, 0, sizeof(mutex_attributes));
    check_return(pthread_mutexattr_init(&mutex_attributes), "pthread_mutexattr_init() failed.");
    check_return(pthread_mutexattr_settype(&mutex_attributes, PTHREAD_MUTEX_RECURSIVE), "pthread_mutexatttr_settype() failed.");
    check_return(pthread_mutex_init(mutex, &mutex_attributes), "pthread_mutex_init() failed.");
    check_return(pthread_mutexattr_destroy(&mutex_attributes), "pthread_mutexattr_destroy() failed.");

    return result;
    error:
    abort();
    return NULL;
}

static void finalize_cell(GC *gc, void *callback_data, pointer a)
{
    pointer scheme_object = *((pointer *) callback_data);
    if (NULL == scheme_object)
    {
        return;
    }
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    if (is_string(gc, a))
    {
        char *s = strvalue(a);
        sc->free(s);
        memset(object_get_byte(a, 0), 0, sizeof(size_t) + sizeof(char *));
    }
    else if (is_port(gc, a))
    {
        port **p = (port **) object_get_byte(a, 0);
        if (NULL != *p)
        {
            if ((*p)->kind & port_file && (*p)->rep.stdio.closeit)
            {
                port_close(sc, a, port_input | port_output);
            }

            if (!((*p) >= sc->load_stack && (*p) < (sc->load_stack + MAXFIL)))
            {
                sc->free((*p));
            }

            gc_lock_object(gc, a);
            *p = NULL;
            gc_unlock_object(gc, a);
        }
    }
    else if (is_mutex(gc, a))
    {
        pthread_mutex_t *mutex = *((pthread_mutex_t **) object_get_byte(a, 0));
        pthread_mutex_destroy(mutex);
    }
    else if (is_scheme(gc, a))
    {
        scheme_deinit(a);
    }
}

static void collection_callback(GC *gc, void *callback_data, bool called_before_collection)
{
    (void) gc;
    pointer scheme_object = *((pointer *) callback_data);
    if (NULL == scheme_object)
    {
        return;
    }
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    if (sc->global_scheme->gc_verbose)
    {
        if (called_before_collection)
        {
            putstr(scheme_object, "GC... ");
        }
        else
        {
            putstr(scheme_object, "Done. ");
            print_gc_statistics(scheme_object);
        }
    }
}

static bool no_memory_callback(GC *gc, void *callback_data)
{
    (void) gc;
    pointer scheme_object = *((pointer *) callback_data);
    if (NULL != scheme_object)
    {
        scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
        sc->no_memory = 1;
    }
    return false; // Don't retry collection.
}

/* ========== Routines for Reading ========== */

static int file_push(pointer scheme_object, const char *fname) // Safe.
{
    FILE *fin = NULL;

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    if (sc->file_i == MAXFIL - 1)
        return 0;

    fin = fopen(fname, "r");
    if (fin != 0)
    {
        sc->file_i++;
        sc->load_stack[sc->file_i].kind = port_file | port_input;
        sc->load_stack[sc->file_i].rep.stdio.file = fin;
        sc->load_stack[sc->file_i].rep.stdio.closeit = 1;
        sc->nesting_stack[sc->file_i] = 0;

        pointer sc_loadport = object_get_slot(sc->gc, scheme_object, SCHEME_LOADPORT);
        gc_lock_object(gc, sc_loadport);
        port *new_loadport = &sc->load_stack[sc->file_i];
        memcpy(object_get_byte(sc_loadport, 0), &new_loadport, sizeof(port *));
        gc_unlock_object(gc, sc_loadport);

#if SHOW_ERROR_LINE
        sc->load_stack[sc->file_i].rep.stdio.curr_line = 0;
        if (fname)
        {
            sc->load_stack[sc->file_i].rep.stdio.filename = store_string(sc, strlen(fname), fname, 0);
        }
#endif
    }
    return fin != 0;
}

static void file_pop(pointer scheme_object) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    if (sc->file_i != 0)
    {
        sc->nesting = sc->nesting_stack[sc->file_i];
        sc->file_i--;

        pointer sc_loadport = object_get_slot(sc->gc, scheme_object, SCHEME_LOADPORT);
        port_close(sc, sc_loadport, port_input);
        gc_lock_object(gc, sc_loadport);
        port *new_port = &sc->load_stack[sc->file_i];
        memcpy(object_get_byte(sc_loadport, 0), &new_port, sizeof(port *));
        gc_unlock_object(gc, sc_loadport);
    }
}

static int file_interactive(pointer scheme_object) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    pointer sc_inport = object_get_slot(sc->gc, scheme_object, SCHEME_INPORT);
    port *inport = *((port **) object_get_byte(sc_inport, 0));
    return sc->file_i == 0 && sc->load_stack[0].rep.stdio.file == stdin && inport->kind & port_file;
}

static port *port_rep_from_filename(scheme *sc, const char *fn, int prop) // Safe.
{
    FILE *f;
    char *rw;
    port *pt;

    if (prop == (port_input | port_output))
    {
        rw = "a+";
    }
    else if (prop == port_output)
    {
        rw = "w";
    }
    else
    {
        rw = "r";
    }
    f = fopen(fn, rw);
    if (f == 0)
    {
        return 0;
    }
    pt = port_rep_from_file(sc, f, prop);
    pt->rep.stdio.closeit = 1;

#if SHOW_ERROR_LINE
    if (fn)
    {
        pt->rep.stdio.filename = store_string(sc, strlen(fn), fn, 0);
    }

    pt->rep.stdio.curr_line = 0;
#endif
    return pt;
}

static pointer port_from_filename(pointer scheme_object, const char *fn, int prop)
{
    port *pt;

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    pt = port_rep_from_filename(sc, fn, prop);
    if (pt == 0)
    {
        return object_get_slot(sc->gc, scheme_object, SCHEME_NIL);
    }
    return mk_port(sc->gc, pt);
}

static port *port_rep_from_file(scheme *sc, FILE *f, int prop) // Safe.
{
    port *pt;

    pt = (port *) sc->malloc(sizeof *pt);
    if (pt == NULL)
    {
        return NULL;
    }
    memset(pt, 0, sizeof(port));
    pt->kind = port_file | prop;
    pt->rep.stdio.file = f;
    pt->rep.stdio.closeit = 0;
    return pt;
}

static pointer port_from_file(pointer scheme_object, FILE *f, int prop)
{
    port *pt;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    pt = port_rep_from_file(sc, f, prop);
    if (pt == 0)
    {
        return object_get_slot(sc->gc, scheme_object, SCHEME_NIL);
    }
    return mk_port(sc->gc, pt);
}

static port *port_rep_from_string(scheme *sc, char *start, char *past_the_end, int prop) // Safe.
{
    port *pt;

    pt = (port *) sc->malloc(sizeof(port));
    if (pt == 0)
    {
        return 0;
    }
    memset(pt, 0, sizeof(port));
    pt->kind = port_string | prop;
    pt->rep.string.start = start;
    pt->rep.string.curr = start;
    pt->rep.string.past_the_end = past_the_end;
    return pt;
}

static pointer port_from_string(pointer scheme_object, char *start, char *past_the_end, int prop)
{
    port *pt;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    pt = port_rep_from_string(sc, start, past_the_end, prop);
    if (pt == 0)
    {
        return object_get_slot(sc->gc, scheme_object, SCHEME_NIL);
    }
    return mk_port(sc->gc, pt);
}

#define BLOCK_SIZE 256

static port *port_rep_from_scratch(scheme *sc) // Safe.
{
    port *pt;
    char *start;

    pt = (port *) sc->malloc(sizeof(port));
    if (pt == 0)
    {
        return 0;
    }
    memset(pt, 0, sizeof(port));
    start = sc->malloc(BLOCK_SIZE);
    if (start == 0)
    {
        sc->free(pt);
        return 0;
    }
    memset(start, ' ', BLOCK_SIZE - 1);
    start[BLOCK_SIZE - 1] = '\0';
    pt->kind = port_string | port_output | port_srfi6;
    pt->rep.string.start = start;
    pt->rep.string.curr = start;
    pt->rep.string.past_the_end = start + BLOCK_SIZE - 1;
    return pt;
}

static pointer port_from_scratch(pointer scheme_object)
{
    port *pt;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    pt = port_rep_from_scratch(sc);
    if (pt == 0)
    {
        return object_get_slot(sc->gc, scheme_object, SCHEME_NIL);
    }
    return mk_port(sc->gc, pt);
}

static void port_close(scheme *sc, pointer p, int flag) // Safe.
{
    port *pt = *((port **) object_get_byte(p, 0));

    pt->kind &= ~flag;
    if ((pt->kind & (port_input | port_output)) == 0)
    {
        if (pt->kind & port_file)
        {

#if SHOW_ERROR_LINE
            /* Cleanup is here so (close-*-port) functions could work too */
            pt->rep.stdio.curr_line = 0;

            if (pt->rep.stdio.filename)
            {
                sc->free(pt->rep.stdio.filename);
                pt->rep.stdio.filename = NULL;
            }
#endif

            fclose(pt->rep.stdio.file);
        }
        pt->kind = port_free;
    }
}

/* get new character from input file */
static int inchar(pointer scheme_object) // Safe.
{
    int c;

    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    pointer sc_inport   = object_get_slot(gc, scheme_object, SCHEME_INPORT),
            sc_loadport = object_get_slot(gc, scheme_object, SCHEME_LOADPORT);

    port *pt = *((port **) object_get_byte(sc_inport, 0));

    if (pt->kind & port_saw_EOF)
    {
        return EOF;
    }
    c = basic_inchar(gc, pt);
    if (c == EOF && sc_inport == sc_loadport)
    {
        /* Instead, set port_saw_EOF */
        pt->kind |= port_saw_EOF;

        /* file_pop(sc); */
        return EOF;
        /* NOTREACHED */
    }
    return c;
}

static int basic_inchar(GC *gc, port *pt) // Safe.
{
    if (pt->kind & port_file)
    {
        fd_set read_fd_set, except_fd_set;
        FD_ZERO(&read_fd_set);
        FD_ZERO(&except_fd_set);

        int read_fd = fileno(pt->rep.stdio.file);
        FD_SET(read_fd, &read_fd_set);
        FD_SET(read_fd, &except_fd_set);

        struct timespec pselect_timeout = {
            .tv_sec  = 0,
            .tv_nsec = 0
        };

        sigset_t pselect_sigmask;
        check_return(sigemptyset(&pselect_sigmask), "sigemptyset() failed.");
        check_return(sigaddset(&pselect_sigmask, SIGSEGV), "sigaddset() failed.");

        int fd_available = 0;
        check(-1 != (fd_available = pselect(FD_SETSIZE,
                                            &read_fd_set,
                                            NULL,
                                            &except_fd_set,
                                            &pselect_timeout,
                                            &pselect_sigmask)),
              "pselect() failed.");

        if (0 == fd_available)
        {
            gc_vm_thread_fall_asleep(gc);
        }

        int retval = fgetc(pt->rep.stdio.file);

        if (0 == fd_available)
        {
            gc_vm_thread_awoke(gc);
        }
        return retval;
    }
    else
    {
        if (*pt->rep.string.curr == 0 || pt->rep.string.curr == pt->rep.string.past_the_end)
        {
            return EOF;
        }
        else
        {
            return *pt->rep.string.curr++;
        }
    }

    error:
    abort();
    return EOF;
}

/* back character to input buffer */
static void backchar(pointer scheme_object, int c) // Safe.
{
    if (c == EOF)
        return;

    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).
    pointer sc_inport = object_get_slot(gc, scheme_object, SCHEME_INPORT);
    port *pt = *((port **) object_get_byte(sc_inport, 0));
    if (pt->kind & port_file)
    {
        ungetc(c, pt->rep.stdio.file);
    }
    else
    {
        if (pt->rep.string.curr != pt->rep.string.start)
        {
            --pt->rep.string.curr;
        }
    }
}

static int realloc_port_string(scheme *sc, port *p) // Safe.
{
    char *start = p->rep.string.start;
    size_t new_size = p->rep.string.past_the_end - start + 1 + BLOCK_SIZE;
    char *str = sc->malloc(new_size);

    if (str)
    {
        memset(str, ' ', new_size - 1);
        str[new_size - 1] = '\0';
        strcpy(str, start);
        p->rep.string.start = str;
        p->rep.string.past_the_end = str + new_size - 1;
        p->rep.string.curr -= start - str;
        sc->free(start);
        return 1;
    }
    else
    {
        return 0;
    }
}

INTERFACE void putstr(pointer scheme_object, const char *s) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    pointer sc_outport = object_get_slot(sc->gc, scheme_object, SCHEME_OUTPORT);
    port *pt = *((port **) object_get_byte(sc_outport, 0));

    if (pt->kind & port_file)
    {
        fputs(s, pt->rep.stdio.file);
    }
    else
    {
        for (; *s; s++)
        {
            if (pt->rep.string.curr != pt->rep.string.past_the_end)
            {
                *pt->rep.string.curr++ = *s;
            }
            else if (pt->kind & port_srfi6 && realloc_port_string(sc, pt))
            {
                *pt->rep.string.curr++ = *s;
            }
        }
    }
}

static void putchars(pointer scheme_object, const char *s, int len) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    pointer sc_outport = object_get_slot(sc->gc, scheme_object, SCHEME_OUTPORT);
    port *pt = *((port **) object_get_byte(sc_outport, 0));

    if (pt->kind & port_file)
    {
        fwrite(s, 1, len, pt->rep.stdio.file);
    }
    else
    {
        for (; len; len--)
        {
            if (pt->rep.string.curr != pt->rep.string.past_the_end)
            {
                *pt->rep.string.curr++ = *s++;
            }
            else if (pt->kind & port_srfi6 && realloc_port_string(sc, pt))
            {
                *pt->rep.string.curr++ = *s++;
            }
        }
    }
}

INTERFACE void putcharacter(pointer scheme_object, int c) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    pointer sc_outport = object_get_slot(sc->gc, scheme_object, SCHEME_OUTPORT);
    port *pt = *((port **) object_get_byte(sc_outport, 0));

    if (pt->kind & port_file)
    {
        fputc(c, pt->rep.stdio.file);
    }
    else
    {
        if (pt->rep.string.curr != pt->rep.string.past_the_end)
        {
            *pt->rep.string.curr++ = c;
        }
        else if (pt->kind & port_srfi6 && realloc_port_string(sc, pt))
        {
            *pt->rep.string.curr++ = c;
        }
    }
}

/* read characters up to delimiter, but cater to character constants */
static char *readstr_upto(pointer scheme_object, char *delim) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    char *p = sc->strbuff;

    while (((size_t) (p - sc->strbuff) < sizeof(sc->strbuff)) && !is_one_of(delim, (*p++ = inchar(scheme_object))));

    if (p == sc->strbuff + 2 && p[-2] == '\\')
    {
        *p = 0;
    }
    else
    {
        backchar(scheme_object, p[-1]);
        *--p = '\0';
    }
    return sc->strbuff;
}

/* read string expression "xxx...xxx" */
static pointer readstrexp(pointer scheme_object)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    char *p = sc->strbuff;
    int c;
    int c1 = 0;
    enum
    { st_ok, st_bsl, st_x1, st_x2, st_oct1, st_oct2 } state = st_ok;

    for (;;)
    {
        c = inchar(scheme_object);
        if (c == EOF || (size_t) (p - sc->strbuff) > (sizeof(sc->strbuff) - 1))
        {
            return object_get_slot(sc->gc, scheme_object, SCHEME_HASH_F);
        }
        switch (state)
        {
            case st_ok:
                switch (c)
                {
                    case '\\':
                        state = st_bsl;
                        break;
                    case '"':
                        *p = 0;
                        return mk_counted_string(scheme_object, sc->strbuff, p - sc->strbuff);
                    default:
                        *p++ = c;
                        break;
                }
                break;
            case st_bsl:
                switch (c)
                {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                        state = st_oct1;
                        c1 = c - '0';
                        break;
                    case 'x':
                    case 'X':
                        state = st_x1;
                        c1 = 0;
                        break;
                    case 'n':
                        *p++ = '\n';
                        state = st_ok;
                        break;
                    case 't':
                        *p++ = '\t';
                        state = st_ok;
                        break;
                    case 'r':
                        *p++ = '\r';
                        state = st_ok;
                        break;
                    case '"':
                        *p++ = '"';
                        state = st_ok;
                        break;
                    default:
                        *p++ = c;
                        state = st_ok;
                        break;
                }
                break;
            case st_x1:
            case st_x2:
                c = toupper(c);
                if (c >= '0' && c <= 'F')
                {
                    if (c <= '9')
                    {
                        c1 = (c1 << 4) + c - '0';
                    }
                    else
                    {
                        c1 = (c1 << 4) + c - 'A' + 10;
                    }
                    if (state == st_x1)
                    {
                        state = st_x2;
                    }
                    else
                    {
                        *p++ = c1;
                        state = st_ok;
                    }
                }
                else
                {
                    return object_get_slot(sc->gc, scheme_object, SCHEME_HASH_F);
                }
                break;
            case st_oct1:
            case st_oct2:
                if (c < '0' || c > '7')
                {
                    *p++ = c1;
                    backchar(scheme_object, c);
                    state = st_ok;
                }
                else
                {
                    if (state == st_oct2 && c1 >= 32)
                        return object_get_slot(sc->gc, scheme_object, SCHEME_HASH_F);

                    c1 = (c1 << 3) + (c - '0');

                    if (state == st_oct1)
                        state = st_oct2;
                    else
                    {
                        *p++ = c1;
                        state = st_ok;
                    }
                }
                break;

        }
    }
}

/* check c is in chars */
static INLINE int is_one_of(char *s, int c)
{
    if (c == EOF)
        return 1;
    while (*s)
        if (*s++ == c)
            return (1);
    return (0);
}

/* skip white characters */
static INLINE int skipspace(pointer scheme_object) // Safe.
{
    int c = 0, curr_line = 0;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    do
    {
        c = inchar(scheme_object);
#if SHOW_ERROR_LINE
        if (c == '\n')
            curr_line++;
#endif
    } while (isspace(c));

/* record it */
#if SHOW_ERROR_LINE
    if (sc->load_stack[sc->file_i].kind & port_file)
        sc->load_stack[sc->file_i].rep.stdio.curr_line += curr_line;
#endif

    if (c != EOF)
    {
        backchar(scheme_object, c);
        return 1;
    }
    else
    {
        return EOF;
    }
}

/* get token */
static int token(pointer scheme_object) // Safe.
{
    int c;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    c = skipspace(scheme_object);
    if (c == EOF)
    {
        return (TOK_EOF);
    }
    switch (c = inchar(scheme_object))
    {
        case EOF:
            return (TOK_EOF);
        case '(':
            return (TOK_LPAREN);
        case ')':
            return (TOK_RPAREN);
        case '.':
            c = inchar(scheme_object);
            if (is_one_of(" \n\t", c))
            {
                return (TOK_DOT);
            }
            else
            {
                backchar(scheme_object, c);
                backchar(scheme_object, '.');
                return TOK_ATOM;
            }
        case '\'':
            return (TOK_QUOTE);
        case ';':
            while ((c = inchar(scheme_object)) != '\n' && c != EOF);

#if SHOW_ERROR_LINE
            if (c == '\n' && sc->load_stack[sc->file_i].kind & port_file)
                sc->load_stack[sc->file_i].rep.stdio.curr_line++;
#endif

            if (c == EOF)
            {
                return (TOK_EOF);
            }
            else
            {
                return (token(scheme_object));
            }
        case '"':
            return (TOK_DQUOTE);
        case BACKQUOTE:
            return (TOK_BQUOTE);
        case ',':
            if ((c = inchar(scheme_object)) == '@')
            {
                return (TOK_ATMARK);
            }
            else
            {
                backchar(scheme_object, c);
                return (TOK_COMMA);
            }
        case '#':
            c = inchar(scheme_object);
            if (c == '(')
            {
                return (TOK_VEC);
            }
            else if (c == '!')
            {
                while ((c = inchar(scheme_object)) != '\n' && c != EOF);

#if SHOW_ERROR_LINE
                if (c == '\n' && sc->load_stack[sc->file_i].kind & port_file)
                    sc->load_stack[sc->file_i].rep.stdio.curr_line++;
#endif

                if (c == EOF)
                {
                    return (TOK_EOF);
                }
                else
                {
                    return (token(scheme_object));
                }
            }
            else
            {
                backchar(scheme_object, c);
                if (is_one_of(" tfodxb\\", c))
                {
                    return TOK_SHARP_CONST;
                }
                else
                {
                    return (TOK_SHARP);
                }
            }
        default:
            backchar(scheme_object, c);
            return (TOK_ATOM);
    }
}

/* ========== Routines for Printing ========== */
#define ok_abbrev(gc, x) (is_pair(gc, x) && pair_cdr(gc, x) == slot(sc_nil))

static void printslashstring(pointer scheme_object, char *p, int len) // Safe.
{
    int i;
    unsigned char *s = (unsigned char *) p;

    putcharacter(scheme_object, '"');
    for (i = 0; i < len; i++)
    {
        if (*s == 0xff || *s == '"' || *s < ' ' || *s == '\\')
        {
            putcharacter(scheme_object, '\\');
            switch (*s)
            {
                case '"':
                    putcharacter(scheme_object, '"');
                    break;
                case '\n':
                    putcharacter(scheme_object, 'n');
                    break;
                case '\t':
                    putcharacter(scheme_object, 't');
                    break;
                case '\r':
                    putcharacter(scheme_object, 'r');
                    break;
                case '\\':
                    putcharacter(scheme_object, '\\');
                    break;
                default:
                {
                    int d = (*s) / 16;

                    putcharacter(scheme_object, 'x');
                    if (d < 10)
                    {
                        putcharacter(scheme_object, d + '0');
                    }
                    else
                    {
                        putcharacter(scheme_object, d - 10 + 'A');
                    }
                    d = (*s) % 16;
                    if (d < 10)
                    {
                        putcharacter(scheme_object, d + '0');
                    }
                    else
                    {
                        putcharacter(scheme_object, d - 10 + 'A');
                    }
                }
            }
        }
        else
        {
            putcharacter(scheme_object, *s);
        }
        s++;
    }
    putcharacter(scheme_object, '"');
}

/* print atoms */
static void printatom(pointer scheme_object, pointer l, int f) // Safe.
{
    char *p;
    int len;

    atom2str(scheme_object, l, f, &p, &len);
    putchars(scheme_object, p, len);
}

/* Uses internal buffer unless string pointer is already available */
static void atom2str(pointer scheme_object, pointer l, int f, char **pp, int *plen) // Safe.
{
    char *p;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));

    #if !defined(NO_DEBUG)
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    if (l == object_get_slot(gc, scheme_object, SCHEME_NIL))
    {
        p = "()";
    }
    else if (l == object_get_slot(gc, scheme_object, SCHEME_HASH_T))
    {
        p = "#t";
    }
    else if (l == object_get_slot(gc, scheme_object, SCHEME_HASH_F))
    {
        p = "#f";
    }
    else if (l == object_get_slot(gc, scheme_object, SCHEME_EOF_OBJ))
    {
        p = "#<EOF>";
    }
    else if (is_port(gc, l))
    {
        p = sc->strbuff;
        snprintf(p, STRBUFFSIZE, "#<PORT>");
    }
    else if (is_number(gc, l))
    {
        p = sc->strbuff;
        if (f <= 1 || f == 10)  /* f is the base for numbers if > 1 */
        {
            if (num_is_integer(gc, l))
            {
                snprintf(p, STRBUFFSIZE, "%ld", ivalue_unchecked(l));
            }
            else
            {
                snprintf(p, STRBUFFSIZE, "%.10g", rvalue_unchecked(l));
                /* r5rs says there must be a '.' (unless 'e'?) */
                f = strcspn(p, ".e");
                if (p[f] == 0)
                {
                    p[f] = '.'; /* not found, so add '.0' at the end */
                    p[f + 1] = '0';
                    p[f + 2] = 0;
                }
            }
        }
        else
        {
            long v = ivalue(gc, l);

            if (f == 16)
            {
                if (v >= 0)
                    snprintf(p, STRBUFFSIZE, "%lx", v);
                else
                    snprintf(p, STRBUFFSIZE, "-%lx", -v);
            }
            else if (f == 8)
            {
                if (v >= 0)
                    snprintf(p, STRBUFFSIZE, "%lo", v);
                else
                    snprintf(p, STRBUFFSIZE, "-%lo", -v);
            }
            else if (f == 2)
            {
                unsigned long b = (v < 0) ? -v : v;

                p = &p[STRBUFFSIZE - 1];
                *p = 0;
                do
                {
                    *--p = (b & 1) ? '1' : '0';
                    b >>= 1;
                } while (b != 0);
                if (v < 0)
                    *--p = '-';
            }
        }
    }
    else if (is_string(gc, l))
    {
        if (!f)
        {
            p = strvalue(l);
        }
        else
        {                       /* Hack, uses the fact that printing is needed */
            *pp = sc->strbuff;
            *plen = 0;
            printslashstring(scheme_object, strvalue(l), strlength(l));
            return;
        }
    }
    else if (is_character(gc, l))
    {
        int c = charvalue(l);

        p = sc->strbuff;
        if (!f)
        {
            p[0] = c;
            p[1] = 0;
        }
        else
        {
            switch (c)
            {
                case ' ':
                    snprintf(p, STRBUFFSIZE, "#\\space");
                    break;
                case '\n':
                    snprintf(p, STRBUFFSIZE, "#\\newline");
                    break;
                case '\r':
                    snprintf(p, STRBUFFSIZE, "#\\return");
                    break;
                case '\t':
                    snprintf(p, STRBUFFSIZE, "#\\tab");
                    break;
                default:
#if USE_ASCII_NAMES
                    if (c == 127)
                    {
                        snprintf(p, STRBUFFSIZE, "#\\del");
                        break;
                    }
                    else if (c < 32)
                    {
                        snprintf(p, STRBUFFSIZE, "#\\%s", charnames[c]);
                        break;
                    }
#else
                    if (c < 32)
                    {
                        snprintf(p, STRBUFFSIZE, "#\\x%x", c);
                        break;
                    }
#endif
                    snprintf(p, STRBUFFSIZE, "#\\%c", c);
                    break;
            }
        }
    }
    else if (is_symbol(gc, l))
    {
        p = symname(gc, l);
    }
    else if (is_proc(l))
    {
        p = sc->strbuff;
        snprintf(p, STRBUFFSIZE, "#<%s PROCEDURE %ld>", procname(gc, l), procnum(gc, l));
    }
    else if (is_macro(l))
    {
        p = "#<MACRO>";
    }
    else if (is_closure(l))
    {
        p = "#<CLOSURE>";
    }
    else if (is_promise(l))
    {
        p = "#<PROMISE>";
    }
    else if (is_foreign(l))
    {
        p = sc->strbuff;
        snprintf(p, STRBUFFSIZE, "#<FOREIGN PROCEDURE %ld>", procnum(gc, l));
    }
    else if (is_continuation(l))
    {
        p = "#<CONTINUATION>";
    }
    else if (is_mutex(gc, l))
    {
        p = "#<MUTEX>";
    }
    else if (is_scheme(gc, l))
    {
        scheme *sc = *((scheme **) object_get_byte(l, 0));
        uint64_t thread_id = *((uint64_t *) (&sc->thread));

        p = sc->strbuff;
        snprintf(p, STRBUFFSIZE, "#<THREAD %" PRIx64 ">", thread_id);
    }
    else
    {
        p = "#<ERROR>";
    }
    *pp = p;
    *plen = strlen(p);
}

/* ========== Routines for Evaluation Cycle ========== */

/* make closure. c is code. e is environment */
#define mk_closure(scheme_object, c, e) get_cell((scheme_object), (c), (e), (unsigned) T_CLOSURE)

/* make continuation. */
#define mk_continuation(scheme_object, d) get_cell((scheme_object), NULL, (d), (unsigned) T_CONTINUATION)

static pointer list_star(pointer _scheme_object, pointer _d)
scope(scheme_object, sc, gc, d, p, q, sc_nil)
{
    local(d)      = _d;
    local(sc_nil) = get(local(scheme_object), SCHEME_NIL);

    if (cdr(local(d)) == local(sc_nil))
    {
        scope_return car(local(d));
    }

    local(p) = cons(local(scheme_object), car(local(d)), cdr(local(d)));
    local(q) = local(p);

    while (cdr(cdr(local(p))) != local(sc_nil))
    {
        local(d) = cons(local(scheme_object), car(local(p)), cdr(local(p)));
        if (cdr(cdr(local(p))) != local(sc_nil))
        {
            local(p) = cdr(local(d));
        }
    }

    set_cdr(gc, local(p), car(cdr(local(p))));
    scope_return local(q);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

/* reverse list -- produce new list */
static pointer reverse(pointer scheme_object, pointer a)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    pointer p = object_get_slot(gc, scheme_object, SCHEME_NIL);

    gc_track_reference(gc, &a, REFERENCE_TRACKING_MODE_HANDLE);
    for (; is_pair(gc, a); a = pair_cdr(gc, a))
    {
        p = cons(scheme_object, pair_car(gc, a), p);
    }
    gc_untrack_reference(gc, &a);

    return p;
}

/* reverse list --- in-place */
static pointer reverse_in_place(pointer scheme_object, pointer term, pointer list) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);

    pointer p = list, result = term, q;

    while (p != sc_nil)
    {
        q = pair_cdr(gc, p);
        set_cdr(gc, p, result);
        result = p;
        p = q;
    }
    return (result);
}

/* append list -- produce new list (in reverse order) */
static pointer revappend(pointer _scheme_object, pointer a, pointer b)
scope(scheme_object, sc, gc, result, p, sc_nil)
{
    local(sc_nil) = get(local(scheme_object), SCHEME_NIL);
    local(result) = a;
    local(p)      = b;

    while (is_pair(gc, local(p)))
    {
        local(result) = cons(local(scheme_object), car(local(p)), local(result));
        local(p) = cdr(local(p));
    }

    if (local(p) == local(sc_nil))
    {
        scope_return local(result);
    }

    scope_return get(local(scheme_object), SCHEME_HASH_F); /* signal an error */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

/* equivalence of atoms */
#if defined(NO_DEBUG)
#undef eqv
int eqv(pointer a, pointer b) // Safe.
#define eqv(gc, a, b) eqv(a, b)
#else
int eqv(GC *gc, pointer a, pointer b) // Safe.
#endif // defined(NO_DEBUG).
{
    if (is_string(gc, a))
    {
        if (is_string(gc, b))
            return (strvalue(a) == strvalue(b));
        else
            return (0);
    }
    else if (is_number(gc, a))
    {
        if (is_number(gc, b))
        {
            if (num_is_integer(gc, a) == num_is_integer(gc, b))
                return num_eq(nvalue(a), nvalue(b));
        }
        return (0);
    }
    else if (is_character(gc, a))
    {
        if (is_character(gc, b))
            return charvalue(a) == charvalue(b);
        else
            return (0);
    }
    else if (is_port(gc, a))
    {
        if (is_port(gc, b))
            return a == b;
        else
            return (0);
    }
    else if (is_proc(a))
    {
        if (is_proc(b))
            return procnum(gc, a) == procnum(gc, b);
        else
            return (0);
    }
    else
    {
        return (a == b);
    }
}

/* true or false value macro */
/* () is #t in R5RS */
#define is_true(p) ((p) != slot(sc_f))
#define is_false(p) ((p) == slot(sc_f))

/* ========== Environment implementation  ========== */

static INLINE void new_frame_in_env(pointer scheme_object, pointer old_env)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    pointer new_env_pair = immutable_cons(scheme_object, sc_nil, old_env);
    object_set_slot(gc, scheme_object, SCHEME_ENVIR, new_env_pair);
    setenvironment(gc, new_env_pair);
    gc_untrack_reference(gc, &scheme_object);
}

static INLINE void new_slot_spec_in_env(pointer scheme_object, pointer env, pointer variable, pointer value)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    gc_track_reference(gc, &env, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer p1 = immutable_cons(scheme_object, variable, value);
    gc_track_reference(gc, &p1, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer p2 = immutable_cons(scheme_object, p1, pair_car(gc, env));
    set_car(gc, env, p2);

    gc_untrack_reference(gc, &p1);
    gc_untrack_reference(gc, &env);
    gc_untrack_reference(gc, &scheme_object);
}

static pointer find_slot_in_env(pointer scheme_object, pointer env, pointer hdl, int all) // Safe.
{
    pointer x, y;

    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);

    for (x = env; x != sc_nil; x = pair_cdr(gc, x))
    {
        for (y = pair_car(gc, x); y != sc_nil; y = pair_cdr(gc, y))
        {
            if (pair_car(gc, pair_car(gc, y)) == hdl)
            {
                break;
            }
        }
        if (y != sc_nil)
        {
            break;
        }
        if (!all)
        {
            return sc_nil;
        }
    }
    if (x != sc_nil)
    {
        return pair_car(gc, y);
    }
    return sc_nil;
}

static INLINE void new_slot_in_env(pointer scheme_object, pointer variable, pointer value)
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    pointer sc_envir = object_get_slot(gc, scheme_object, SCHEME_ENVIR);
    new_slot_spec_in_env(scheme_object, sc_envir, variable, value);
}

static INLINE void set_slot_in_env(GC *gc, pointer slot, pointer value) // Safe.
{
    set_cdr(gc, slot, value);
}

#if defined(NO_DEBUG)
static INLINE pointer slot_value_in_env(pointer slot) // Safe.
#define slot_value_in_env(gc, slot) slot_value_in_env(slot)
#else
static INLINE pointer slot_value_in_env(GC *gc, pointer slot) // Safe.
#endif // defined(NO_DEBUG).
{
    return pair_cdr(gc, slot);
}

/* ========== Evaluation Cycle ========== */


static pointer _Error_1(pointer _scheme_object, const char *s, pointer _a)
scope(scheme_object, sc, gc, sc_nil, sc_quote, sc_code, a, x, hdl, p1, p2, new_string)
{
    const char *str = s;

    local(sc_nil)   = get(local(scheme_object), SCHEME_NIL);
    local(sc_quote) = get(local(scheme_object), SCHEME_QUOTE);
    local(a)        = _a;

#if USE_ERROR_HOOK
    local(hdl) = get(local(scheme_object), SCHEME_ERROR_HOOK);
#endif

#if SHOW_ERROR_LINE
    char sbuf[STRBUFFSIZE];

    /* make sure error is not in REPL */
    if (sc->load_stack[sc->file_i].kind & port_file && sc->load_stack[sc->file_i].rep.stdio.file != stdin)
    {
        int ln = sc->load_stack[sc->file_i].rep.stdio.curr_line;
        const char *fname = sc->load_stack[sc->file_i].rep.stdio.filename;

        /* should never happen */
        if (!fname)
            fname = "<unknown>";

        /* we started from 0 */
        ln++;
        snprintf(sbuf, STRBUFFSIZE, "(%s : %i) %s", fname, ln, s);

        str = (const char *) sbuf;
    }
#endif

#if USE_ERROR_HOOK
    local(x) = find_slot_in_env(local(scheme_object), get(local(scheme_object), SCHEME_ENVIR), local(hdl), 1);
    if (local(x) != local(sc_nil))
    {
        if (local(a) != 0)
        {
            local(p1) = cons(local(scheme_object), local(a), local(sc_nil));
            local(p2) = cons(local(scheme_object), local(sc_quote), local(p1));
            pointer p3 = cons(local(scheme_object), local(p2), local(sc_nil));
            set(local(scheme_object), SCHEME_CODE, p3);
        }
        else
        {
            set(local(scheme_object), SCHEME_CODE, local(sc_nil));
        }

        local(new_string) = mk_string(local(scheme_object), str);
        setimmutable(gc, local(new_string));

        local(sc_code) = get(local(scheme_object), SCHEME_CODE);
        local(sc_code) = cons(local(scheme_object), local(new_string), local(sc_code));
        set(local(scheme_object), SCHEME_CODE, local(sc_code));

        local(sc_code) = cons(local(scheme_object), slot_value_in_env(gc, local(x)), local(sc_code));
        set(local(scheme_object), SCHEME_CODE, local(sc_code));

        sc->op = (int) OP_EVAL;
        scope_return get(local(scheme_object), SCHEME_HASH_T);
    }
#endif

    if (local(a) != 0)
    {
        pointer new_args = cons(local(scheme_object), local(a), local(sc_nil));
        set(local(scheme_object), SCHEME_ARGS, new_args);
    }
    else
    {
        set(local(scheme_object), SCHEME_ARGS, local(sc_nil));
    }

    local(new_string) = mk_string(local(scheme_object), str);
    setimmutable(gc, local(new_string));

    pointer new_args = cons(local(scheme_object), local(new_string), get(local(scheme_object), SCHEME_ARGS));
    set(local(scheme_object), SCHEME_ARGS, new_args);

    sc->op = (int) OP_ERR0;
    scope_return get(local(scheme_object), SCHEME_HASH_T);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

#define Error_1(s, a) scope_return _Error_1(local(scheme_object), s, a)
#define Error_0(s) scope_return _Error_1(local(scheme_object), s, 0)

/* Too small to turn into function */
#define  BEGIN     do {
#define  END  } while (0)
#define s_goto(a) \
    BEGIN \
    sc->op = (int) (a); \
    scope_return slot(sc_t); \
    END

#define s_return(a) scope_return _s_return(local(scheme_object), a)

static INLINE void dump_stack_reset(pointer scheme_object) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    object_set_slot(gc, scheme_object, SCHEME_DUMP, sc_nil);
}

static INLINE void dump_stack_initialize(pointer scheme_object) // Safe.
{
    dump_stack_reset(scheme_object);
}

static pointer _s_return(pointer _scheme_object, pointer a) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(_scheme_object, 0));
    GC *gc = sc->gc;

    object_set_slot(gc, _scheme_object, SCHEME_VALUE, a);

    pointer sc_nil  = object_get_slot(gc, _scheme_object, SCHEME_NIL),
            sc_dump = object_get_slot(gc, _scheme_object, SCHEME_DUMP);

    if (sc_dump == sc_nil)
        return sc_nil;

    sc->op = ivalue(gc, pair_car(gc, sc_dump));

    scope(scheme_object, sc, gc)
    {
        set(local(scheme_object), SCHEME_ARGS, cadr(sc_dump));
        set(local(scheme_object), SCHEME_ENVIR, caddr(sc_dump));
        set(local(scheme_object), SCHEME_CODE, cadddr(sc_dump));
        set(local(scheme_object), SCHEME_DUMP, cddddr(sc_dump));
    } endscope;

    return object_get_slot(gc, _scheme_object, SCHEME_HASH_T);
}

static void s_save(pointer _scheme_object, enum scheme_opcodes op, pointer _args, pointer _code)
scope(scheme_object, sc, gc, args, code, sc_envir, sc_dump, new_integer, t1, t2, t3, t4)
{
    local(args)     = _args;
    local(code)     = _code;
    local(sc_envir) = get(local(scheme_object), SCHEME_ENVIR);
    local(sc_dump)  = get(local(scheme_object), SCHEME_DUMP);

    local(new_integer) = mk_integer(gc, (long) (op));

    local(t1) = cons(local(scheme_object), local(code), local(sc_dump));
    local(t2) = cons(local(scheme_object), local(sc_envir), local(t1)),
    local(t3) = cons(local(scheme_object), local(args), local(t2)),
    local(t4) = cons(local(scheme_object), local(new_integer), local(t3));

    set(local(scheme_object), SCHEME_DUMP, local(t4));
} endscope;

#define s_retbool(tf) s_return((tf) ? slot(sc_t) : slot(sc_f))


static pointer opexe_0(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_CODE, sc_code);
    defslot(local(scheme_object), SCHEME_INPORT, sc_inport);
    defslot(local(scheme_object), SCHEME_SAVE_INPORT, sc_save_inport);
    defslot(local(scheme_object), SCHEME_OUTPORT, sc_outport);
    defslot(local(scheme_object), SCHEME_LOADPORT, sc_loadport);
    defslot(local(scheme_object), SCHEME_VALUE, sc_value);
    defslot(local(scheme_object), SCHEME_ENVIR, sc_envir);
    defslot(local(scheme_object), SCHEME_GLOBAL_ENV, sc_global_env);
    defslot(local(scheme_object), SCHEME_COMPILE_HOOK, sc_compile_hook);
    defslot(local(scheme_object), SCHEME_LAMBDA, sc_lambda);

    switch (op)
    {
        case OP_LOAD:          /* load */
        {
            const char *file_name = strvalue(car(slot(sc_args)));
            if (file_interactive(local(scheme_object)))
            {
                port *p = *((port **) object_get_byte(slot(sc_outport), 0));
                fprintf(p->rep.stdio.file, "Loading %s\n", file_name);
            }
            if (!file_push(local(scheme_object), file_name))
            {
                Error_1("unable to open", car(slot(sc_args)));
            }
            else
            {
                pointer new_integer = mk_integer(gc, sc->file_i);
                set(local(scheme_object), SCHEME_ARGS, new_integer);
                s_goto(OP_T0LVL);
            }
        }

        case OP_T0LVL:         /* top level */
        {
            port *p = *((port **) object_get_byte(slot(sc_loadport), 0));
            /* If we reached the end of file, this loop is done. */
            if (p->kind & port_saw_EOF)
            {
                if (sc->file_i == 0)
                {
                    set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
                    s_goto(OP_QUIT);
                }
                else
                {
                    file_pop(local(scheme_object));
                    s_return(slot(sc_value));
                }
                /* NOTREACHED */
            }

            /* If interactive, be nice to user. */
            if (file_interactive(local(scheme_object)))
            {
                set(local(scheme_object), SCHEME_ENVIR, slot(sc_global_env));
                dump_stack_reset(local(scheme_object));
                putstr(local(scheme_object), "\n");
                putstr(local(scheme_object), prompt);
            }

            /* Set up another iteration of REPL */
            sc->nesting = 0;
            set(local(scheme_object), SCHEME_SAVE_INPORT, slot(sc_inport));
            set(local(scheme_object), SCHEME_INPORT, slot(sc_loadport));

            s_save(local(scheme_object), OP_T0LVL, slot(sc_nil), slot(sc_nil));
            s_save(local(scheme_object), OP_VALUEPRINT, slot(sc_nil), slot(sc_nil));
            s_save(local(scheme_object), OP_T1LVL, slot(sc_nil), slot(sc_nil));

            s_goto(OP_READ_INTERNAL);
        }

        case OP_T1LVL:         /* top level */
            set(local(scheme_object), SCHEME_CODE, slot(sc_value));
            set(local(scheme_object), SCHEME_INPORT, slot(sc_save_inport));
            s_goto(OP_EVAL);

        case OP_READ_INTERNAL: /* internal read */
            sc->tok = token(local(scheme_object));
            if (sc->tok == TOK_EOF)
            {
                s_return(get(local(scheme_object), SCHEME_EOF_OBJ));
            }
            s_goto(OP_RDSEXPR);

        case OP_GENSYM:
        {
            pointer new_symbol = gensym(local(scheme_object));
            s_return(new_symbol);
        }

        case OP_VALUEPRINT:    /* print evaluation result */
            /* OP_VALUEPRINT is always pushed, because when changing from
             * non-interactive to interactive mode, it needs to be
             * already on the stack */
            if (sc->tracing)
            {
                putstr(local(scheme_object), "\nGives: ");
            }
            if (file_interactive(local(scheme_object)))
            {
                sc->print_flag = 1;
                set(local(scheme_object), SCHEME_ARGS, slot(sc_value));
                s_goto(OP_P0LIST);
            }
            else
            {
                s_return(slot(sc_value));
            }

        case OP_EVAL:          /* main part of evaluation */
#if USE_TRACING
            if (sc->tracing)
            {
                /*s_save(sc,OP_VALUEPRINT,sc->NIL,sc->NIL); */
                s_save(local(scheme_object), OP_REAL_EVAL, slot(sc_args), slot(sc_code));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_code));
                putstr(local(scheme_object), "\nEval: ");
                s_goto(OP_P0LIST);
            }
            /* fall through */
        case OP_REAL_EVAL:
#endif
        {
            if (is_symbol(gc, slot(sc_code)))
            {                   /* symbol */
                local(x) = find_slot_in_env(local(scheme_object),
                                            slot(sc_envir),
                                            slot(sc_code),
                                            1);
                if (local(x) != slot(sc_nil))
                {
                    s_return(slot_value_in_env(gc, local(x)));
                }
                else
                {
                    Error_1("eval: unbound variable:", slot(sc_code));
                }
            }
            else if (is_pair(gc, slot(sc_code)))
            {
                if (is_syntax(local(x) = car(slot(sc_code))))
                {               /* SYNTAX */
                    setslot(sc_code, cdr(slot(sc_code)));
                    s_goto(syntaxnum(gc, local(x)));
                }
                else
                {               /* first, eval top element and eval arguments */
                    s_save(local(scheme_object), OP_E0ARGS, slot(sc_nil), slot(sc_code));
                    /* If no macros => s_save(sc,OP_E1ARGS, sc->NIL, cdr(sc->code)); */
                    setslot(sc_code, car(slot(sc_code)));
                    s_goto(OP_EVAL);
                }
            }
            else
            {
                s_return(slot(sc_code));
            }
        }

        case OP_E0ARGS:        /* eval arguments */
        {
            if (is_macro(slot(sc_value)))
            {                   /* macro expansion */
                s_save(local(scheme_object), OP_DOMACRO, slot(sc_nil), slot(sc_nil));
                pointer new_pair = cons(local(scheme_object), slot(sc_code), slot(sc_nil));
                set(local(scheme_object), SCHEME_ARGS, new_pair);
                set(local(scheme_object), SCHEME_CODE, slot(sc_value));
                s_goto(OP_APPLY);
            }
            else
            {
                set(local(scheme_object), SCHEME_CODE, cdr(slot(sc_code)));
                s_goto(OP_E1ARGS);
            }
        }

        case OP_E1ARGS:        /* eval arguments */
        {
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_args));
            setslot(sc_args, local(x));

            if (is_pair(gc, slot(sc_code)))
            {                   /* continue */
                s_save(local(scheme_object), OP_E1ARGS, slot(sc_args), cdr(slot(sc_code)));
                setslot(sc_code, car(slot(sc_code)));
                setslot(sc_args, slot(sc_nil));
                s_goto(OP_EVAL);
            }
            else
            {                   /* end */
                local(x) = reverse_in_place(local(scheme_object), slot(sc_nil), slot(sc_args));
                setslot(sc_args, local(x));

                set(local(scheme_object), SCHEME_CODE, car(slot(sc_args)));
                set(local(scheme_object), SCHEME_ARGS, cdr(slot(sc_args)));

                s_goto(OP_APPLY);
            }
        }

#if USE_TRACING
        case OP_TRACING:
        {
            int tr = sc->tracing;

            sc->tracing = ivalue(gc, car(slot(sc_args)));

            pointer new_pair = mk_integer(gc, tr);
            s_return(new_pair);
        }
#endif

        case OP_APPLY:         /* apply 'code' to 'args' */
#if USE_TRACING
            if (sc->tracing)
            {
                s_save(local(scheme_object), OP_REAL_APPLY, slot(sc_args), slot(sc_code));

                sc->print_flag = 1;
                /*  sc->args=cons(sc,sc->code,sc->args); */
                putstr(local(scheme_object), "\nApply to: ");
                s_goto(OP_P0LIST);
            }
            /* fall through */
        case OP_REAL_APPLY:
#endif
        {
            if (is_proc(slot(sc_code)))
            {
                s_goto(procnum(gc, slot(sc_code)));  /* PROCEDURE */
            }
            else if (is_foreign(slot(sc_code)))
            {
                /* Keep nested calls from GC'ing the arglist */
                /* push_recent_alloc(sc,sc->args,sc->NIL); */

                foreign_func *ff = (foreign_func *) object_get_byte(slot(sc_code), 0);
                local(x) = (*ff)(local(scheme_object), slot(sc_args));

                s_return(local(x));
            }
            else if (is_closure(slot(sc_code)) || is_macro(slot(sc_code)) || is_promise(slot(sc_code)))
            {                   /* CLOSURE */
                /* Should not accept promise */
                /* make environment */

                new_frame_in_env(local(scheme_object), closure_env(gc, slot(sc_code)));

                for (local(x) = car(closure_code(gc, slot(sc_code))), local(y) = slot(sc_args);
                     is_pair(gc, local(x));
                     local(x) = cdr(local(x)), local(y) = cdr(local(y)))
                {
                    if (local(y) == slot(sc_nil))
                    {
                        Error_0("not enough arguments");
                    }
                    else
                    {
                        new_slot_in_env(local(scheme_object), car(local(x)), car(local(y)));
                    }
                }

                if (local(x) == slot(sc_nil))
                {
                    /*--
                     * if (y != sc->NIL) {
                     *   Error_0(sc,"too many arguments");
                     * }
                     */
                }
                else if (is_symbol(gc, local(x)))
                {
                    new_slot_in_env(local(scheme_object), local(x), local(y));
                }
                else
                {
                    Error_1("syntax error in closure: not a symbol:", local(x));
                }

                setslot(sc_code, cdr(closure_code(gc, slot(sc_code))));
                setslot(sc_args, slot(sc_nil));

                s_goto(OP_BEGIN);
            }
            else if (is_continuation(slot(sc_code)))
            {                   /* CONTINUATION */
                set(local(scheme_object), SCHEME_DUMP, cont_dump(slot(sc_code)));
                s_return(slot(sc_args) != slot(sc_nil) ? car(slot(sc_args)) : slot(sc_nil));
            }
            else
            {
                // Error_0("illegal function");
                Error_1("illegal function", slot(sc_code));
            }
        }

        case OP_DOMACRO:       /* do macro */
        {
            set(local(scheme_object), SCHEME_CODE, slot(sc_value));
            s_goto(OP_EVAL);
        }

#if 1
        case OP_LAMBDA:        /* lambda */
            /* If the hook is defined, apply it to sc->code, otherwise
             * set sc->value fall thru */
        {
            local(x) = find_slot_in_env(local(scheme_object), slot(sc_envir), slot(sc_compile_hook), 1);

            if (local(x) == slot(sc_nil))
            {
                set(local(scheme_object), SCHEME_VALUE, slot(sc_code));
                /* Fallthru */
            }
            else
            {
                s_save(local(scheme_object), OP_LAMBDA1, slot(sc_args), slot(sc_code));

                pointer new_pair = cons(local(scheme_object), slot(sc_code), slot(sc_nil));
                set(local(scheme_object), SCHEME_ARGS, new_pair);
                set(local(scheme_object), SCHEME_CODE, slot_value_in_env(gc, local(x)));

                s_goto(OP_APPLY);
            }
        }

        case OP_LAMBDA1:
        {
            pointer new_closure = mk_closure(local(scheme_object), slot(sc_value), slot(sc_envir));
            s_return(new_closure);
        }

#else
        case OP_LAMBDA:        /* lambda */
        {
            pointer new_closure = mk_closure(local(scheme_object), slot(sc_code), slot(sc_envir));
            s_return(new_closure);
        }

#endif

        case OP_MKCLOSURE:     /* make-closure */
        {
            local(x) = car(slot(sc_args));
            if (car(local(x)) == slot(sc_lambda))
            {
                local(x) = cdr(local(x));
            }

            if (cdr(slot(sc_args)) == slot(sc_nil))
            {
                local(y) = slot(sc_envir);
            }
            else
            {
                local(y) = cadr(slot(sc_args));
            }

            pointer new_closure = mk_closure(local(scheme_object), local(x), local(y));
            s_return(new_closure);
        }

        case OP_QUOTE:         /* quote */
        {
            s_return(car(slot(sc_code)));
        }

        case OP_DEF0:          /* define */
        {
            if (car(slot(sc_code)) == slot(sc_nil))
            {
                Error_0("define: wrong number of arguments");
            }

            if (is_immutable(car(slot(sc_code))))
            {
                Error_1("define: unable to alter immutable", car(slot(sc_code)));
            }

            if (is_pair(gc, car(slot(sc_code))))
            {
                local(x) = caar(slot(sc_code));
                pointer p1 = cons(local(scheme_object), cdar(slot(sc_code)), cdr(slot(sc_code))),
                        p2 = cons(local(scheme_object), slot(sc_lambda), p1);
                set(local(scheme_object), SCHEME_CODE, p2);
            }
            else
            {
                local(x) = car(slot(sc_code));
                set(local(scheme_object), SCHEME_CODE, cadr(slot(sc_code)));
            }

            if (!is_symbol(gc, local(x)))
            {
                Error_0("variable is not a symbol");
            }

            s_save(local(scheme_object), OP_DEF1, slot(sc_nil), local(x));
            s_goto(OP_EVAL);
        }

        case OP_DEF1:          /* define */
        {
            local(x) = find_slot_in_env(local(scheme_object), slot(sc_envir), slot(sc_code), 0);
            if (local(x) != slot(sc_nil))
            {
                set_slot_in_env(gc, local(x), slot(sc_value));
            }
            else
            {
                new_slot_in_env(local(scheme_object), slot(sc_code), slot(sc_value));
            }

            s_return(slot(sc_code));
        }

        case OP_DEFP:          /* defined? */
            local(x) = slot(sc_envir);
            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                local(x) = cadr(slot(sc_args));
            }
            s_retbool(find_slot_in_env(local(scheme_object), local(x), car(slot(sc_args)), 1) != slot(sc_nil));

        case OP_SET0:          /* set! */
            if (is_immutable(car(slot(sc_code))))
            {
                Error_1("set!: unable to alter immutable variable", car(slot(sc_code)));
            }

            s_save(local(scheme_object), OP_SET1, slot(sc_nil), car(slot(sc_code)));
            setslot(sc_code, cadr(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_SET1:          /* set! */
            local(y) = find_slot_in_env(local(scheme_object), slot(sc_envir), slot(sc_code), 1);
            if (local(y) != slot(sc_nil))
            {
                set_slot_in_env(gc, local(y), slot(sc_value));
                s_return(slot(sc_value));
            }
            else
            {
                Error_1("set!: unbound variable:", slot(sc_code));
            }

        case OP_BEGIN:         /* begin */
            if (!is_pair(gc, slot(sc_code)))
            {
                s_return(slot(sc_code));
            }
            if (cdr(slot(sc_code)) != slot(sc_nil))
            {
                s_save(local(scheme_object), OP_BEGIN, slot(sc_nil), cdr(slot(sc_code)));
            }

            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_IF0:           /* if */
            s_save(local(scheme_object), OP_IF1, slot(sc_nil), cdr(slot(sc_code)));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_IF1:           /* if */
            if (is_true(slot(sc_value)))
            {
                setslot(sc_code, car(slot(sc_code)));
            }
            else
            {
                setslot(sc_code, cadr(slot(sc_code))); // (if #f 1) ==> () because
                                                       // car(sc->NIL) = sc->NIL
            }
            s_goto(OP_EVAL);

        case OP_LET0:          /* let */
            set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
            set(local(scheme_object), SCHEME_VALUE, slot(sc_code));
            set(local(scheme_object), SCHEME_CODE, is_symbol(gc, car(slot(sc_code))) ? cadr(slot(sc_code)) : car(slot(sc_code)));
            s_goto(OP_LET1);

        case OP_LET1:          /* let (calculate parameters) */
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_args));
            setslot(sc_args, local(x));
            set(local(scheme_object), SCHEME_ARGS, slot(sc_args));

            if (is_pair(gc, slot(sc_code)))
            {                   /* continue */
                if (!is_pair(gc, car(slot(sc_code))) || !is_pair(gc, cdar(slot(sc_code))))
                {
                    Error_1("Bad syntax of binding spec in let :", car(slot(sc_code)));
                }
                s_save(local(scheme_object), OP_LET1, slot(sc_args), cdr(slot(sc_code)));

                setslot(sc_code, cadar(slot(sc_code)));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
                s_goto(OP_EVAL);
            }
            else
            {                   /* end */
                setslot(sc_args, reverse_in_place(local(scheme_object), slot(sc_nil), slot(sc_args)));

                set(local(scheme_object), SCHEME_CODE, car(slot(sc_args)));
                set(local(scheme_object), SCHEME_ARGS, cdr(slot(sc_args)));
                s_goto(OP_LET2);
            }

        case OP_LET2:          /* let */
            new_frame_in_env(local(scheme_object), slot(sc_envir));
            for (local(x) = is_symbol(gc, car(slot(sc_code))) ? cadr(slot(sc_code)) : car(slot(sc_code)), local(y) = slot(sc_args);
                 local(y) != slot(sc_nil);
                 local(x) = cdr(local(x)), local(y) = cdr(local(y)))
            {
                new_slot_in_env(local(scheme_object), caar(local(x)), car(local(y)));
            }
            if (is_symbol(gc, car(slot(sc_code))))
            {                   /* named let */
                setslot(sc_args, slot(sc_nil));

                for (local(x) = cadr(slot(sc_code)); local(x) != slot(sc_nil); local(x) = cdr(local(x)))
                {
                    if (!is_pair(gc, local(x)))
                        Error_1("Bad syntax of binding in let :", local(x));
                    if (!is_list(local(scheme_object), car(local(x))))
                        Error_1("Bad syntax of binding in let :", car(local(x)));

                    local(y) = cons(local(scheme_object), caar(local(x)), slot(sc_args));
                    setslot(sc_args, local(y));
                    set(local(scheme_object), SCHEME_ARGS, slot(sc_args));
                }

                local(x) = cons(local(scheme_object),
                                reverse_in_place(local(scheme_object), slot(sc_nil), slot(sc_args)),
                                cddr(slot(sc_code)));
                local(x) = mk_closure(local(scheme_object), local(x), slot(sc_envir));
                new_slot_in_env(local(scheme_object), car(slot(sc_code)), local(x));

                setslot(sc_code, cddr(slot(sc_code)));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
            }
            else
            {
                setslot(sc_code, cdr(slot(sc_code)));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
            }
            s_goto(OP_BEGIN);

        case OP_LET0AST:       /* let* */
            if (car(slot(sc_code)) == slot(sc_nil))
            {
                new_frame_in_env(local(scheme_object), slot(sc_envir));
                setslot(sc_code, cdr(slot(sc_code)));
                s_goto(OP_BEGIN);
            }
            if (!is_pair(gc, car(slot(sc_code))) || !is_pair(gc, caar(slot(sc_code))) || !is_pair(gc, cdaar(slot(sc_code))))
            {
                Error_1("Bad syntax of binding spec in let* :", car(slot(sc_code)));
            }
            s_save(local(scheme_object), OP_LET1AST, cdr(slot(sc_code)), car(slot(sc_code)));
            setslot(sc_code, cadaar(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_LET1AST:       /* let* (make new frame) */
            new_frame_in_env(local(scheme_object), slot(sc_envir));
            s_goto(OP_LET2AST);

        case OP_LET2AST:       /* let* (calculate parameters) */
            new_slot_in_env(local(scheme_object), caar(slot(sc_code)), slot(sc_value));
            setslot(sc_code, cdr(slot(sc_code)));
            if (is_pair(gc, slot(sc_code)))
            {                   /* continue */
                s_save(local(scheme_object), OP_LET2AST, slot(sc_args), slot(sc_code));
                setslot(sc_code, cadar(slot(sc_code)));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
                s_goto(OP_EVAL);
            }
            else
            {                   /* end */
                set(local(scheme_object), SCHEME_CODE, slot(sc_args));
                set(local(scheme_object), SCHEME_ARGS, slot(sc_nil));
                s_goto(OP_BEGIN);
            }

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }

    scope_return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_1(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_CODE, sc_code);
    defslot(local(scheme_object), SCHEME_VALUE, sc_value);
    defslot(local(scheme_object), SCHEME_ENVIR, sc_envir);
    defslot(local(scheme_object), SCHEME_LAMBDA, sc_lambda);
    defslot(local(scheme_object), SCHEME_FEED_TO, sc_feed_to);
    defslot(local(scheme_object), SCHEME_QUOTE, sc_quote);
    defslot(local(scheme_object), SCHEME_DUMP, sc_dump);

    switch (op)
    {
        case OP_LET0REC:       /* letrec */
            new_frame_in_env(local(scheme_object), slot(sc_envir));
            setslot(sc_args, slot(sc_nil));
            setslot(sc_value, slot(sc_code));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_LET1REC);

        case OP_LET1REC:       /* letrec (calculate parameters) */
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_args));
            setslot(sc_args, local(x));
            if (is_pair(gc, slot(sc_code)))
            {                   /* continue */
                if (!is_pair(gc, car(slot(sc_code))) || !is_pair(gc, cdar(slot(sc_code))))
                {
                    Error_1("Bad syntax of binding spec in letrec :", car(slot(sc_code)));
                }
                s_save(local(scheme_object), OP_LET1REC, slot(sc_args), cdr(slot(sc_code)));
                setslot(sc_code, cadar(slot(sc_code)));
                setslot(sc_args, slot(sc_nil));
                s_goto(OP_EVAL);
            }
            else
            {                   /* end */
                setslot(sc_args, reverse_in_place(local(scheme_object), slot(sc_nil), slot(sc_args)));
                setslot(sc_code, car(slot(sc_args)));
                setslot(sc_args, cdr(slot(sc_args)));
                s_goto(OP_LET2REC);
            }

        case OP_LET2REC:       /* letrec */
            for (local(x) = car(slot(sc_code)), local(y) = slot(sc_args);
                 local(y) != slot(sc_nil);
                 local(x) = cdr(local(x)), local(y) = cdr(local(y)))
            {
                new_slot_in_env(local(scheme_object), caar(local(x)), car(local(y)));
            }
            setslot(sc_code, cdr(slot(sc_code)));
            setslot(sc_args, slot(sc_nil));
            s_goto(OP_BEGIN);

        case OP_COND0:         /* cond */
            if (!is_pair(gc, slot(sc_code)))
            {
                Error_0("syntax error in cond");
            }
            s_save(local(scheme_object), OP_COND1, slot(sc_nil), slot(sc_code));
            setslot(sc_code, caar(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_COND1:         /* cond */
            if (is_true(slot(sc_value)))
            {
                setslot(sc_code, cdar(slot(sc_code)));
                if (slot(sc_code) == slot(sc_nil))
                {
                    s_return(slot(sc_value));
                }
                if (car(slot(sc_code)) == slot(sc_feed_to))
                {
                    if (!is_pair(gc, cdr(slot(sc_code))))
                    {
                        Error_0("syntax error in cond");
                    }
                    local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
                    local(x) = cons(local(scheme_object), slot(sc_quote), local(x));

                    local(x) = cons(local(scheme_object), local(x), slot(sc_nil));
                    local(x) = cons(local(scheme_object), cadr(slot(sc_code)), local(x));
                    setslot(sc_code, local(x));
                    s_goto(OP_EVAL);
                }
                s_goto(OP_BEGIN);
            }
            else
            {
                setslot(sc_code, cdr(slot(sc_code)));
                if (slot(sc_code) == slot(sc_nil))
                {
                    s_return(slot(sc_nil));
                }
                else
                {
                    s_save(local(scheme_object), OP_COND1, slot(sc_nil), slot(sc_code));
                    setslot(sc_code, caar(slot(sc_code)));
                    s_goto(OP_EVAL);
                }
            }

        case OP_DELAY:         /* delay */
        {
            local(x) = cons(local(scheme_object), slot(sc_nil), slot(sc_code));
            local(x) = mk_closure(local(scheme_object), local(x), slot(sc_envir));
            settypeflag(local(x), T_PROMISE);
            s_return(local(x));
        }

        case OP_AND0:          /* and */
            if (slot(sc_code) == slot(sc_nil))
            {
                s_return(slot(sc_t));
            }
            s_save(local(scheme_object), OP_AND1, slot(sc_nil), cdr(slot(sc_code)));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_AND1:          /* and */
            if (is_false(slot(sc_value)))
            {
                s_return(slot(sc_value));
            }
            else if (slot(sc_code) == slot(sc_nil))
            {
                s_return(slot(sc_value));
            }
            else
            {
                s_save(local(scheme_object), OP_AND1, slot(sc_nil), cdr(slot(sc_code)));
                setslot(sc_code, car(slot(sc_code)));
                s_goto(OP_EVAL);
            }

        case OP_OR0:           /* or */
            if (slot(sc_code) == slot(sc_nil))
            {
                s_return(slot(sc_f));
            }
            s_save(local(scheme_object), OP_OR1, slot(sc_nil), cdr(slot(sc_code)));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_OR1:           /* or */
            if (is_true(slot(sc_value)))
            {
                s_return(slot(sc_value));
            }
            else if (slot(sc_code) == slot(sc_nil))
            {
                s_return(slot(sc_value));
            }
            else
            {
                s_save(local(scheme_object), OP_OR1, slot(sc_nil), cdr(slot(sc_code)));
                setslot(sc_code, car(slot(sc_code)));
                s_goto(OP_EVAL);
            }

        case OP_C0STREAM:      /* cons-stream */
            s_save(local(scheme_object), OP_C1STREAM, slot(sc_nil), cdr(slot(sc_code)));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_C1STREAM:      /* cons-stream */
        {
            setslot(sc_args, slot(sc_value));   /* save sc_value to register sc_args for gc */
            local(x) = cons(local(scheme_object), slot(sc_nil), slot(sc_code));
            local(x) = mk_closure(local(scheme_object), local(x), slot(sc_envir));
            settypeflag(local(x), T_PROMISE);
            s_return(cons(local(scheme_object), slot(sc_args), local(x)));
        }

        case OP_MACRO0:        /* macro */
            if (is_pair(gc, car(slot(sc_code))))
            {
                local(x) = caar(slot(sc_code));
                local(y) = cons(local(scheme_object), cdar(slot(sc_code)), cdr(slot(sc_code)));
                local(y) = cons(local(scheme_object), slot(sc_lambda), local(y));
                setslot(sc_code, local(y));
            }
            else
            {
                local(x) = car(slot(sc_code));
                setslot(sc_code, cadr(slot(sc_code)));
            }
            if (!is_symbol(gc, local(x)))
            {
                Error_0("variable is not a symbol");
            }
            s_save(local(scheme_object), OP_MACRO1, slot(sc_nil), local(x));
            s_goto(OP_EVAL);

        case OP_MACRO1:        /* macro */
            settypeflag(slot(sc_value), T_MACRO);
            local(x) = find_slot_in_env(local(scheme_object), slot(sc_envir), slot(sc_code), 0);
            if (local(x) != slot(sc_nil))
            {
                set_slot_in_env(gc, local(x), slot(sc_value));
            }
            else
            {
                new_slot_in_env(local(scheme_object), slot(sc_code), slot(sc_value));
            }
            s_return(slot(sc_code));

        case OP_CASE0:         /* case */
            s_save(local(scheme_object), OP_CASE1, slot(sc_nil), cdr(slot(sc_code)));
            setslot(sc_code, car(slot(sc_code)));
            s_goto(OP_EVAL);

        case OP_CASE1:         /* case */
            for (local(x) = slot(sc_code);
                 local(x) != slot(sc_nil);
                 local(x) = cdr(local(x)))
            {
                local(y) = caar(local(x));
                if (!is_pair(gc, local(y)))
                {
                    break;
                }
                for (;
                     local(y) != slot(sc_nil);
                     local(y) = cdr(local(y)))
                {
                    if (eqv(gc, car(local(y)), slot(sc_value)))
                    {
                        break;
                    }
                }
                if (local(y) != slot(sc_nil))
                {
                    break;
                }
            }
            if (local(x) != slot(sc_nil))
            {
                if (is_pair(gc, caar(local(x))))
                {
                    setslot(sc_code, cdar(local(x)));
                    s_goto(OP_BEGIN);
                }
                else
                {               /* else */
                    s_save(local(scheme_object), OP_CASE2, slot(sc_nil), cdar(local(x)));
                    setslot(sc_code, caar(local(x)));
                    s_goto(OP_EVAL);
                }
            }
            else
            {
                s_return(slot(sc_nil));
            }

        case OP_CASE2:         /* case */
            if (is_true(slot(sc_value)))
            {
                s_goto(OP_BEGIN);
            }
            else
            {
                s_return(slot(sc_nil));
            }

        case OP_PAPPLY:        /* apply */
        {
            setslot(sc_code, car(slot(sc_args)));
            local(x) = list_star(local(scheme_object), cdr(slot(sc_args)));
            setslot(sc_args, local(x));
            /*sc_args = cadr(sc_args); */
            s_goto(OP_APPLY);
        }

        case OP_PEVAL:         /* eval */
            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                setslot(sc_envir, cadr(slot(sc_args)));
            }
            setslot(sc_code, car(slot(sc_args)));
            s_goto(OP_EVAL);

        case OP_CONTINUATION:  /* call-with-current-continuation */
        {
            setslot(sc_code, car(slot(sc_args)));
            local(x) = mk_continuation(local(scheme_object), slot(sc_dump));
            local(x) = cons(local(scheme_object), local(x), slot(sc_nil));
            setslot(sc_args, local(x));
            s_goto(OP_APPLY);
        }

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }
    return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_2(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y, retval)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);

    num v;

#if USE_MATH
    double dd;
#endif

    switch (op)
    {
#if USE_MATH
        case OP_INEX2EX:       /* inexact->exact */
            local(x) = car(slot(sc_args));
            if (num_is_integer(gc, local(x)))
            {
                s_return(local(x));
            }
            else if (modf(rvalue_unchecked(local(x)), &dd) == 0.0)
            {
                local(retval) = mk_integer(gc, ivalue(gc, local(x)));
                s_return(local(retval));
            }
            else
            {
                Error_1("inexact->exact: not integral:", local(x));
            }

        case OP_EXP:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, exp(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_LOG:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, log(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_SIN:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, sin(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_COS:
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, cos(rvalue(gc, local(x))));
            s_return(local(retval));

        case OP_TAN:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, tan(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_ASIN:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, asin(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_ACOS:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, acos(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_ATAN:
        {
            local(x) = car(slot(sc_args));
            if (cdr(slot(sc_args)) == slot(sc_nil))
            {
                local(retval) = mk_real(gc, atan(rvalue(gc, local(x))));
                s_return(local(retval));
            }
            else
            {
                local(y) = cadr(slot(sc_args));
                local(retval) = mk_real(gc, atan2(rvalue(gc, local(x)), rvalue(gc, local(y))));
                s_return(local(retval));
            }
        }

        case OP_SQRT:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, sqrt(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_EXPT:
        {
            double result;
            int real_result = 1;
            local(y) = cadr(slot(sc_args));

            local(x) = car(slot(sc_args));
            if (num_is_integer(gc, local(x)) && num_is_integer(gc, local(y)))
                real_result = 0;

            /* This 'if' is an R5RS compatibility fix. */
            /* NOTE: Remove this 'if' fix for R6RS.    */
            if (rvalue(gc, local(x)) == 0 && rvalue(gc, local(y)) < 0)
            {
                result = 0.0;
            }
            else
            {
                result = pow(rvalue(gc, local(x)), rvalue(gc, local(y)));
            }
            /* Before returning integer result make sure we can. */
            /* If the test fails, result is too big for integer. */
            if (!real_result)
            {
                long result_as_long = (long) result;

                if (result != (double) result_as_long)
                    real_result = 1;
            }
            if (real_result)
            {
                local(retval) = mk_real(gc, result);
                s_return(local(retval));
            }
            else
            {
                local(retval) = mk_integer(gc, result);
                s_return(local(retval));
            }
        }

        case OP_FLOOR:
        {
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, floor(rvalue(gc, local(x))));
            s_return(local(retval));
        }

        case OP_CEILING:
            local(x) = car(slot(sc_args));
            local(retval) = mk_real(gc, ceil(rvalue(gc, local(x))));
            s_return(local(retval));

        case OP_TRUNCATE:
        {
            double rvalue_of_x;

            local(x) = car(slot(sc_args));
            rvalue_of_x = rvalue(gc, local(x));
            if (rvalue_of_x > 0)
            {
                local(retval) = mk_real(gc, floor(rvalue_of_x));
                s_return(local(retval));
            }
            else
            {
                local(retval) = mk_real(gc, ceil(rvalue_of_x));
                s_return(local(retval));
            }
        }

        case OP_ROUND:
            local(x) = car(slot(sc_args));
            if (num_is_integer(gc, local(x)))
                s_return(local(x));
            local(retval) = mk_real(gc, round_per_R5RS(rvalue(gc, local(x))));
            s_return(local(retval));
#endif

        case OP_ADD:           /* + */
            v = num_zero;
            for (local(x) = slot(sc_args); local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                v = num_add(v, nvalue(car(local(x))));
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_MUL:           /* * */
            v = num_one;
            for (local(x) = slot(sc_args); local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                v = num_mul(v, nvalue(car(local(x))));
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_SUB:           /* - */
            if (cdr(slot(sc_args)) == slot(sc_nil))
            {
                local(x) = slot(sc_args);
                v = num_zero;
            }
            else
            {
                local(x) = cdr(slot(sc_args));
                v = nvalue(car(slot(sc_args)));
            }
            for (; local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                v = num_sub(v, nvalue(car(local(x))));
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_DIV:           /* / */
            if (cdr(slot(sc_args)) == slot(sc_nil))
            {
                local(x) = slot(sc_args);
                v = num_one;
            }
            else
            {
                local(x) = cdr(slot(sc_args));
                v = nvalue(car(slot(sc_args)));
            }
            for (; local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                if (!is_zero_double(rvalue(gc, car(local(x)))))
                {
                    v = num_div(v, nvalue(car(local(x))));
                }
                else
                {
                    Error_0("/: division by zero");
                }
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_INTDIV:        /* quotient */
            if (cdr(slot(sc_args)) == slot(sc_nil))
            {
                local(x) = slot(sc_args);
                v = num_one;
            }
            else
            {
                local(x) = cdr(slot(sc_args));
                v = nvalue(car(slot(sc_args)));
            }
            for (; local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                if (ivalue(gc, car(local(x))) != 0)
                {
                    v = num_intdiv(v, nvalue(car(local(x))));
                }
                else
                {
                    Error_0("quotient: division by zero");
                }
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_REM:           /* remainder */
            v = nvalue(car(slot(sc_args)));
            if (ivalue(gc, cadr(slot(sc_args))) != 0)
            {
                v = num_rem(v, nvalue(cadr(slot(sc_args))));
            }
            else
            {
                Error_0("remainder: division by zero");
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_MOD:           /* modulo */
            v = nvalue(car(slot(sc_args)));
            if (ivalue(gc, cadr(slot(sc_args))) != 0)
            {
                v = num_mod(v, nvalue(cadr(slot(sc_args))));
            }
            else
            {
                Error_0("modulo: division by zero");
            }
            local(retval) = mk_number(gc, v);
            s_return(local(retval));

        case OP_CAR:           /* car */
            s_return(caar(slot(sc_args)));

        case OP_CDR:           /* cdr */
            s_return(cdar(slot(sc_args)));

        case OP_CONS:          /* cons */
            set_cdr(gc, slot(sc_args), cadr(slot(sc_args)));
            s_return(slot(sc_args));

        case OP_SETCAR:        /* set-car! */
            if (!is_immutable(car(slot(sc_args))))
            {
                local(x) = car(slot(sc_args));
                set_car(gc, local(x), cadr(slot(sc_args)));
                s_return(car(slot(sc_args)));
            }
            else
            {
                Error_0("set-car!: unable to alter immutable pair");
            }

        case OP_SETCDR:        /* set-cdr! */
            if (!is_immutable(car(slot(sc_args))))
            {
                local(x) = car(slot(sc_args));
                set_cdr(gc, local(x), cadr(slot(sc_args)));
                s_return(car(slot(sc_args)));
            }
            else
            {
                Error_0("set-cdr!: unable to alter immutable pair");
            }

        case OP_CHAR2INT:
        {                       /* char->integer */
            char c = (char) ivalue(gc, car(slot(sc_args)));
            local(retval) = mk_integer(gc, (unsigned char) c);
            s_return(local(retval));
        }

        case OP_INT2CHAR:
        {                       /* integer->char */
            unsigned char c = (unsigned char) ivalue(gc, car(slot(sc_args)));
            local(retval) = mk_character(gc, (char) c);
            s_return(local(retval));
        }

        case OP_CHARUPCASE:
        {
            unsigned char c = (unsigned char) ivalue(gc, car(slot(sc_args)));
            c = toupper(c);
            local(retval) = mk_character(gc, (char) c);
            s_return(local(retval));
        }

        case OP_CHARDNCASE:
        {
            unsigned char c = (unsigned char) ivalue(gc, car(slot(sc_args)));
            c = tolower(c);
            local(retval) = mk_character(gc, (char) c);
            s_return(local(retval));
        }

        case OP_STR2SYM:       /* string->symbol */
            local(retval) = mk_symbol(local(scheme_object), strvalue(car(slot(sc_args))));
            s_return(local(retval));

        case OP_STR2ATOM:      /* string->atom */
        {
            char *s = strvalue(car(slot(sc_args)));
            long pf = 0;

            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                /* we know cadr(slot(sc_args)) is a natural number */
                /* see if it is 2, 8, 10, or 16, or error */
                pf = ivalue_unchecked(cadr(slot(sc_args)));
                if (pf == 16 || pf == 10 || pf == 8 || pf == 2)
                {
                    /* base is OK */
                }
                else
                {
                    pf = -1;
                }
            }
            if (pf < 0)
            {
                Error_1("string->atom: bad base:", cadr(slot(sc_args)));
            }
            else if (*s == '#') /* no use of base! */
            {
                local(retval) = mk_sharp_const(local(scheme_object), s + 1);
                s_return(local(retval));
            }
            else
            {
                if (pf == 0 || pf == 10)
                {
                    local(retval) = mk_atom(local(scheme_object), s);
                    s_return(local(retval));
                }
                else
                {
                    char *ep;
                    long iv = strtol(s, &ep, (int) pf);

                    if (*ep == 0)
                    {
                        local(retval) = mk_integer(gc, iv);
                        s_return(local(retval));
                    }
                    else
                    {
                        s_return(slot(sc_f));
                    }
                }
            }
        }

        case OP_SYM2STR:       /* symbol->string */
            local(x) = mk_string(local(scheme_object), symname(gc, car(slot(sc_args))));
            setimmutable(gc, local(x));
            s_return(local(x));

        case OP_ATOM2STR:      /* atom->string */
        {
            long pf = 0;

            local(x) = car(slot(sc_args));
            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                /* we know cadr(slot(sc_args)) is a natural number */
                /* see if it is 2, 8, 10, or 16, or error */
                pf = ivalue_unchecked(cadr(slot(sc_args)));
                if (is_number(gc, local(x)) && (pf == 16 || pf == 10 || pf == 8 || pf == 2))
                {
                    /* base is OK */
                }
                else
                {
                    pf = -1;
                }
            }
            if (pf < 0)
            {
                Error_1("atom->string: bad base:", cadr(slot(sc_args)));
            }
            else if (is_number(gc, local(x)) || is_character(gc, local(x)) || is_string(gc, local(x)) || is_symbol(gc, local(x)))
            {
                char *p;
                int len;

                atom2str(local(scheme_object), local(x), (int) pf, &p, &len);
                local(retval) = mk_counted_string(local(scheme_object), p, len);
                s_return(local(retval));
            }
            else
            {
                Error_1("atom->string: not an atom:", local(x));
            }
        }

        case OP_MKSTRING:
        {                       /* make-string */
            int fill = ' ';
            int len;

            len = ivalue(gc, car(slot(sc_args)));

            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                fill = charvalue(cadr(slot(sc_args)));
            }
            local(retval) = mk_empty_string(local(scheme_object), len, (char) fill);
            s_return(local(retval));
        }

        case OP_STRLEN:        /* string-length */
            local(retval) = mk_integer(gc, strlength(car(slot(sc_args))));
            s_return(local(retval));

        case OP_STRREF:
        {                       /* string-ref */
            char *str;
            size_t index;

            str = strvalue(car(slot(sc_args)));

            index = ivalue(gc, cadr(slot(sc_args)));

            if (index >= strlength(car(slot(sc_args))))
            {
                Error_1("string-ref: out of bounds:", cadr(slot(sc_args)));
            }

            local(retval) = mk_character(gc, ((unsigned char *) str)[index]);
            s_return(local(retval));
        }

        case OP_STRSET:
        {                       /* string-set! */
            char *str;
            size_t index;
            int c;

            if (is_immutable(car(slot(sc_args))))
            {
                Error_1("string-set!: unable to alter immutable string:", car(slot(sc_args)));
            }
            str = strvalue(car(slot(sc_args)));

            index = ivalue(gc, cadr(slot(sc_args)));
            if (index >= strlength(car(slot(sc_args))))
            {
                Error_1("string-set!: out of bounds:", cadr(slot(sc_args)));
            }

            c = charvalue(caddr(slot(sc_args)));
            str[index] = (char) c;
            s_return(car(slot(sc_args)));
        }

        case OP_STRAPPEND:
        {                       /* string-append */
            /* in 1.29 string-append was in Scheme in init.scm but was too slow */
            int len = 0;
            char *pos;

            /* compute needed length for new string */
            for (local(x) = slot(sc_args); local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                len += strlength(car(local(x)));
            }
            local(y) = mk_empty_string(local(scheme_object), len, ' ');
            /* store the contents of the argument strings into the new string */
            for (pos = strvalue(local(y)), local(x) = slot(sc_args);
                 local(x) != slot(sc_nil);
                 pos += strlength(car(local(x))), local(x) = cdr(local(x)))
            {
                memcpy(pos, strvalue(car(local(x))), strlength(car(local(x))));
            }
            s_return(local(y));
        }

        case OP_SUBSTR:
        {                       /* substring */
            char *str;
            size_t index0;
            size_t index1;
            int len;

            str = strvalue(car(slot(sc_args)));

            index0 = ivalue(gc, cadr(slot(sc_args)));

            if (index0 > strlength(car(slot(sc_args))))
            {
                Error_1("substring: start out of bounds:", cadr(slot(sc_args)));
            }

            if (cddr(slot(sc_args)) != slot(sc_nil))
            {
                index1 = ivalue(gc, caddr(slot(sc_args)));
                if (index1 > strlength(car(slot(sc_args))) || index1 < index0)
                {
                    Error_1("substring: end out of bounds:", caddr(slot(sc_args)));
                }
            }
            else
            {
                index1 = strlength(car(slot(sc_args)));
            }

            len = index1 - index0;
            local(x) = mk_empty_string(local(scheme_object), len, ' ');
            memcpy(strvalue(local(x)), str + index0, len);
            strvalue(local(x))[len] = 0;

            s_return(local(x));
        }

        case OP_VECTOR:
        {                       /* vector */
            int i;
            int len = list_length(local(scheme_object), slot(sc_args));

            if (len < 0)
            {
                Error_1("vector: not a proper list:", slot(sc_args));
            }
            local(y) = mk_vector(local(scheme_object), len);
            if (sc->global_scheme->no_memory)
            {
                Error_0("vector: no memory.");
            }
            for (local(x) = slot(sc_args), i = 0; is_pair(gc, local(x)); local(x) = cdr(local(x)), i++)
            {
                set_vector_elem(gc, local(y), i, car(local(x)));
            }
            s_return(local(y));
        }

        case OP_MKVECTOR:
        {                       /* make-vector */
            int len;

            len = ivalue(gc, car(slot(sc_args)));

            local(y) = mk_vector(local(scheme_object), len);
            if (sc->global_scheme->no_memory)
            {
                Error_0("vector: no memory.");
            }

            pointer fill = slot(sc_nil);
            if (cdr(slot(sc_args)) != slot(sc_nil))
            {
                fill = cadr(slot(sc_args));
            }

            if (fill != slot(sc_nil))
            {
                fill_vector(gc, local(y), fill);
            }
            s_return(local(y));
        }

        case OP_VECLEN:        /* vector-length */
            local(retval) = mk_integer(gc, car(slot(sc_args))->fields.slot_count);
            s_return(local(retval));

        case OP_VECREF:
        {                       /* vector-ref */
            int index = ivalue(gc, cadr(slot(sc_args)));

            if (index >= car(slot(sc_args))->fields.slot_count)
            {
                Error_1("vector-ref: out of bounds:", cadr(slot(sc_args)));
            }

            s_return(vector_elem(gc, car(slot(sc_args)), index));
        }

        case OP_VECSET:
        {                       /* vector-set! */
            int index;

            if (is_immutable(car(slot(sc_args))))
            {
                Error_1("vector-set!: unable to alter immutable vector:", car(slot(sc_args)));
            }

            index = ivalue(gc, cadr(slot(sc_args)));
            if (index >= car(slot(sc_args))->fields.slot_count)
            {
                Error_1("vector-set!: out of bounds:", cadr(slot(sc_args)));
            }

            set_vector_elem(gc, car(slot(sc_args)), index, caddr(slot(sc_args)));
            s_return(car(slot(sc_args)));
        }

        case OP_RAND:
        {
            local(x) = mk_integer(gc, random());
            s_return(local(x));
        }

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }

    scope_return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static int is_list(pointer scheme_object, pointer a) // Safe.
{
    return list_length(scheme_object, a) >= 0;
}

/* Result is:
   proper list: length
   circular list: -1
   not even a pair: -2
   dotted list: -2 minus length before dot
*/
int list_length(pointer scheme_object, pointer a) // Safe.
{
    #if !defined(NO_DEBUG)
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    #endif // !defined(NO_DEBUG).

    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    int i = 0;
    pointer slow, fast;

    slow = fast = a;
    while (1)
    {
        if (fast == sc_nil)
            return i;
        if (!is_pair(gc, fast))
            return -2 - i;
        fast = pair_cdr(gc, fast);
        ++i;
        if (fast == sc_nil)
            return i;
        if (!is_pair(gc, fast))
            return -2 - i;
        ++i;
        fast = pair_cdr(gc, fast);

        /* Safe because we would have already returned if `fast'
         * encountered a non-pair. */
        slow = pair_cdr(gc, slow);
        if (fast == slow)
        {
            /* the fast pointer has looped back around and caught up
             * with the slow pointer, hence the structure is circular,
             * not of finite length, and therefore not a list */
            return -1;
        }
    }
}

static pointer opexe_3(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_EOF_OBJ, sc_eof_obj);

    num v;
    int (*comp_func) (num, num) = 0;

    switch (op)
    {
        case OP_NOT:           /* not */
            s_retbool(is_false(car(slot(sc_args))));
        case OP_BOOLP:         /* boolean? */
            s_retbool(car(slot(sc_args)) == slot(sc_f) || car(slot(sc_args)) == slot(sc_t));
        case OP_EOFOBJP:       /* boolean? */
            s_retbool(car(slot(sc_args)) == slot(sc_eof_obj));
        case OP_NULLP:         /* null? */
            s_retbool(car(slot(sc_args)) == slot(sc_nil));
        case OP_NUMEQ:         /* = */
        case OP_LESS:          /* < */
        case OP_GRE:           /* > */
        case OP_LEQ:           /* <= */
        case OP_GEQ:           /* >= */
            switch (op)
            {
                case OP_NUMEQ:
                    comp_func = num_eq;
                    break;
                case OP_LESS:
                    comp_func = num_lt;
                    break;
                case OP_GRE:
                    comp_func = num_gt;
                    break;
                case OP_LEQ:
                    comp_func = num_le;
                    break;
                case OP_GEQ:
                    comp_func = num_ge;
                    break;

                default:
                    abort();
                    break;
            }
            local(x) = slot(sc_args);
            v = nvalue(car(local(x)));
            local(x) = cdr(local(x));

            for (; local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                if (!comp_func(v, nvalue(car(local(x)))))
                {
                    s_retbool(0);
                }
                v = nvalue(car(local(x)));
            }
            s_retbool(1);
        case OP_SYMBOLP:       /* symbol? */
            s_retbool(is_symbol(gc, car(slot(sc_args))));
        case OP_NUMBERP:       /* number? */
            s_retbool(is_number(gc, car(slot(sc_args))));
        case OP_STRINGP:       /* string? */
            s_retbool(is_string(gc, car(slot(sc_args))));
        case OP_INTEGERP:      /* integer? */
            s_retbool(is_integer(gc, car(slot(sc_args))));
        case OP_REALP:         /* real? */
            s_retbool(is_number(gc, car(slot(sc_args))));    /* All numbers are real */
        case OP_CHARP:         /* char? */
            s_retbool(is_character(gc, car(slot(sc_args))));
#if USE_CHAR_CLASSIFIERS
        case OP_CHARAP:        /* char-alphabetic? */
            s_retbool(Cisalpha(ivalue(gc, car(slot(sc_args)))));
        case OP_CHARNP:        /* char-numeric? */
            s_retbool(Cisdigit(ivalue(gc, car(slot(sc_args)))));
        case OP_CHARWP:        /* char-whitespace? */
            s_retbool(Cisspace(ivalue(gc, car(slot(sc_args)))));
        case OP_CHARUP:        /* char-upper-case? */
            s_retbool(Cisupper(ivalue(gc, car(slot(sc_args)))));
        case OP_CHARLP:        /* char-lower-case? */
            s_retbool(Cislower(ivalue(gc, car(slot(sc_args)))));
#endif
        case OP_PORTP:         /* port? */
            s_retbool(is_port(gc, car(slot(sc_args))));
        case OP_INPORTP:       /* input-port? */
            s_retbool(is_inport(gc, car(slot(sc_args))));
        case OP_OUTPORTP:      /* output-port? */
            s_retbool(is_outport(gc, car(slot(sc_args))));
        case OP_PROCP:         /* procedure? */
          /*--
              * continuation should be procedure by the example
              * (call-with-current-continuation procedure?) ==> #t
                 * in R^3 report sec. 6.9
              */
            s_retbool(is_proc(car(slot(sc_args))) ||
                      is_closure(car(slot(sc_args))) ||
                      is_continuation(car(slot(sc_args))) ||
                      is_foreign(car(slot(sc_args))));
        case OP_PAIRP:         /* pair? */
            s_retbool(is_pair(gc, car(slot(sc_args))));
        case OP_LISTP:         /* list? */
            s_retbool(list_length(local(scheme_object), car(slot(sc_args))) >= 0);
        case OP_ENVP:          /* environment? */
            s_retbool(is_environment(gc, car(slot(sc_args))));
        case OP_VECTORP:       /* vector? */
            s_retbool(is_vector(gc, car(slot(sc_args))));
        case OP_EQ:            /* eq? */
            s_retbool(car(slot(sc_args)) == cadr(slot(sc_args)));
        case OP_EQV:           /* eqv? */
            s_retbool(eqv(gc, car(slot(sc_args)), cadr(slot(sc_args))));
        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }
    scope_return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_4(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_CODE, sc_code);
    defslot(local(scheme_object), SCHEME_VALUE, sc_value);
    defslot(local(scheme_object), SCHEME_GLOBAL_ENV, sc_global_env);
    defslot(local(scheme_object), SCHEME_ENVIR, sc_envir);
    defslot(local(scheme_object), SCHEME_INPORT, sc_inport);
    defslot(local(scheme_object), SCHEME_OUTPORT, sc_outport);

    switch (op)
    {
        case OP_FORCE:         /* force */
            setslot(sc_code, car(slot(sc_args)));
            if (is_promise(slot(sc_code)))
            {
                /* Should change type to closure here */
                s_save(local(scheme_object), OP_SAVE_FORCED, slot(sc_nil), slot(sc_code));
                setslot(sc_args, slot(sc_nil));
                s_goto(OP_APPLY);
            }
            else
            {
                s_return(slot(sc_code));
            }

        case OP_SAVE_FORCED:   /* Save forced value replacing promise */
            setslot(sc_code, slot(sc_value));
            s_return(slot(sc_value));

        case OP_WRITE:         /* write */
        case OP_DISPLAY:       /* display */
        case OP_WRITE_CHAR:    /* write-char */
            if (is_pair(gc, cdr(slot(sc_args))))
            {
                if (cadr(slot(sc_args)) != slot(sc_outport))
                {
                    local(x) = cons(local(scheme_object), slot(sc_outport), slot(sc_nil));
                    s_save(local(scheme_object), OP_SET_OUTPORT, local(x), slot(sc_nil));
                    setslot(sc_outport, cadr(slot(sc_args)));
                }
            }
            setslot(sc_args, car(slot(sc_args)));
            if (op == OP_WRITE)
            {
                sc->print_flag = 1;
            }
            else
            {
                sc->print_flag = 0;
            }
            s_goto(OP_P0LIST);

        case OP_NEWLINE:       /* newline */
            if (is_pair(gc, slot(sc_args)))
            {
                if (car(slot(sc_args)) != slot(sc_outport))
                {
                    local(x) = cons(local(scheme_object), slot(sc_outport), slot(sc_nil));
                    s_save(local(scheme_object), OP_SET_OUTPORT, local(x), slot(sc_nil));
                    setslot(sc_outport, car(slot(sc_args)));
                }
            }
            putstr(local(scheme_object), "\n");
            s_return(slot(sc_t));

        case OP_ERR0:          /* error */
            sc->retcode = -1;
            if (!is_string(gc, car(slot(sc_args))))
            {
                local(x) = mk_string(local(scheme_object), " -- ");
                local(x) = cons(local(scheme_object), local(x), slot(sc_args));
                setslot(sc_args, local(x));
                setimmutable(gc, car(slot(sc_args)));
            }
            putstr(local(scheme_object), "Error: ");
            putstr(local(scheme_object), strvalue(car(slot(sc_args))));
            setslot(sc_args, cdr(slot(sc_args)));
            s_goto(OP_ERR1);

        case OP_ERR1:          /* error */
            putstr(local(scheme_object), " ");
            if (slot(sc_args) != slot(sc_nil))
            {
                s_save(local(scheme_object), OP_ERR1, cdr(slot(sc_args)), slot(sc_nil));
                setslot(sc_args, car(slot(sc_args)));
                sc->print_flag = 1;
                s_goto(OP_P0LIST);
            }
            else
            {
                putstr(local(scheme_object), "\n");
                if (sc->interactive_repl)
                {
                    s_goto(OP_T0LVL);
                }
                else
                {
                    scope_return slot(sc_nil);
                }
            }

        case OP_REVERSE:       /* reverse */
            local(x) = reverse(local(scheme_object), car(slot(sc_args)));
            s_return(local(x));

        case OP_LIST_STAR:     /* list* */
            local(x) = list_star(local(scheme_object), slot(sc_args));
            s_return(local(x));

        case OP_APPEND:        /* append */
            local(x) = slot(sc_nil);
            local(y) = slot(sc_args);
            if (local(y) == local(x))
            {
                s_return(local(x));
            }

            /* cdr() in the while condition is not a typo. If car() */
            /* is used (append '() 'a) will return the wrong result. */
            while (cdr(local(y)) != slot(sc_nil))
            {
                local(x) = revappend(local(scheme_object), local(x), car(local(y)));
                local(y) = cdr(local(y));
                if (local(x) == slot(sc_f))
                {
                    Error_0("non-list argument to append");
                }
            }

            s_return(reverse_in_place(local(scheme_object), car(local(y)), local(x)));

#if USE_PLIST
        case OP_PUT:           /* put */
            if (!hasprop(car(slot(sc_args))) || !hasprop(cadr(slot(sc_args))))
            {
                Error_0("illegal use of put");
            }
            for (local(x) = symprop(car(slot(sc_args))), local(y) = cadr(slot(sc_args));
                 local(x) != slot(sc_nil); local(x) = cdr(local(x)))
            {
                if (caar(local(x)) == local(y))
                {
                    break;
                }
            }
            if (local(x) != slot(sc_nil))
            {
                local(x) = car(local(x));
                set_cdr(gc, local(x), caddr(slot(sc_args)));
            }
            else
            {
                local(x) = car(slot(sc_args));
                local(y) = cons(local(scheme_object), local(y), caddr(slot(sc_args)));
                local(y) = cons(local(scheme_object), local(y), symprop(car(slot(sc_args))));
                set_cdr(gc, local(x),l ocal(y));
            }

            s_return(slot(sc_t));

        case OP_GET:           /* get */
            if (!hasprop(car(slot(sc_args))) || !hasprop(cadr(slot(sc_args))))
            {
                Error_0("illegal use of get");
            }
            for (local(x) = symprop(car(slot(sc_args))), local(y) = cadr(slot(sc_args));
                 local(x) != slot(sc_nil);
                 local(x) = cdr(local(x)))
            {
                if (caar(local(x)) == local(y))
                {
                    break;
                }
            }
            if (local(x) != slot(sc_nil))
            {
                s_return(cdar(local(x)));
            }
            else
            {
                s_return(slot(sc_nil));
            }
#endif /* USE_PLIST */
        case OP_QUIT:          /* quit */
            if (is_pair(gc, slot(sc_args)))
            {
                sc->retcode = ivalue(gc, car(slot(sc_args)));
            }
            scope_return (slot(sc_nil));

        case OP_GC:            /* gc */
        {
            ok_to_freely_gc(local(scheme_object), true);
            s_return(slot(sc_t));
        }

        case OP_GCVERB:        /* gc-verbose */
        {
            int was = sc->global_scheme->gc_verbose;
            sc->global_scheme->gc_verbose = (car(slot(sc_args)) != slot(sc_f));
            s_retbool(was);
        }

        case OP_GCSTAT:
        {
            print_gc_statistics(local(scheme_object));
            s_retbool(slot(sc_t));
        }

        case OP_NEWSEGMENT:    /* new-segment */
            if (!is_pair(gc, slot(sc_args)) || !is_number(gc, car(slot(sc_args))))
            {
                Error_0("new-segment: argument must be a number");
            }
            ok_to_freely_gc(local(scheme_object), true);
            s_return(slot(sc_t));

        case OP_OBLIST:        /* oblist */
            local(x) = oblist_all_symbols(local(scheme_object));
            s_return(local(x));

        case OP_CURR_INPORT:   /* current-input-port */
            s_return(slot(sc_inport));

        case OP_CURR_OUTPORT:  /* current-output-port */
            s_return(slot(sc_outport));

        case OP_OPEN_INFILE:   /* open-input-file */
        case OP_OPEN_OUTFILE:  /* open-output-file */
        case OP_OPEN_INOUTFILE:    /* open-input-output-file */
        {
            int prop = 0;

            switch (op)
            {
                case OP_OPEN_INFILE:
                    prop = port_input;
                    break;
                case OP_OPEN_OUTFILE:
                    prop = port_output;
                    break;
                case OP_OPEN_INOUTFILE:
                    prop = port_input | port_output;
                    break;

                default:
                    abort();
                    break;
            }
            local(x) = port_from_filename(local(scheme_object), strvalue(car(slot(sc_args))), prop);
            if (local(x) == slot(sc_nil))
            {
                s_return(slot(sc_f));
            }
            s_return(local(x));
        }

#if USE_STRING_PORTS
        case OP_OPEN_INSTRING: /* open-input-string */
        case OP_OPEN_INOUTSTRING:  /* open-input-output-string */
        {
            int prop = 0;

            switch (op)
            {
                case OP_OPEN_INSTRING:
                    prop = port_input;
                    break;
                case OP_OPEN_INOUTSTRING:
                    prop = port_input | port_output;
                    break;

                default:
                    abort();
                    break;
            }

            local(x) = port_from_string(local(scheme_object),
                                        strvalue(car(slot(sc_args))),
                                        strvalue(car(slot(sc_args))) + strlength(car(slot(sc_args))),
                                        prop);
            if (local(x) == slot(sc_nil))
            {
                s_return(slot(sc_f));
            }
            s_return(local(x));
        }
        case OP_OPEN_OUTSTRING:    /* open-output-string */
        {
            if (car(slot(sc_args)) == slot(sc_nil))
            {
                local(x) = port_from_scratch(local(scheme_object));
                if (local(x) == slot(sc_nil))
                {
                    s_return(slot(sc_f));
                }
            }
            else
            {
                local(x) = port_from_string(local(scheme_object),
                                                  strvalue(car(slot(sc_args))),
                                                  strvalue(car(slot(sc_args))) + strlength(car(slot(sc_args))),
                                                  port_output);
                if (local(x) == slot(sc_nil))
                {
                    s_return(slot(sc_f));
                }
            }
            s_return(local(x));
        }
        case OP_GET_OUTSTRING: /* get-output-string */
        {
            port *p = *((port **) get_byte(car(slot(sc_args)), 0));

            if (p->kind & port_string)
            {
                off_t size;
                char *str;

                size = p->rep.string.curr - p->rep.string.start + 1;
                str = sc->malloc(size);
                if (str != NULL)
                {
                    memcpy(str, p->rep.string.start, size - 1);
                    str[size - 1] = '\0';
                    local(x) = mk_string(local(scheme_object), str);
                    sc->free(str);
                    s_return(local(x));
                }
            }
            s_return(slot(sc_f));
        }
#endif

        case OP_CLOSE_INPORT:  /* close-input-port */
            port_close(sc, car(slot(sc_args)), port_input);
            s_return(slot(sc_t));

        case OP_CLOSE_OUTPORT: /* close-output-port */
            port_close(sc, car(slot(sc_args)), port_output);
            s_return(slot(sc_t));

        case OP_INT_ENV:       /* interaction-environment */
            s_return(slot(sc_global_env));

        case OP_CURR_ENV:      /* current-environment */
            s_return(slot(sc_envir));

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }

    scope_return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_5(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_CODE, sc_code);
    defslot(local(scheme_object), SCHEME_VALUE, sc_value);
    defslot(local(scheme_object), SCHEME_ENVIR, sc_envir);
    defslot(local(scheme_object), SCHEME_EOF_OBJ, sc_eof_obj);
    defslot(local(scheme_object), SCHEME_INPORT, sc_inport);
    defslot(local(scheme_object), SCHEME_OUTPORT, sc_outport);
    defslot(local(scheme_object), SCHEME_SHARP_HOOK, sc_sharp_hook);
    defslot(local(scheme_object), SCHEME_QUOTE, sc_quote);
    defslot(local(scheme_object), SCHEME_QQUOTE, sc_qquote);
    defslot(local(scheme_object), SCHEME_UNQUOTE, sc_unquote);
    defslot(local(scheme_object), SCHEME_UNQUOTESP, sc_unquotesp);

    if (sc->nesting != 0)
    {
        int n = sc->nesting;

        sc->nesting = 0;
        sc->retcode = -1;
        Error_1("unmatched parentheses:", mk_integer(gc, n));
    }

    switch (op)
    {
            /* ========== reading part ========== */
        case OP_READ:
            if (!is_pair(gc, slot(sc_args)))
            {
                s_goto(OP_READ_INTERNAL);
            }
            if (!is_inport(gc, car(slot(sc_args))))
            {
                Error_1("read: not an input port:", car(slot(sc_args)));
            }
            if (car(slot(sc_args)) == slot(sc_inport))
            {
                s_goto(OP_READ_INTERNAL);
            }
            local(x) = slot(sc_inport);
            setslot(sc_inport, car(slot(sc_args)));
            local(x) = cons(local(scheme_object), local(x), slot(sc_nil));
            s_save(local(scheme_object), OP_SET_INPORT, local(x), slot(sc_nil));
            s_goto(OP_READ_INTERNAL);

        case OP_READ_CHAR:     /* read-char */
        case OP_PEEK_CHAR:     /* peek-char */
        {
            int c;

            if (is_pair(gc, slot(sc_args)))
            {
                if (car(slot(sc_args)) != slot(sc_inport))
                {
                    local(x) = slot(sc_inport);
                    local(x) = cons(local(scheme_object), local(x), slot(sc_nil));
                    s_save(local(scheme_object), OP_SET_INPORT, local(x), slot(sc_nil));
                    setslot(sc_inport, car(slot(sc_args)));
                }
            }
            c = inchar(local(scheme_object));
            if (c == EOF)
            {
                s_return(slot(sc_eof_obj));
            }
            if (sc->op == OP_PEEK_CHAR)
            {
                backchar(local(scheme_object), c);
            }
            local(x) = mk_character(gc, c);
            s_return(local(x));
        }

        case OP_CHAR_READY:    /* char-ready? */
        {
            local(x) = slot(sc_inport);
            int res;

            if (is_pair(gc, slot(sc_args)))
            {
                local(x) = car(slot(sc_args));
            }

            port *p = *((port **) get_byte(local(x), 0));
            res = p->kind & port_string;
            s_retbool(res);
        }

        case OP_SET_INPORT:    /* set-input-port */
            setslot(sc_inport, car(slot(sc_args)));
            s_return(slot(sc_value));

        case OP_SET_OUTPORT:   /* set-output-port */
            setslot(sc_outport, car(slot(sc_args)));
            s_return(slot(sc_value));

        case OP_RDSEXPR:
            switch (sc->tok)
            {
                case TOK_EOF:
                    s_return(slot(sc_eof_obj));
                    /* NOTREACHED */
/*
 * Commented out because we now skip comments in the scanner
 *
          case TOK_COMMENT: {
               int c;
               while ((c=inchar(sc)) != '\n' && c!=EOF)
                    ;
               sc->tok = token(sc);
               s_goto(sc,OP_RDSEXPR);
          }
*/
                case TOK_VEC:
                    s_save(local(scheme_object), OP_RDVEC, slot(sc_nil), slot(sc_nil));
                    /* fall through */
                case TOK_LPAREN:
                    sc->tok = token(local(scheme_object));
                    if (sc->tok == TOK_RPAREN)
                    {
                        s_return(slot(sc_nil));
                    }
                    else if (sc->tok == TOK_DOT)
                    {
                        Error_0("syntax error: illegal dot expression");
                    }
                    else
                    {
                        sc->nesting_stack[sc->file_i]++;
                        s_save(local(scheme_object), OP_RDLIST, slot(sc_nil), slot(sc_nil));
                        s_goto(OP_RDSEXPR);
                    }
                case TOK_QUOTE:
                    s_save(local(scheme_object), OP_RDQUOTE, slot(sc_nil), slot(sc_nil));
                    sc->tok = token(local(scheme_object));
                    s_goto(OP_RDSEXPR);
                case TOK_BQUOTE:
                    sc->tok = token(local(scheme_object));
                    if (sc->tok == TOK_VEC)
                    {
                        s_save(local(scheme_object), OP_RDQQUOTEVEC, slot(sc_nil), slot(sc_nil));
                        sc->tok = TOK_LPAREN;
                        s_goto(OP_RDSEXPR);
                    }
                    else
                    {
                        s_save(local(scheme_object), OP_RDQQUOTE, slot(sc_nil), slot(sc_nil));
                    }
                    s_goto(OP_RDSEXPR);
                case TOK_COMMA:
                    s_save(local(scheme_object), OP_RDUNQUOTE, slot(sc_nil), slot(sc_nil));
                    sc->tok = token(local(scheme_object));
                    s_goto(OP_RDSEXPR);
                case TOK_ATMARK:
                    s_save(local(scheme_object), OP_RDUQTSP, slot(sc_nil), slot(sc_nil));
                    sc->tok = token(local(scheme_object));
                    s_goto(OP_RDSEXPR);
                case TOK_ATOM:
                    local(x) = mk_atom(local(scheme_object), readstr_upto(local(scheme_object), DELIMITERS));
                    s_return(local(x));
                case TOK_DQUOTE:
                    local(x) = readstrexp(local(scheme_object));
                    if (local(x) == slot(sc_f))
                    {
                        Error_0("Error reading string");
                    }
                    setimmutable(gc, local(x));
                    s_return(local(x));
                case TOK_SHARP:
                {
                    local(x) = find_slot_in_env(local(scheme_object), slot(sc_envir), slot(sc_sharp_hook), 1);

                    if (local(x) == slot(sc_nil))
                    {
                        Error_0("undefined sharp expression");
                    }
                    else
                    {
                        local(x) = cons(local(scheme_object), slot_value_in_env(gc, local(x)), slot(sc_nil));
                        setslot(sc_code, local(x));
                        s_goto(OP_EVAL);
                    }
                }
                case TOK_SHARP_CONST:
                    local(x) = mk_sharp_const(local(scheme_object), readstr_upto(local(scheme_object), DELIMITERS));
                    if (local(x) == slot(sc_nil))
                    {
                        Error_0("undefined sharp expression");
                    }
                    else
                    {
                        s_return(local(x));
                    }
                default:
                    Error_0("syntax error: illegal token");
            }
            break;

        case OP_RDLIST:
        {
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_args));
            setslot(sc_args, local(x));
            sc->tok = token(local(scheme_object));
/* We now skip comments in the scanner
          while (sc->tok == TOK_COMMENT) {
               int c;
               while ((c=inchar(sc)) != '\n' && c!=EOF)
                    ;
               sc->tok = token(sc);
          }
*/
            if (sc->tok == TOK_EOF)
            {
                s_return(slot(sc_eof_obj));
            }
            else if (sc->tok == TOK_RPAREN)
            {
                int c = inchar(local(scheme_object));

                if (c != '\n')
                    backchar(local(scheme_object), c);
#if SHOW_ERROR_LINE
                else if (sc->load_stack[sc->file_i].kind & port_file)
                    sc->load_stack[sc->file_i].rep.stdio.curr_line++;
#endif
                sc->nesting_stack[sc->file_i]--;
                s_return(reverse_in_place(local(scheme_object), slot(sc_nil), slot(sc_args)));
            }
            else if (sc->tok == TOK_DOT)
            {
                s_save(local(scheme_object), OP_RDDOT, slot(sc_args), slot(sc_nil));
                sc->tok = token(local(scheme_object));
                s_goto(OP_RDSEXPR);
            }
            else
            {
                s_save(local(scheme_object), OP_RDLIST, slot(sc_args), slot(sc_nil));;
                s_goto(OP_RDSEXPR);
            }
        }

        case OP_RDDOT:
            if (token(local(scheme_object)) != TOK_RPAREN)
            {
                Error_0("syntax error: illegal dot expression");
            }
            else
            {
                sc->nesting_stack[sc->file_i]--;
                s_return(reverse_in_place(local(scheme_object), slot(sc_value), slot(sc_args)));
            }

        case OP_RDQUOTE:
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
            local(x) = cons(local(scheme_object), slot(sc_quote), local(x));
            s_return(local(x));

        case OP_RDQQUOTE:
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
            local(x) = cons(local(scheme_object), slot(sc_qquote), local(x));
            s_return(local(x));

        case OP_RDQQUOTEVEC:
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
            local(x) = cons(local(scheme_object), slot(sc_qquote), local(x));
            local(x) = cons(local(scheme_object), local(x), slot(sc_nil));
            local(x) = cons(local(scheme_object), mk_symbol(local(scheme_object), "vector"), local(x));
            local(x) = cons(local(scheme_object), mk_symbol(local(scheme_object), "apply"), local(x));
            s_return(local(x));

        case OP_RDUNQUOTE:
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
            local(x) = cons(local(scheme_object), slot(sc_unquote), local(x));
            s_return(local(x));

        case OP_RDUQTSP:
            local(x) = cons(local(scheme_object), slot(sc_value), slot(sc_nil));
            local(x) = cons(local(scheme_object), slot(sc_unquotesp), local(x));
            s_return(local(x));

        case OP_RDVEC:
            /*sc->code=cons(sc,mk_proc(sc,OP_VECTOR),sc->value);
             * s_goto(sc,OP_EVAL); Cannot be quoted */
            /*x=cons(sc,mk_proc(sc,OP_VECTOR),sc->value);
             * s_return(sc,x); Cannot be part of pairs */
            /*sc->code=mk_proc(sc,OP_VECTOR);
             * sc->args=sc->value;
             * s_goto(sc,OP_APPLY); */
            setslot(sc_args, slot(sc_value));
            s_goto(OP_VECTOR);

            /* ========== printing part ========== */
        case OP_P0LIST:
            if (is_vector(gc, slot(sc_args)))
            {
                putstr(local(scheme_object), "#(");
                local(x) = mk_integer(gc, 0);
                local(x) = cons(local(scheme_object), slot(sc_args), local(x));
                setslot(sc_args, local(x));
                s_goto(OP_PVECFROM);
            }
            else if (is_environment(gc, slot(sc_args)))
            {
                putstr(local(scheme_object), "#<ENVIRONMENT>");
                s_return(slot(sc_t));
            }
            else if (!is_pair(gc, slot(sc_args)))
            {
                printatom(local(scheme_object), slot(sc_args), sc->print_flag);
                s_return(slot(sc_t));
            }
            else if (car(slot(sc_args)) == slot(sc_quote) && ok_abbrev(gc, cdr(slot(sc_args))))
            {
                putstr(local(scheme_object), "'");
                setslot(sc_args, cadr(slot(sc_args)));
                s_goto(OP_P0LIST);
            }
            else if (car(slot(sc_args)) == slot(sc_qquote) && ok_abbrev(gc, cdr(slot(sc_args))))
            {
                putstr(local(scheme_object), "`");
                setslot(sc_args, cadr(slot(sc_args)));
                s_goto(OP_P0LIST);
            }
            else if (car(slot(sc_args)) == slot(sc_unquote) && ok_abbrev(gc, cdr(slot(sc_args))))
            {
                putstr(local(scheme_object), ",");
                setslot(sc_args, cadr(slot(sc_args)));
                s_goto(OP_P0LIST);
            }
            else if (car(slot(sc_args)) == slot(sc_unquotesp) && ok_abbrev(gc, cdr(slot(sc_args))))
            {
                putstr(local(scheme_object), ",@");
                setslot(sc_args, cadr(slot(sc_args)));
                s_goto(OP_P0LIST);
            }
            else
            {
                putstr(local(scheme_object), "(");
                s_save(local(scheme_object), OP_P1LIST, cdr(slot(sc_args)), slot(sc_nil));
                setslot(sc_args, car(slot(sc_args)));
                s_goto(OP_P0LIST);
            }

        case OP_P1LIST:
            if (is_pair(gc, slot(sc_args)))
            {
                s_save(local(scheme_object), OP_P1LIST, cdr(slot(sc_args)), slot(sc_nil));
                putstr(local(scheme_object), " ");
                setslot(sc_args, car(slot(sc_args)));
                s_goto(OP_P0LIST);
            }
            else if (is_vector(gc, slot(sc_args)))
            {
                s_save(local(scheme_object), OP_P1LIST, slot(sc_nil), slot(sc_nil));
                putstr(local(scheme_object), " . ");
                s_goto(OP_P0LIST);
            }
            else
            {
                if (slot(sc_args) != slot(sc_nil))
                {
                    putstr(local(scheme_object), " . ");
                    printatom(local(scheme_object), slot(sc_args), sc->print_flag);
                }
                putstr(local(scheme_object), ")");
                s_return(slot(sc_t));
            }
        case OP_PVECFROM:
        {
            int i = ivalue_unchecked(cdr(slot(sc_args)));
            local(x) = car(slot(sc_args));
            int len = local(x)->fields.slot_count;

            if (i == len)
            {
                putstr(local(scheme_object), ")");
                s_return(slot(sc_t));
            }
            else
            {
                local(x) = vector_elem(gc, local(x), i);

                local(y) = cdr(slot(sc_args));
                num *n = get_byte(local(y), 0);
                gc_lock_object(gc, local(y));
                n->value.ivalue = i + 1;
                gc_unlock_object(gc, local(y));

                s_save(local(scheme_object), OP_PVECFROM, slot(sc_args), slot(sc_nil));
                setslot(sc_args, local(x));
                if (i > 0)
                {
                    putstr(local(scheme_object), " ");
                }

                s_goto(OP_P0LIST);
            }
        }

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);

    }
    scope_return slot(sc_t);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_6(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, x, y)
{
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_LAMBDA, sc_lambda);
    defslot(local(scheme_object), SCHEME_VALUE, sc_value);

    long v;

    switch (op)
    {
        case OP_LIST_LENGTH:   /* length *//* a.k */
            v = list_length(local(scheme_object), car(slot(sc_args)));
            if (v < 0)
            {
                Error_1("length: not a list:", car(slot(sc_args)));
            }
            local(x) = mk_integer(gc, v);
            s_return(local(x));

        case OP_ASSQ:          /* assq *//* a.k */
            local(x) = car(slot(sc_args));
            for (local(y) = cadr(slot(sc_args)); is_pair(gc, local(y)); local(y) = cdr(local(y)))
            {
                if (!is_pair(gc, car(local(y))))
                {
                    Error_0("unable to handle non pair element");
                }
                if (local(x) == caar(local(y)))
                    break;
            }
            if (is_pair(gc, local(y)))
            {
                s_return(car(local(y)));
            }
            else
            {
                s_return(slot(sc_f));
            }


        case OP_GET_CLOSURE:   /* get-closure-code *//* a.k */
            setslot(sc_args, car(slot(sc_args)));
            if (slot(sc_args) == slot(sc_nil))
            {
                s_return(slot(sc_f));
            }
            else if (is_closure(slot(sc_args)))
            {
                local(x) = cons(local(scheme_object), slot(sc_lambda), closure_code(gc, slot(sc_value)));
                s_return(local(x));
            }
            else if (is_macro(slot(sc_args)))
            {
                local(x) = cons(local(scheme_object), slot(sc_lambda), closure_code(gc, slot(sc_value)));
                s_return(local(x));
            }
            else
            {
                s_return(slot(sc_f));
            }

        case OP_CLOSUREP:      /* closure? */
            /*
             * Note, macro object is also a closure.
             * Therefore, (closure? <#MACRO>) ==> #t
             */
            s_retbool(is_closure(car(slot(sc_args))));
        case OP_MACROP:        /* macro? */
            s_retbool(is_macro(car(slot(sc_args))));
        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }
    scope_return slot(sc_t);               /* NOTREACHED */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

static pointer opexe_7(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, t)
{
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);
    defslot(local(scheme_object), SCHEME_HASH_T, sc_t);
    defslot(local(scheme_object), SCHEME_HASH_F, sc_f);
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);

    switch (op)
    {
        case OP_CURRENT_THREAD:
            s_return(local(scheme_object));

        case OP_THREADP:
            s_retbool(is_scheme(gc, car(slot(sc_args))));

        case OP_THREAD_FINISHED:
        {
            pointer t = car(slot(sc_args));
            if (!is_scheme(gc, t))
            {
                Error_1("thread-finished?: not a thread: ", t);
            }
            else
            {
                scheme *t_sc = *((scheme **) object_get_byte(t, 0));
                s_retbool(!t_sc->is_working);
            }
        }

        case OP_THREAD_CREATE:
        {
            local(t) = car(slot(sc_args));
            pointer result = scheme_create_thread(local(scheme_object), local(t));
            if (NULL == result)
            {
                Error_1("thread-create: failed to created thread from: ", local(t));
            }
            else
            {
                s_return(result);
            }
        }

        case OP_THREAD_GET_ID:
        {
            if (slot(sc_nil) == slot(sc_args))
            {
                local(t) = local(scheme_object);
            }
            else
            {
                local(t) = car(slot(sc_args));
            }
            scheme *arg_sc = *((scheme **) object_get_byte(local(t), 0));
            uint64_t arg_tid = pthread_getthreadid(&arg_sc->thread);
            snprintf(sc->strbuff, STRBUFFSIZE, "%" PRIx64, arg_tid);
            local(t) = mk_string(local(scheme_object), sc->strbuff);
            s_return(local(t));
        }

        case OP_THREAD_JOIN:
        {
            local(t) = car(slot(sc_args));
            if (local(t) == local(scheme_object))
            {
                Error_0("thread-join: can't join self");
            }

            scheme *arg_sc = *((scheme **) object_get_byte(local(t), 0));

            if (!arg_sc->is_working)
            {
                s_return(get(local(t), SCHEME_VALUE));
            }

            check_return(tracing_mutex_lock(&sc->global_scheme->global_lock, ""), "pthread_mutex_lock() failed.");
            bool was_joined = arg_sc->was_joined;

            if (was_joined)
            {
                check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
                Error_1("thread-join: thread was already joined: ", local(t));
            }

            arg_sc->was_joined = true;
            check(scheme_unregister_vm_thread(sc->global_scheme->vm_threads, arg_sc), "Failed to unregister VM thread.");
            check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");

            gc_vm_thread_fall_asleep(gc);
            check_return(pthread_join(arg_sc->thread, NULL), "pthread_join() failed.");
            gc_vm_thread_awoke(gc);
            s_return(get(local(t), SCHEME_VALUE));
        }

        case OP_MKMUTEX:
        {
            local(t) = mk_mutex(sc);
            s_return(local(t));
        }

        case OP_MUTEXP:
            s_retbool(is_mutex(gc, car(slot(sc_args))));

        case OP_MUTEX_LOCK:
        case OP_MUTEX_UNLOCK:
        case OP_MUTEX_TRYLOCK:
        {
            int (*f)(pthread_mutex_t *) = NULL;
            switch (op)
            {
                case OP_MUTEX_LOCK:
                    f = pthread_mutex_lock;
                    break;

                case OP_MUTEX_UNLOCK:
                    f = pthread_mutex_unlock;
                    break;

                case OP_MUTEX_TRYLOCK:
                    f = pthread_mutex_trylock;
                    break;

                default:
                    abort();
            }

            local(t) = car(slot(sc_args));
            pthread_mutex_t *mutex = *((pthread_mutex_t **) object_get_byte(local(t), 0));

            if (pthread_mutex_lock == f)
            {
                gc_vm_thread_fall_asleep(gc);
            }

            int e = f(mutex);

            if (pthread_mutex_lock == f)
            {
                gc_vm_thread_awoke(gc);
            }

            if (0 == e)
            {
                s_return(slot(sc_t));
            }
            else if (pthread_mutex_trylock == f && EBUSY == e)
            {
                s_return(slot(sc_f));
            }
            else
            {
                local(t) = mk_string(local(scheme_object), strerror(e));
                s_return(local(t));
            }
        }

        default:
            snprintf(sc->strbuff, STRBUFFSIZE, "%d: illegal operator", sc->op);
            Error_0(sc->strbuff);
    }

    scope_return slot(sc_t);
    error:
    abort();
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

typedef pointer(*dispatch_func) (pointer scheme_object, enum scheme_opcodes);

#if defined(NO_DEBUG)
typedef int (*test_predicate) (pointer);
#else
typedef int (*test_predicate) (GC *, pointer);
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
static int is_any(pointer p) // Safe.
#else
static int is_any(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    (void) p;
    #if !defined(NO_DEBUG)
    (void) gc;
    #endif // !defined(NO_DEBUG).
    return 1;
}

#if defined(NO_DEBUG)
static int is_nonneg(pointer p) // Safe.
#define is_nonneg(gc, p) is_nonneg(p)
#else
static int is_nonneg(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    return is_integer(gc, p) && ivalue(gc, p) >= 0 ;
}

/* Correspond carefully with following defines! */
static struct
{
    test_predicate fct;
    const char *kind;
} tests[] =
{
    {0, 0},                      /* unused */
    {is_any, 0},
    {is_string, "string"},
    {is_symbol, "symbol"},
    {is_port, "port"},
    {is_inport, "input port"},
    {is_outport, "output port"},
    {is_environment, "environment"},
    {is_pair, "pair"},
    {0, "pair or '()"},
    {is_character, "character"},
    {is_vector, "vector"},
    {is_number, "number"},
    {is_integer, "integer"},
    {is_nonneg, "non-negative integer"},
    {is_mutex, "mutex"},
    {is_scheme, "thread"}
};

#define TST_NONE 0
#define TST_ANY "\001"
#define TST_STRING "\002"
#define TST_SYMBOL "\003"
#define TST_PORT "\004"
#define TST_INPORT "\005"
#define TST_OUTPORT "\006"
#define TST_ENVIRONMENT "\007"
#define TST_PAIR "\010"
#define TST_LIST "\011"
#define TST_CHAR "\012"
#define TST_VECTOR "\013"
#define TST_NUMBER "\014"
#define TST_INTEGER "\015"
#define TST_NATURAL "\016"
#define TST_MUTEX "\017"
#define TST_SCHEME "\020"

typedef struct
{
    dispatch_func func;
    char *name;
    int min_arity;
    int max_arity;
    char *arg_tests_encoding;
} op_code_info;

#define INF_ARG 0xffff

static op_code_info dispatch_table[] = {
#define _OP_DEF(A,B,C,D,E,OP) {A,B,C,D,E},
#include "opdefines.h"
    {0}
};

#if defined(NO_DEBUG)
#undef procname
static const char *procname(pointer x) // Safe.
#define procname(gc, x) procname(x)
#else
static const char *procname(GC *gc, pointer x) // Safe.
#endif // define(NO_DEBUG).
{
    const char *result = "ILLEGAL!";

    if (gc_object_get_size_in_bytes(gc, x) >= (OBJECT_SLOT_SIZE + sizeof(num)))
    {
        int n = procnum(gc, x);
        const char *name = dispatch_table[n].name;

        if (name != 0)
        {
            result = name;
        }
    }
    return result;
}

/* kernel of this interpreter */
static void Eval_Cycle(pointer _scheme_object, enum scheme_opcodes op)
scope(scheme_object, sc, gc, sc_args, sc_nil)
{
    defslot(local(scheme_object), SCHEME_ARGS, sc_args);
    defslot(local(scheme_object), SCHEME_NIL, sc_nil);

    sc->op = op;
    for (;;)
    {
        op_code_info *pcd = dispatch_table + sc->op;

        if (pcd->name != 0)
        {                       /* if built-in function, check arguments */
            char msg[STRBUFFSIZE];
            int ok = 1;
            int n = list_length(local(scheme_object), slot(sc_args));

            /* Check number of arguments */
            if (n < pcd->min_arity)
            {
                ok = 0;
                snprintf(msg, STRBUFFSIZE, "%s: needs%s %d argument(s)",
                         pcd->name, pcd->min_arity == pcd->max_arity ? "" : " at least", pcd->min_arity);
            }
            if (ok && n > pcd->max_arity)
            {
                ok = 0;
                snprintf(msg, STRBUFFSIZE, "%s: needs%s %d argument(s)",
                         pcd->name, pcd->min_arity == pcd->max_arity ? "" : " at most", pcd->max_arity);
            }
            if (ok)
            {
                if (pcd->arg_tests_encoding != 0)
                {
                    int i = 0;
                    int j;
                    const char *t = pcd->arg_tests_encoding;
                    pointer arglist = slot(sc_args);

                    do
                    {
                        pointer arg = pair_car(gc, arglist);

                        j = (int) t[0];
                        if (j == TST_LIST[0])
                        {
                            if (arg != slot(sc_nil) && !is_pair(gc, arg))
                                break;
                        }
                        else
                        {
                            #if defined(NO_DEBUG)
                            if (!tests[j].fct(arg))
                            #else
                            if (!tests[j].fct(gc, arg))
                            #endif // defined(NO_DEBUG).
                                break;
                        }

                        if (t[1] != 0)
                        {       /* last test is replicated as necessary */
                            t++;
                        }
                        arglist = pair_cdr(gc, arglist);
                        i++;
                    } while (i < n);
                    if (i < n)
                    {
                        ok = 0;
                        snprintf(msg, STRBUFFSIZE, "%s: argument %d must be: %s", pcd->name, i + 1, tests[j].kind);
                    }
                }
            }
            if (!ok)
            {
                if (_Error_1(local(scheme_object), msg, 0) == slot(sc_nil))
                {
                    scope_return;
                }
                pcd = dispatch_table + sc->op;
            }
        }
        ok_to_freely_gc(local(scheme_object), false);
        pointer retval = pcd->func(local(scheme_object), (enum scheme_opcodes) sc->op);
        if (retval == slot(sc_nil))
        {
            scope_return;
        }
        if (sc->global_scheme->no_memory)
        {
            fprintf(stderr, "No memory!\n");
            scope_return;
        }
    }
} endscope;

/* ========== Initialization of internal keywords ========== */

static void assign_syntax(pointer scheme_object, char *name)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    pointer x = oblist_add_by_name(scheme_object, name);
    settypeflag(x, typeflag(x) | T_SYNTAX);
}

static void assign_proc(pointer _scheme_object, enum scheme_opcodes op, char *name)
scope(scheme_object, sc, gc, x, y)
{
    local(x) = mk_symbol(local(scheme_object), name);
    local(y) = mk_proc(local(scheme_object), op);
    new_slot_in_env(local(scheme_object), local(x), local(y));
} endscope;

static pointer mk_proc(pointer scheme_object, enum scheme_opcodes op)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    pointer y = gc_alloc(gc, false, (unsigned) T_PROC, 0, sizeof(num));
    num *n = object_get_byte(y, 0);

    gc_lock_object(gc, y);
    n->value.ivalue = (long) op;
    set_num_integer(y);
    gc_unlock_object(gc, y);

    return y;
}

/* Hard-coded for the given keywords. Remember to rewrite if more are added! */
#if defined(NO_DEBUG)
#undef syntaxnum
static int syntaxnum(pointer p) // Safe.
#define syntaxnum(gc, p) syntaxnum(p)
#else
static int syntaxnum(GC *gc, pointer p) // Safe.
#endif // defined(NO_DEBUG).
{
    const char *s = strvalue(pair_car(gc, p));

    switch (strlen(s))
    {
        case 2:
            if (s[0] == 'i')
                return OP_IF0;  /* if */
            else
                return OP_OR0;  /* or */
        case 3:
            if (s[0] == 'a')
                return OP_AND0; /* and */
            else
                return OP_LET0; /* let */
        case 4:
            switch (s[3])
            {
                case 'e':
                    return OP_CASE0;    /* case */
                case 'd':
                    return OP_COND0;    /* cond */
                case '*':
                    return OP_LET0AST;  /* let* */
                default:
                    return OP_SET0; /* set! */
            }
        case 5:
            switch (s[2])
            {
                case 'g':
                    return OP_BEGIN;    /* begin */
                case 'l':
                    return OP_DELAY;    /* delay */
                case 'c':
                    return OP_MACRO0;   /* macro */
                default:
                    return OP_QUOTE;    /* quote */
            }
        case 6:
            switch (s[2])
            {
                case 'm':
                    return OP_LAMBDA;   /* lambda */
                case 'f':
                    return OP_DEF0; /* define */
                default:
                    return OP_LET0REC;  /* letrec */
            }
        default:
            return OP_C0STREAM; /* cons-stream */
    }
}

/* initialization of TinyScheme */
#if USE_INTERFACE
INTERFACE static pointer s_cons(pointer scheme_object, pointer a, pointer b)
{
    return cons(scheme_object, a, b);
}

INTERFACE static pointer s_immutable_cons(pointer scheme_object, pointer a, pointer b)
{
    return immutable_cons(scheme_object, a, b);
}

static struct scheme_interface vtbl = {
    scheme_define,
    s_cons,
    s_immutable_cons,
    mk_integer,
    mk_real,
    mk_symbol,
    gensym,
    mk_string,
    mk_counted_string,
    mk_character,
    mk_vector,
    mk_foreign_func,
    putstr,
    putcharacter,

    is_string,
    string_value,
    is_number,
    nvalue,
    ivalue,
    rvalue,
    is_integer,
    is_real,
    is_character,
    charvalue,
    is_list,
    is_vector,
    list_length,
    vector_length,
    fill_vector,
    vector_elem,
    set_vector_elem,
    is_port,
    is_pair,
    pair_car,
    pair_cdr,
    set_car,
    set_cdr,

    is_symbol,
    symname,

    is_syntax,
    is_proc,
    is_foreign,
    syntaxname,
    is_closure,
    is_macro,
    closure_code,
    closure_env,

    is_continuation,
    is_promise,
    is_environment,
    is_immutable,
    setimmutable,

    scheme_load_file,
    scheme_load_string
};
#endif

void *scheme_thread(void *data)
{
    pointer scheme_object = (pointer) data;
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_vm_register_thread(gc);
    gc_vm_thread_awoke(gc);

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer thread_func = object_get_slot(gc, scheme_object, SCHEME_ARGS),
            sc_nil      = object_get_slot(gc, scheme_object, SCHEME_NIL);
    object_set_slot(gc, scheme_object, SCHEME_ARGS, sc_nil);

    scheme_call(scheme_object, thread_func, sc_nil);

    gc_untrack_reference(gc, &scheme_object);

    sc->is_working = false;

    gc_vm_thread_fall_asleep(gc);
    gc_vm_unregister_thread(gc);

    error:
    return NULL;
}

bool scheme_unregister_vm_thread(DynamicArray *vm_threads, scheme *thread)
{
    size_t vm_thread_count = dynamic_array_count(vm_threads);
    size_t i = 0;
    for (; i < vm_thread_count; ++i)
    {
        scheme *vm_thread = *DYNAMIC_ARRAY_GET((scheme **), vm_threads, i);
        if (vm_thread == thread)
        {
            dynamic_array_delete_at(vm_threads, i);
            break;
        }
    }

    return i < vm_thread_count;
}

pointer scheme_create_thread(pointer scheme_object, pointer thread_func)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    scheme *new_sc = sc->malloc(sizeof(scheme));
    if (NULL == new_sc)
    {
        return NULL;
    }
    memset(new_sc, 0, sizeof(scheme));

    check_return(tracing_mutex_lock(&sc->global_scheme->global_lock, ""), "pthread_mutex_lock() failed.");
    bool registered_new_thread = dynamic_array_push(sc->global_scheme->vm_threads, &new_sc);
    check_return(tracing_mutex_unlock(&sc->global_scheme->global_lock, ""), "pthread_mutex_unlock() failed.");
    check(registered_new_thread, "Failed to add new VM threads to vm_threads list.");

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    gc_track_reference(gc, &thread_func, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer new_scheme_object = gc_alloc(gc, false, (unsigned) T_SCHEME, SCHEME_OBJECT_SLOT_COUNT, sizeof(scheme *));
    if (NULL == new_scheme_object)
    {
        gc_untrack_reference(gc, &scheme_object);
        gc_untrack_reference(gc, &thread_func);
        sc->free(new_sc);
        return NULL;
    }

    for (size_t slot_index = 0; slot_index < SCHEME_OBJECT_SLOT_COUNT; ++slot_index)
    {
        object_set_slot(gc, new_scheme_object, slot_index, object_get_slot(gc, scheme_object, slot_index));
    }

    gc_untrack_reference(gc, &scheme_object);
    gc_untrack_reference(gc, &thread_func);

    gc_lock_object(gc, new_scheme_object);
    memcpy(object_get_byte(new_scheme_object, 0), &new_sc, sizeof(scheme *));
    gc_unlock_object(gc, new_scheme_object);

    *new_sc = *sc;

    pointer sc_nil = object_get_slot(gc, new_scheme_object, SCHEME_NIL);

    object_set_slot(gc, new_scheme_object, SCHEME_INPORT, sc_nil);
    object_set_slot(gc, new_scheme_object, SCHEME_SAVE_INPORT, sc_nil);
    object_set_slot(gc, new_scheme_object, SCHEME_LOADPORT, sc_nil);

    new_sc->gensym_cnt = 0;
    new_sc->nesting = 0;
    new_sc->interactive_repl = 0;
    new_sc->gc_verbose = 0;
    new_sc->tracing = 0;
    new_sc->retcode = 0;
    new_sc->file_i = 0;
    memset(new_sc->load_stack, 0, sizeof(new_sc->load_stack));
    memset(new_sc->nesting_stack, 0, sizeof(new_sc->nesting_stack));
    new_sc->no_memory = 0;
    memset(new_sc->linebuff, 0, sizeof(new_sc->linebuff));
    memset(new_sc->strbuff, 0, sizeof(new_sc->strbuff));
    new_sc->tmpfp = NULL;
    new_sc->tok = 0;
    new_sc->print_flag = 0;
    new_sc->op = 0;
    new_sc->is_working = true;
    new_sc->was_joined = false;

    dump_stack_initialize(new_scheme_object);

    object_set_slot(gc, new_scheme_object, SCHEME_CODE, sc_nil);
    object_set_slot(gc, new_scheme_object, SCHEME_C_NEST, sc_nil);
    object_set_slot(gc, new_scheme_object, SCHEME_ARGS, thread_func);

    // SCHEME_OBLIST of child thread points to global scheme object.
    if (sc == sc->global_scheme)
    {
        object_set_slot(gc, new_scheme_object, SCHEME_OBLIST, scheme_object);
    }

    check_return(pthread_create(&new_sc->thread, NULL, scheme_thread, new_scheme_object), "pthread_create() failed.");

    return new_scheme_object;

    error:
    abort();
    return NULL;
}

bool scheme_init_new(GC *gc, func_alloc malloc_handler, func_dealloc free_handler, pointer *scheme_object_out)
{
    scheme *sc = (scheme *) (NULL != malloc_handler ? malloc_handler : malloc)(sizeof(scheme));
    if (NULL == sc)
    {
        return false;
    }
    memset(sc, 0, sizeof(scheme));

    pthread_mutexattr_t global_lock_mutex_attributes;
    memset(&global_lock_mutex_attributes, 0, sizeof(global_lock_mutex_attributes));
    check_return(pthread_mutexattr_init(&global_lock_mutex_attributes), "pthread_mutexattr_init() failed.");
    check_return(pthread_mutexattr_settype(&global_lock_mutex_attributes, PTHREAD_MUTEX_RECURSIVE), "pthread_mutexatttr_settype() failed.");
    check_return(pthread_mutex_init(&sc->global_lock, &global_lock_mutex_attributes), "pthread_mutex_init() failed.");
    check_return(pthread_mutexattr_destroy(&global_lock_mutex_attributes), "pthread_mutexattr_destroy() failed.");

    pointer scheme_object = gc_alloc(gc, false, (unsigned) T_SCHEME, SCHEME_OBJECT_SLOT_COUNT, sizeof(scheme *));
    check_mem(scheme_object);
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    gc_lock_object(gc, scheme_object);
    memcpy(object_get_byte(scheme_object, 0), &sc, sizeof(scheme *));
    gc_unlock_object(gc, scheme_object);

    int i, n = sizeof(dispatch_table) / sizeof(dispatch_table[0]);
    pointer x;

    num_zero.is_fixnum = 1;
    num_zero.value.ivalue = 0;
    num_one.is_fixnum = 1;
    num_one.value.ivalue = 1;

    sc->gc = gc;
    sc->global_scheme = sc;

#if USE_INTERFACE
    sc->vptr = &vtbl;
#endif
    sc->gensym_cnt = 0;
    sc->malloc = NULL != malloc_handler ? malloc_handler : malloc;
    sc->free = NULL != free_handler ? free_handler : free;

    pointer t = gc_alloc(gc, false, 0, 0, 0);
    object_set_slot(gc, scheme_object, SCHEME_NIL, t);
    object_set_slot(gc, t, 0, t);

    object_fill_slots(gc, scheme_object, t);

    t = gc_alloc(gc, false, 0, 0, 0);
    object_set_slot(gc, scheme_object, SCHEME_HASH_T, t);
    t = gc_alloc(gc, false, 0, 0, 0);
    object_set_slot(gc, scheme_object, SCHEME_HASH_F, t);
    t = gc_alloc(gc, false, 0, 0, 0);
    object_set_slot(gc, scheme_object, SCHEME_EOF_OBJ, t);

    sc->no_memory = 0;

    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    gc_track_reference(gc, &sc_nil, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    object_set_slot(gc, scheme_object, SCHEME_INPORT, sc_nil);
    object_set_slot(gc, scheme_object, SCHEME_OUTPORT, sc_nil);
    object_set_slot(gc, scheme_object, SCHEME_SAVE_INPORT, sc_nil);
    object_set_slot(gc, scheme_object, SCHEME_LOADPORT, sc_nil);

    sc->nesting = 0;
    sc->interactive_repl = 0;

    sc->gc_verbose = 0;
    sc->tracing = 0;
    dump_stack_initialize(scheme_object);
    object_set_slot(gc, scheme_object, SCHEME_CODE, sc_nil);

    /* init c_nest */
    object_set_slot(gc, scheme_object, SCHEME_C_NEST, sc_nil);

    t = oblist_initial_value(scheme_object);
    object_set_slot(gc, scheme_object, SCHEME_OBLIST, t);
    /* init global_env */
    new_frame_in_env(scheme_object, sc_nil);
    object_set_slot(gc, scheme_object, SCHEME_GLOBAL_ENV, object_get_slot(gc, scheme_object, SCHEME_ENVIR));
    /* init else */
    x = mk_symbol(scheme_object, "else");
    new_slot_in_env(scheme_object, x, object_get_slot(gc, scheme_object, SCHEME_HASH_T));

    assign_syntax(scheme_object, "lambda");
    assign_syntax(scheme_object, "quote");
    assign_syntax(scheme_object, "define");
    assign_syntax(scheme_object, "if");
    assign_syntax(scheme_object, "begin");
    assign_syntax(scheme_object, "set!");
    assign_syntax(scheme_object, "let");
    assign_syntax(scheme_object, "let*");
    assign_syntax(scheme_object, "letrec");
    assign_syntax(scheme_object, "cond");
    assign_syntax(scheme_object, "delay");
    assign_syntax(scheme_object, "and");
    assign_syntax(scheme_object, "or");
    assign_syntax(scheme_object, "cons-stream");
    assign_syntax(scheme_object, "macro");
    assign_syntax(scheme_object, "case");

    for (i = 0; i < n; i++)
    {
        if (dispatch_table[i].name != 0)
        {
            assign_proc(scheme_object, (enum scheme_opcodes) i, dispatch_table[i].name);
        }
    }

    /* initialization of global pointers to special symbols */
    pointer s = mk_symbol(scheme_object, "lambda");
    object_set_slot(gc, scheme_object, SCHEME_LAMBDA, s);
    s = mk_symbol(scheme_object, "quote");
    object_set_slot(gc, scheme_object, SCHEME_QUOTE, s);
    s = mk_symbol(scheme_object, "quasiquote");
    object_set_slot(gc, scheme_object, SCHEME_QQUOTE, s);
    s = mk_symbol(scheme_object, "unquote");
    object_set_slot(gc, scheme_object, SCHEME_UNQUOTE, s);
    s = mk_symbol(scheme_object, "unquote-splicing");
    object_set_slot(gc, scheme_object, SCHEME_UNQUOTESP, s);
    s = mk_symbol(scheme_object, "=>");
    object_set_slot(gc, scheme_object, SCHEME_FEED_TO, s);
    s = mk_symbol(scheme_object, "*colon-hook*");
    object_set_slot(gc, scheme_object, SCHEME_COLON_HOOK, s);
    s = mk_symbol(scheme_object, "*error-hook*");
    object_set_slot(gc, scheme_object, SCHEME_ERROR_HOOK, s);
    s = mk_symbol(scheme_object, "*sharp-hook*");
    object_set_slot(gc, scheme_object, SCHEME_SHARP_HOOK, s);
    s = mk_symbol(scheme_object, "*compile-hook*");
    object_set_slot(gc, scheme_object, SCHEME_COMPILE_HOOK, s);

    gc_untrack_reference(gc, &sc_nil);
    gc_untrack_reference(gc, &scheme_object);

    check_mem(sc->vm_threads = dynamic_array_create(sizeof(scheme *), 0));

    *scheme_object_out = scheme_object;
    return !sc->no_memory;

    error:
    abort();
    return false;
}

void scheme_set_input_port_file(pointer scheme_object, FILE *fin)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    pointer new_port = port_from_file(scheme_object, fin, port_input);
    object_set_slot(gc, scheme_object, SCHEME_INPORT, new_port);
    gc_untrack_reference(gc, &scheme_object);
}

void scheme_set_input_port_string(pointer scheme_object, char *start, char *past_the_end)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    pointer new_port = port_from_string(scheme_object, start, past_the_end, port_input);
    object_set_slot(gc, scheme_object, SCHEME_INPORT, new_port);
    gc_untrack_reference(gc, &scheme_object);
}

void scheme_set_output_port_file(pointer scheme_object, FILE *fout)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    pointer new_port = port_from_file(scheme_object, fout, port_output);
    object_set_slot(gc, scheme_object, SCHEME_OUTPORT, new_port);
    gc_untrack_reference(gc, &scheme_object);
}

void scheme_set_output_port_string(pointer scheme_object, char *start, char *past_the_end)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;
    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
    pointer new_port = port_from_string(scheme_object, start, past_the_end, port_output);
    object_set_slot(gc, scheme_object, SCHEME_OUTPORT, new_port);
    gc_untrack_reference(gc, &scheme_object);
}

void scheme_set_external_data(scheme *sc, void *p) // Safe.
{
    sc->ext_data = p;
}

void scheme_deinit(pointer scheme_object) // Safe.
{
    int i;

#if SHOW_ERROR_LINE
    char *fname;
#endif
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    object_fill_slots(gc, scheme_object, NULL);

    sc->gc_verbose = 0;

#if SHOW_ERROR_LINE
    for (i = 0; i <= sc->file_i; i++)
    {
        if (sc->load_stack[i].kind & port_file)
        {
            fname = sc->load_stack[i].rep.stdio.filename;
            if (fname)
            {
                sc->free(fname);
                sc->load_stack[i].rep.stdio.filename = NULL;
            }
        }
    }
#endif

    void (*f)(void *) = sc->free;

    if (sc == sc->global_scheme)
    {
        pthread_mutex_destroy(&sc->global_lock);
        dynamic_array_destroy(sc->vm_threads);
        debug("Freed global scheme.");
    }
    else
    {
        pthread_mutex_t *global_lock = &sc->global_scheme->global_lock;
        check_return(tracing_mutex_lock(global_lock, ""), "pthread_mutex_lock() failed.");

        check(sc->global_scheme->is_working, "Global scheme stopped.");
        check(!sc->is_working, "Thread is still working.");

        if (!sc->was_joined)
        {
            check_return(pthread_detach(sc->thread), "pthread_detach() failed.");
            sc->was_joined = true;
            check(scheme_unregister_vm_thread(sc->global_scheme->vm_threads, sc), "Failed to unregister VM thread.");
        }

        check_return(tracing_mutex_unlock(global_lock, ""), "pthread_mutex_unlock() failed.");
    }

    f(sc);

    return;
    error:
    abort();
}

void scheme_load_file(pointer scheme_object, FILE *fin)
{
    scheme_load_named_file(scheme_object, fin, 0);
}

void scheme_load_named_file(pointer _scheme_object, FILE *fin, const char *filename)
scope(scheme_object, sc, gc, x)
{
    dump_stack_reset(local(scheme_object));

    set(local(scheme_object), SCHEME_ENVIR, get(local(scheme_object), SCHEME_GLOBAL_ENV));

    sc->file_i = 0;
    sc->load_stack[0].kind = port_input | port_file;
    sc->load_stack[0].rep.stdio.file = fin;

    local(x) = mk_port(gc, sc->load_stack);
    set(local(scheme_object), SCHEME_LOADPORT, local(x));

    sc->retcode = 0;
    if (fin == stdin)
    {
        sc->interactive_repl = 1;
    }

#if SHOW_ERROR_LINE
    sc->load_stack[0].rep.stdio.curr_line = 0;
    if (fin != stdin && filename)
    {
        if (NULL != sc->load_stack[0].rep.stdio.filename)
        {
            free(sc->load_stack[0].rep.stdio.filename);
        }
        sc->load_stack[0].rep.stdio.filename = store_string(sc, strlen(filename), filename, 0);
    }
#endif

    set(local(scheme_object), SCHEME_INPORT, get(local(scheme_object), SCHEME_LOADPORT));

    local(x) = mk_integer(gc, sc->file_i);
    set(local(scheme_object), SCHEME_ARGS, local(x));
    Eval_Cycle(local(scheme_object), OP_T0LVL);
    if (sc->retcode == 0)
    {
        sc->retcode = sc->nesting != 0;
    }
} endscope;

void scheme_load_string(pointer _scheme_object, const char *cmd)
scope(scheme_object, sc, gc, x)
{
    #if SHOW_ERROR_LINE
    if (sc->load_stack[0].kind & port_file)
    {
        /* Cleanup is here so (close-*-port) functions could work too */
        sc->load_stack[0].rep.stdio.curr_line = 0;

        if (sc->load_stack[0].rep.stdio.filename)
        {
            sc->free(sc->load_stack[0].rep.stdio.filename);
            sc->load_stack[0].rep.stdio.filename = NULL;
        }
    }
    #endif

    dump_stack_reset(local(scheme_object));
    set(local(scheme_object), SCHEME_ENVIR, get(local(scheme_object), SCHEME_GLOBAL_ENV));
    sc->file_i = 0;
    sc->load_stack[0].kind = port_input | port_string;
    sc->load_stack[0].rep.string.start = (char *) cmd;  /* This func respects const */
    sc->load_stack[0].rep.string.past_the_end = (char *) cmd + strlen(cmd);
    sc->load_stack[0].rep.string.curr = (char *) cmd;

    local(x) = mk_port(gc, sc->load_stack);
    set(local(scheme_object), SCHEME_LOADPORT, local(x));

    sc->retcode = 0;
    sc->interactive_repl = 0;
    set(local(scheme_object), SCHEME_INPORT, get(local(scheme_object), SCHEME_LOADPORT));

    local(x) = mk_integer(gc, sc->file_i);
    set(local(scheme_object), SCHEME_ARGS, local(x));

    Eval_Cycle(local(scheme_object), OP_T0LVL);

    if (sc->retcode == 0)
    {
        sc->retcode = sc->nesting != 0;
    }
} endscope;

void scheme_define(pointer scheme_object, pointer envir, pointer symbol, pointer value)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL),
            x      = find_slot_in_env(scheme_object, envir, symbol, 0);

    if (x != sc_nil)
    {
        set_slot_in_env(gc, x, value);
    }
    else
    {
        new_slot_spec_in_env(scheme_object, envir, symbol, value);
    }
}

void scheme_register_foreign_func(pointer scheme_object, scheme_registerable *sr)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer sc_global_env = object_get_slot(gc, scheme_object, SCHEME_GLOBAL_ENV);
    gc_track_reference(gc, &sc_global_env, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer new_symbol = mk_symbol(scheme_object, sr->name);
    gc_track_reference(gc, &new_symbol, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer new_foreign_func = mk_foreign_func(gc, sr->f);
    gc_track_reference(gc, &new_foreign_func, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    scheme_define(scheme_object, sc_global_env, new_symbol, new_foreign_func);

    gc_untrack_reference(gc, &new_foreign_func);
    gc_untrack_reference(gc, &new_symbol);
    gc_untrack_reference(gc, &sc_global_env);
    gc_untrack_reference(gc, &scheme_object);
}

void scheme_register_foreign_func_list(pointer scheme_object, scheme_registerable *list, int count)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    for (int i = 0; i < count; i++)
    {
        scheme_register_foreign_func(scheme_object, list + i);
    }

    gc_untrack_reference(gc, &scheme_object);
}

pointer scheme_apply0(pointer scheme_object, const char *procname)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer sc_nil = object_get_slot(gc, scheme_object, SCHEME_NIL);
    gc_track_reference(gc, &sc_nil, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer new_symbol = mk_symbol(scheme_object, procname),
            new_cons   = cons(scheme_object, new_symbol, sc_nil);

    pointer result = scheme_eval(scheme_object, new_cons);

    gc_untrack_reference(gc, &sc_nil);
    gc_untrack_reference(gc, &scheme_object);
    return result;
}

void save_from_C_call(pointer scheme_object)
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer saved_data = cons(scheme_object,
                              object_get_slot(gc, scheme_object, SCHEME_ENVIR),
                              object_get_slot(gc, scheme_object, SCHEME_DUMP));

    /* Push */
    pointer new_cons = cons(scheme_object, saved_data, object_get_slot(gc, scheme_object, SCHEME_C_NEST));
    object_set_slot(gc,
                    scheme_object,
                    SCHEME_C_NEST,
                    new_cons);

    /* Truncate the dump stack so TS will return here when done, not
     * directly resume pre-C-call operations. */
    dump_stack_reset(scheme_object);

    gc_untrack_reference(gc, &scheme_object);
}

void restore_from_C_call(pointer scheme_object) // Safe.
{
    scheme *sc = *((scheme **) object_get_byte(scheme_object, 0));
    GC *gc = sc->gc;

    pointer saved_data = pair_car(gc, object_get_slot(gc, scheme_object, SCHEME_C_NEST));

    object_set_slot(gc, scheme_object, SCHEME_ENVIR, pair_car(gc, saved_data));
    object_set_slot(gc, scheme_object, SCHEME_DUMP, pair_cdr(gc, saved_data));
    /* Pop */
    object_set_slot(gc,
                    scheme_object,
                    SCHEME_C_NEST,
                    pair_cdr(gc, object_get_slot(gc, scheme_object, SCHEME_C_NEST)));
}

/* "func" and "args" are assumed to be already eval'ed. */
pointer scheme_call(pointer _scheme_object, pointer _func, pointer _args)
scope(scheme_object, sc, gc, func, args)
{
    local(func) = _func;
    local(args) = _args;

    int old_repl = sc->interactive_repl;
    sc->interactive_repl = 0;
    save_from_C_call(local(scheme_object));
    set(local(scheme_object), SCHEME_ENVIR, get(local(scheme_object), SCHEME_GLOBAL_ENV));
    set(local(scheme_object), SCHEME_ARGS, local(args));
    set(local(scheme_object), SCHEME_CODE, local(func));
    sc->retcode = 0;
    Eval_Cycle(local(scheme_object), OP_APPLY);
    sc->interactive_repl = old_repl;
    restore_from_C_call(local(scheme_object));
    scope_return get(local(scheme_object), SCHEME_VALUE);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

pointer scheme_eval(pointer _scheme_object, pointer _obj)
scope(scheme_object, sc, gc, obj)
{
    local(obj) = _obj;

    int old_repl = sc->interactive_repl;
    sc->interactive_repl = 0;
    save_from_C_call(local(scheme_object));
    set(local(scheme_object), SCHEME_ARGS, get(local(scheme_object), SCHEME_NIL));
    set(local(scheme_object), SCHEME_CODE, local(obj));
    sc->retcode = 0;
    Eval_Cycle(local(scheme_object), OP_EVAL);
    sc->interactive_repl = old_repl;
    restore_from_C_call(local(scheme_object));
    scope_return get(local(scheme_object), SCHEME_VALUE);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wreturn-type"
} endscope;
#pragma GCC diagnostic pop

/* ========== Main ========== */

#if defined(SYNCHRONIZED_TRACING)
pthread_mutex_t tracing_lock;
#endif // defined(SYNCHRONIZED_TRACING).

uint64_t pthread_getthreadid(pthread_t *arg)
{
    pthread_t thread = arg ? *arg : pthread_self();
    uint64_t result = *((uint64_t *) (&thread));
    return result;
}

#if defined(__APPLE__) && !defined (OSX)
int main()
{
    extern MacTS_main(int argc, char **argv);
    char **argv;
    int argc = ccommand(&argv);

    MacTS_main(argc, argv);
    return 0;
}

int MacTS_main(int argc, char **argv)
{
#else
int main(int argc, char **argv)
{
#endif
    pointer scheme_object = NULL;
    scheme *sc = NULL;
    FILE *fin = NULL;
    char *file_name = InitFile;
    int retcode;
    int isfile = 1;

    if (argc == 1)
    {
        printf(banner);
    }
    if (argc == 2 && strcmp(argv[1], "-?") == 0)
    {
        printf("Usage: tinyscheme -?\n");
        printf("or:    tinyscheme [<file1> <file2> ...]\n");
        printf("followed by\n");
        printf("          -1 <file> [<arg1> <arg2> ...]\n");
        printf("          -c <Scheme commands> [<arg1> <arg2> ...]\n");
        printf("assuming that the executable is named tinyscheme.\n");
        printf("Use - as filename for stdin.\n");
        return 1;
    }

    time_t seconds = time(NULL);
    check(0 != seconds, "time() failed.");
    srandom(seconds);

    check_return(pthread_mutex_init(&tracing_lock, NULL), "pthread_mutex_init() failed.");

    size_t pool_size = 0;
    char *env_pool_size_string = getenv("TINY_SCHEME_GC_POOL_SIZE");
    if (NULL != env_pool_size_string)
    {
        if (1 != sscanf(env_pool_size_string, "%zu", &pool_size))
        {
            pool_size = 0;
        }
    }

    bool use_parallel_marking = NULL == getenv("TINY_SCHEME_GC_NO_PARALLEL_MARKING");

    GC *gc = gc_create(pool_size ? pool_size : GC_DEFAULT_POOL_SIZE, use_parallel_marking);

    if (NULL == gc)
    {
        fprintf(stderr, "Could not initialize GC!\n");
        return 2;
    }

    gc_start(gc);

    gc_vm_register_thread(gc);
    gc_vm_thread_awoke(gc);

    gc_track_reference(gc, &scheme_object, REFERENCE_TRACKING_MODE_HANDLE);
    gc_register_object_finalizer(gc, finalize_cell);
    gc_register_no_memory_callback(gc, no_memory_callback);
    gc_register_collection_callback(gc, collection_callback);
    gc_set_callback_data(gc, &scheme_object);
    gc_set_notification_signal(gc);

    if (!scheme_init_new(gc, NULL, NULL, &scheme_object))
    {
        fprintf(stderr, "Could not initialize scheme!\n");
        gc_unset_notification_signal(gc);
        gc_free(gc);
        return 3;
    }

    sc = *((scheme **) object_get_byte(scheme_object, 0));

    sc->is_working = true;
    sc->thread = pthread_self();

    scheme_set_input_port_file(scheme_object, stdin);
    scheme_set_output_port_file(scheme_object, stdout);

#if USE_DL
    pointer new_symbol = mk_symbol(scheme_object, "load-extension");
    gc_track_reference(gc, &new_symbol, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    pointer new_foreign_func = mk_foreign_func(gc, scm_load_ext);
    gc_track_reference(gc, &new_foreign_func, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);

    scheme_define(scheme_object,
                  object_get_slot(gc, scheme_object, SCHEME_GLOBAL_ENV),
                  new_symbol,
                  new_foreign_func);

    gc_untrack_reference(gc, &new_foreign_func);
    gc_untrack_reference(gc, &new_symbol);
#endif

    argv++;
    if (access(file_name, 0) != 0)
    {
        char *p = getenv("TINYSCHEMEINIT");

        if (p != 0)
        {
            file_name = p;
        }
    }

    check_return(setvbuf(stdin, NULL, _IONBF, 0), "setvbuf() failed.");

    do
    {
        if (strcmp(file_name, "-") == 0)
        {
            fin = stdin;
        }
        else if (strcmp(file_name, "-1") == 0 || strcmp(file_name, "-c") == 0)
        {
            pointer args = object_get_slot(gc, scheme_object, SCHEME_NIL);
            gc_track_reference(gc, &args, REFERENCE_TRACKING_MODE_HANDLE);

            isfile = file_name[1] == '1';
            file_name = *argv++;
            if (strcmp(file_name, "-") == 0)
            {
                fin = stdin;
            }
            else if (isfile)
            {
                fin = fopen(file_name, "r");
            }
            for (; *argv; argv++)
            {
                pointer value = mk_string(scheme_object, *argv);
                args = cons(scheme_object, value, args);
            }
            args = reverse_in_place(scheme_object, object_get_slot(gc, scheme_object, SCHEME_NIL), args);
            pointer new_symbol = mk_symbol(scheme_object, "*args*");
            scheme_define(scheme_object, object_get_slot(gc, scheme_object, SCHEME_GLOBAL_ENV), new_symbol, args);
            gc_untrack_reference(gc, &args);
        }
        else
        {
            fin = fopen(file_name, "r");
        }
        if (isfile && fin == 0)
        {
            fprintf(stderr, "Could not open file %s\n", file_name);
        }
        else
        {
            if (isfile)
            {
                scheme_load_named_file(scheme_object, fin, file_name);
            }
            else
            {
                scheme_load_string(scheme_object, file_name);
            }
            if (!isfile || fin != stdin)
            {
                if (sc->retcode != 0)
                {
                    fprintf(stderr, "Errors encountered reading %s\n", file_name);
                }
                if (isfile)
                {
                    fclose(fin);
                }
            }
        }
        file_name = *argv++;
    } while (file_name != 0);
    if (argc == 1)
    {
        scheme_load_named_file(scheme_object, stdin, 0);
    }
    retcode = sc->retcode;

    // Join unfinished threads.
    while (true)
    {
        check_return(tracing_mutex_lock(&sc->global_lock, ""), "pthread_mutex_lock() failed.");
        scheme *other_thread = NULL;
        pthread_t thread_to_join;
        if (0 != dynamic_array_count(sc->vm_threads))
        {
            other_thread = *DYNAMIC_ARRAY_GET((scheme **), sc->vm_threads, 0);
            dynamic_array_delete_at(sc->vm_threads, 0);
            other_thread->was_joined = true;
            thread_to_join = other_thread->thread;
        }
        check_return(tracing_mutex_unlock(&sc->global_lock, ""), "pthread_mutex_unlock() failed.");

        if (NULL != other_thread)
        {
            gc_vm_thread_fall_asleep(gc);
            check_return(pthread_join(thread_to_join, NULL), "pthread_join() failed.");
            gc_vm_thread_awoke(gc);
        }
        else
        {
            break;
        }
    }

    gc_register_no_memory_callback(gc, NULL);
    gc_register_collection_callback(gc, NULL);
    object_fill_slots(gc, scheme_object, NULL);
    gc_set_parallel_marking(gc, false);
    while (gc_object_get_size_in_bytes(gc, scheme_object) != gc_get_used_size(gc))
    {
        gc_wait_for_collect_completed(gc, false);
    }
    sc->was_joined = true;
    sc->is_working = false;
    gc_untrack_reference(gc, &scheme_object); // For this to be collected last.
    while (0 != gc_get_used_size(gc))
    {
        gc_wait_for_collect_completed(gc, false);
    }

    gc_vm_thread_fall_asleep(gc);
    gc_vm_unregister_thread(gc);

    gc_unset_notification_signal(gc);
    gc_stop(gc);
    gc_free(gc);

    #if defined(SYNCHRONIZED_TRACING)
    check_return(pthread_mutex_destroy(&tracing_lock), "pthread_mutex_destroy() failed.");
    #endif // defined(SYNCHRONIZED_TRACING).

    return retcode;
    error:
    abort();
    return -1;
}
