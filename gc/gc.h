// gc.h - GC interface.

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdatomic.h>
#include <stddef.h>

#if defined(GC_EXPORT_INTERNALS)
#define OBJECT_SLOT_SIZE (sizeof(void *))
#define OBJECT_SIZE_UNIT 4u
#endif // defined(GC_EXPORT_INTERNALS).

#define OBJECT_TOTAL_SIZE_BIT_COUNT 10u
#define OBJECT_MAX_SIZE (((1u << OBJECT_TOTAL_SIZE_BIT_COUNT) - 1) * OBJECT_SIZE_UNIT)

#define OBJECT_SLOT_COUNT_BIT_COUNT 10u
#define OBJECT_MAX_SLOT_COUNT ((1u << OBJECT_SLOT_COUNT_BIT_COUNT) - 1)

#define GC_DEFAULT_POOL_SIZE 128u // Number of pages.

#pragma pack(push, 0)

typedef struct ObjectHeader
{
    union
    {
        atomic_char32_t all_fields;
        struct
        {
            unsigned is_root    : 1;
            unsigned is_scanned : 1;
            unsigned is_garbage : 1;
            unsigned is_tracked : 1;
            unsigned flags      : 8; // Object-specific flags. Ignored by GC.
            unsigned total_size : OBJECT_TOTAL_SIZE_BIT_COUNT; // In OBJECT_SIZE_UNIT-byte blocks, including header.
            unsigned slot_count : OBJECT_SLOT_COUNT_BIT_COUNT;
        } fields;
    };
    // 32 bits in total.

    #if defined(__x86_64__)
    uint32_t __pad__; // Padded to OBJECT_SLOT_SIZE.
    #endif // defined(__x86_64__).
} ObjectHeader;

#pragma pack(pop)

typedef struct GCStatistics
{
    // All size values are in bytes.
    size_t pool_size;

    // Counters for current collection.
    size_t alive_objects;
    size_t used_size;

    size_t garbage_objects;
    size_t garbage_size;

    size_t sigsegv_count;

    // Info from last collection.
    size_t last_collection_alive_objects;
    size_t last_collection_used_size;
    double last_collection_used_ratio;

    size_t last_collection_garbage_objects;
    size_t last_collection_garbage_size;
    double last_collection_garbage_ratio;

    size_t last_collection_sigsegv_count;

    size_t last_collected_garbage_objects;
    size_t last_collected_size;
    double last_collected_ratio;
    size_t last_collection_move_count;

    size_t last_collection_time; // In microseconds.

    size_t total_sigsegv_count;

    size_t peak_usage_size;
    double peak_usage_ratio;

    size_t current_usage_size;
    double current_usage_ratio;
} GCStatistics;

typedef struct GC GC;

// Initializes new GC object.
GC *gc_create(size_t pool_page_count, bool use_parallel_marking);
// Frees given GC object.
void gc_free(GC *gc);

// Starts GC thread.
bool gc_start(GC *gc);
// Asks GC thread to stop and waits till it's stopped.
void gc_stop(GC *gc);

// Sets SIGSEGV to be handled by GC.
bool gc_set_notification_signal(GC *gc);
// Restores original SIGSEGV handler.
void gc_unset_notification_signal(GC *gc);

typedef void (*gc_object_finalizer)(GC *gc, void *callback_data, ObjectHeader *object);

// Registers new_object_finalizer callback to be called after object is freed by GC to allow
// proper finalization. Retuns previous finalizer.
gc_object_finalizer gc_register_object_finalizer(GC *gc, gc_object_finalizer new_object_finalizer);

typedef bool (*gc_no_memory_callback)(GC *gc, void *callback_data);

// Registers callback to be called when there's no enough memory.
// Returns previous callback.
// If callback returns true - GC will retry to do collection again.
gc_no_memory_callback gc_register_no_memory_callback(GC *gc, gc_no_memory_callback new_no_memory_callback);

typedef void (*gc_collection_callback)(GC *gc, void *callback_data, bool called_before_collection);

// Registers callback to be called before and after collection (parameter called_before_collection indicates it).
// Returns previous callback.
gc_collection_callback gc_register_collection_callback(GC *gc, gc_collection_callback new_collection_callback);

// Sets data to pass to all callbacks. Returns old data.
void *gc_set_callback_data(GC *gc, void *new_callback_data);

// Allocates new object with given number of slots and raw bytes.
ObjectHeader *gc_alloc(GC *gc, bool is_root, unsigned flags, size_t slot_count, size_t byte_count);

// Sleeps calling thread till collection is completed.
// If check_vm_working is true then falls asleep only if GC has requested VM to go to sleep.
// Otherwise, goes to sleep unconditionally (i.e. when there's no memory).
void gc_wait_for_collect_completed(GC *gc, bool check_vm_working);

// This enum specifies possible modes for tracking of external references.
// Used by gc_track_reference().
typedef enum ReferenceTrackingMode
{
    // During its lifetime reference can point only to single object.
    // Object isn't marked as externally referenced.
    // When its collected - external reference is set to NULL.
    REFERENCE_TRACKING_MODE_WEAK          = 0,

    // During its lifetime reference can point only to single object. This type of reference can't point to NULL.
    // Object is marked as externally referenced.
    // So, it won't be collected till reference exists.
    REFERENCE_TRACKING_MODE_SINGLE_OBJECT = 1,

    // During its lifetime reference can point to different objects. This type of reference can point to NULL.
    // Object pointed by this type of reference won't be collected.
    REFERENCE_TRACKING_MODE_HANDLE        = 2
} ReferenceTrackingMode;

// Asks GC to update external reference to the object that reference is currently pointing to
// (or will be pointing to, in case of REFERENCE_TRACKING_MODE_HANDLE).
// Update means that reference is still valid after object is being moved or set to NULL when
// object is freed (for REFERENCE_TRACKING_MODE_WEAK).
bool gc_track_reference(GC *gc, ObjectHeader **reference, ReferenceTrackingMode tracking_mode);

// Stops tracking given reference.
// If reference has mode REFERENCE_TRACKING_MODE_SINGLE_OBJECT and it's last reference to the object -
// resets is_tracked bit of pointed object to 0 making object collectable again.
void gc_untrack_reference(GC *gc, ObjectHeader **reference);

// Obtains content of given slot of the object.
#if defined(NO_DEBUG)
ObjectHeader *object_get_slot(ObjectHeader *o, size_t slot_index);
#define object_get_slot(gc, o, slot_index) object_get_slot(o, slot_index)
#else
ObjectHeader *object_get_slot(GC *gc, ObjectHeader *o, size_t slot_index);
#endif // !defined(NO_DEBUG).
// Sets given slot of the object to new value.
ObjectHeader *object_set_slot(GC *gc, ObjectHeader *o, size_t slot_index, ObjectHeader *value);

// Gets pointer to given byte of raw data of the object.
void *object_get_byte(ObjectHeader *o, size_t byte_index);
// Sets all slots of the object to the given value.
ObjectHeader *object_fill_slots(GC *gc, ObjectHeader *o, ObjectHeader *value);

// Helper function to get size of object in bytes.
#if defined(NO_DEBUG)
size_t gc_object_get_size_in_bytes(ObjectHeader *object);
#define gc_object_get_size_in_bytes(gc, object) gc_object_get_size_in_bytes(object)
#else
size_t gc_object_get_size_in_bytes(GC *gc, ObjectHeader *object);
#endif // defined(NO_DEBUG).

typedef enum ObjectHeaderField
{
    OBJECT_HEADER_FIELD_IS_ROOT    = 0,
    OBJECT_HEADER_FIELD_IS_SCANNED = 1,
    OBJECT_HEADER_FIELD_IS_GARBAGE = 2,
    OBJECT_HEADER_FIELD_IS_TRACKED = 3,
    OBJECT_HEADER_FIELD_FLAGS      = 4,
    OBJECT_HEADER_FIELD_TOTAL_SIZE = 5,
    OBJECT_HEADER_FIELD_SLOT_COUNT = 6
} ObjectHeaderField;

// Helper function which atomically gets given header field of given object.
unsigned gc_object_get_header_field(ObjectHeader *object, int field);

// Updates header of given object.
// Takes necessary locks and temporarily unprotects the object in case of parallel execution with VM.
// Possible header fields to change are defined in enum ObjectHeaderFlag. Next function parameter after
// field to change is an unsigned with new value of the flag. Several pairs of flag and new value may be specified.
// Example: gc_update_object_header(gc, object, OBJECT_HEADER_FIELD_IS_SCANNED, (unsigned) true);
// Example: gc_update_object_header(gc, object, OBJECT_HEADER_FIELD_IS_SCANNED, (unsigned) true, OBJECT_HEADER_FIELD_IS_GARBAGE, (unsigned) false);
void gc_update_object_header(GC *gc, ObjectHeader *object, size_t number_of_flags,  ...);

// Locks object's memory and allows direct modifications of its raw data.
void gc_lock_object(GC *gc, ObjectHeader *object);

// Unlocks object's memory.
void gc_unlock_object(GC *gc, ObjectHeader *object);

// Returns pointer to start of the pool of objects.
void *gc_get_pool(GC *gc);
// Returns size of pool in bytes.
size_t gc_get_pool_size(GC *gc);
// Returns used size of pool in bytes.
size_t gc_get_used_size(GC *gc);

// Function for VM. Notifies GC that new VM thread was born.
void gc_vm_register_thread(GC *gc);
// Function for VM. Notifies GC that VM thread died.
void gc_vm_unregister_thread(GC *gc);
// Function for VM. Notifies GC that existing VM thread awoke.
void gc_vm_thread_awoke(GC *gc);
// Function for VM. Notifies GC that existing VM thread went to sleep.
void gc_vm_thread_fall_asleep(GC *gc);

// Is collection requested by GC.
bool gc_is_collection_needed(GC *gc);

/* Typical pattern of usage of gc_vm_* thread-related functions:
 * gc_vm_register_thread();
 * gc_vm_thread_awoke();
 * ...
 * gc_vm_thread_fall_asleep();
 * ...
 * gc_vm_thread_awoke();
 * ...
 * gc_vm_thread_fall_asleep();
 * gc_vm_unregister_thread();
 */

#if defined(GC_EXPORT_INTERNALS)
// Write barrier called by object_set_slot(). Does actual work.
void gc_notify_object_modified(GC *gc, ObjectHeader *object, ObjectHeader **slot, ObjectHeader *new_value);
#endif // defined(GC_EXPORT_INTERNALS).

// Returns pointer to GCStatistics struct for given GC.
GCStatistics *gc_get_statistics(GC *gc);

// Enables or disables parallel marking. Returns previous value.
bool gc_set_parallel_marking(GC *gc, bool enable_parallel_marking);

#if defined(GC_EXPORT_INTERNALS)
#if defined(NO_DEBUG)
#define object_get_slot_impl(gc, o, slot_index) \
({ \
    ObjectHeader **__slot      = ((void *) (o)) + (((size_t) (slot_index)) + 1) * OBJECT_SLOT_SIZE, \
                 *__slot_value = *__slot; \
    __slot_value; \
})
#else
#define object_get_slot_impl(gc, o, slot_index) \
({ \
    assert((o) && "Bad object pointer."); \
    assert(((size_t) (slot_index)) < gc_object_get_header_field((ObjectHeader *) (o), OBJECT_HEADER_FIELD_SLOT_COUNT) && "Bad slot index."); \
\
    ObjectHeader **__slot      = ((void *) (o)) + (((size_t) (slot_index)) + 1) * OBJECT_SLOT_SIZE, \
                 *__slot_value = *__slot; \
\
    ObjectHeader *__heap_used_part_boudary = gc_get_pool(gc) + gc_get_used_size(gc); \
\
    assert((NULL == __slot_value || \
            (((void *) __slot_value >= gc_get_pool(gc)) && (__slot_value < __heap_used_part_boudary))) && \
           "Bad slot value."); \
    assert(((ObjectHeader *) (o)) < __heap_used_part_boudary && "Object is in freed part of the heap."); \
    assert(0 == ((size_t) __slot_value) % OBJECT_SLOT_SIZE && "Bad slot value."); \
\
    __slot_value; \
})
#endif // defined(NO_DEBUG).
#endif // defined(GC_EXPORT_INTERNALS).
