// gc.c - GC implementation.

#include <features.h>
#include <assert.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <limits.h>
#include <stdarg.h>
#include <time.h>
#include <setjmp.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <pthread.h>
#include <sched.h>

#include "debug.h"
#include "dynamic_array.h"

#define GC_EXPORT_INTERNALS
#include "gc.h"

// External reference to the object on the managed heap.
typedef struct ExternalReference
{
    ReferenceTrackingMode tracking_mode;
    ObjectHeader **reference;
} ExternalReference;

// Holds shift of references for a given block of reachable objects.
typedef struct ReferenceUpdateRecord
{
    size_t shift;
    // Block start isn't needed since these records are sorted in the list, which
    // also includes info about not moved blocks.
    void *block_end;
} ReferenceUpdateRecord;

typedef struct GC
{
    void *pool;             // Pointer to a pool.
    atomic_uintptr_t free;  // Pointer to a free part of a pool.
    size_t pool_size;       // In bytes.
    size_t pool_page_count;
    size_t page_size;       // In bytes.

    // 1 mutex per page.
    // These locks are used to synchronize access to protected part of the heap in case of modification.
    // They are needed because when object becomes temporarily unprotected (for the update by another VM thread or
    // GC marking thread) the update of scanned object by VM thread will be undetected.
    pthread_mutex_t *page_locks;
    void *protected_zone_end; // Points to first unprotected page of heap.
    // Used to synchronize accesses to protected_zone_end with real calls to mprotect().
    pthread_mutex_t protected_zone_end_lock;

    void *page_protection_flags; // Sequence of bits corresponding to protection of pool pages.

    struct sigaction old_sigsegv_action;
    // Returns to mutator after SIGSEGV. Used to detect writes to protected objects.
    sigjmp_buf sigsegv_return;

    // Finalizer to be called by GC when object is freed.
    gc_object_finalizer finalizer;

    // Callback to be called by GC when there's no memory.
    gc_no_memory_callback no_memory_callback;

    // Callback to be called by GC before and after collection.
    gc_collection_callback collection_callback;

    // Custom data passed to different callbacks.
    void *callback_data;

    // Determines whether object is scanned or not by comparison with corresponding bit in the object's header.
    // At the end of collection, instead of flipping each object's scanned_bit only this flag is flipped.
    // During mark, (gc->scanned_bit == object->scanned_bit) means that object is unscanned.
    // After mark gc->scanned_bit is flipped, so during gc_collect()
    // (gc->scanned_bit == object->scanned_bit) means that object is scanned.
    bool scanned_bit;

    // Determines whether object is garbage or not by comparison with corresponding bit in the object's header.
    // After collection this bit is flipped.
    // During marking and compaction, (gc->garbage_bit == object->is_garbage) means that object is garbage.
    bool garbage_bit;

    pthread_mutex_t tracked_references_lock; // Used to synchronize access to tracked_references.
    // List of external references for GC to update.
    // Used by gc_track_reference(), gc_untrack_reference(), gc_update_freed_tracked_references(),
    // gc_update_references(), gc_mark_objects_as_reachable().
    DynamicArray *tracked_references;

    atomic_bool thread_working; // Used to stop GC thread.
    pthread_t thread; // GC worker thread handle.

    // Points to the first object which wasn't scanned during parallel marking.
    // This is a border between scanned and unscanned part of the heap after parallel marking.
    void *parallel_mark_boundary;

    atomic_bool vm_working;  // These two are used to start/stop VM threads, which check these
    atomic_bool gc_complete; // flags in gc_wait_for_collect_completed() and gc_vm_thread_awoke().
    atomic_bool gc_collecting; // Whether GC is collecting now. VM should sleep during this time.

    atomic_uint vm_thread_count; // Number of registered VM threads.
    atomic_uint vm_running_thread_count; // Number of actually running VM threads.
    atomic_uint vm_waiting_thread_count; // Number of VM threads which wait for GC to do collection.

    // Mutex used to synchronize events.
    pthread_mutex_t vm_mutex;

    // This event is used in gc_wait_for_collect_completed() to synchronize VM and GC.
    pthread_cond_t collect_completed_event;

    // This event is used to for GC to wait VM goes to sleep.
    pthread_cond_t vm_is_sleeping_event;

    // This event is used to for GC to wait VM awakes.
    pthread_cond_t vm_is_running_event;

    // Used in gc_alloc() to syncronize access to free pointer and "hot" part of the heap.
    pthread_mutex_t alloc_mutex;
    // Used in gc_notify_object_modified() to synchronize access to sigsegv_return sigjmp_buf.
    pthread_mutex_t mutation_mutex;

    GCStatistics statistics; // Stores different statistics.

    // Indicates that part of marking is done in parallel with VM.
    // Otherwise, all marking is done when VM sleeps (waits for collection to complete).
    bool is_parallel_marking_enabled;

    // List of shifts for moved blocks.
    DynamicArray *reference_update_records;

    size_t mark_delay; // Duration of short pauses done during parallel marking. In ms.

    /* TODO: GC improvements.
     *  - void log_callback(GC *gc, void *data); + default callback.
     *  - bool oom_callback(GC *gc, void *data); + default callback.
     *  - move callback.
     *  - move callback for subscribed objects.
     *  - finalize callback with cancellation.
     *  - free callback with cancellation.
     */
} GC;

#define min(a, b) \
({ \
    __auto_type __a = (a); \
    __auto_type __b = (b); \
    __a < __b ? __a : __b; \
})

#define max(a, b)({ \
    __auto_type __a = (a); \
    __auto_type __b = (b); \
    __a > __b ? __a : __b; \
})

// These functions may be executed in parallel with VM.

// GC worker thread.
void *gc_thread(void *data);

// Does marking by walking through object graph and calling gc_scan_object() on non-garbage or root objects.
// In case of parallel marking protects pages with scanned objects.
void gc_mark(GC *gc, bool is_concurrent_mark);

// Does actual scanning of single object. Marks referenced objects as non-garbage.
ObjectHeader *gc_scan_object(GC *gc, ObjectHeader *object);

// Helper macro which atomically gets given header field of given object.
#define gc_object_get_header_field_impl(object, field) \
({ \
    ObjectHeader local_object = { .all_fields = atomic_load(&(((ObjectHeader *) object)->all_fields)) }; \
    unsigned field_value = 0; \
\
    switch (field) \
    { \
        case OBJECT_HEADER_FIELD_IS_ROOT: \
            field_value = local_object.fields.is_root; \
            break; \
\
        case OBJECT_HEADER_FIELD_IS_SCANNED: \
            field_value = local_object.fields.is_scanned; \
            break; \
\
        case OBJECT_HEADER_FIELD_IS_GARBAGE: \
            field_value = local_object.fields.is_garbage; \
            break; \
\
        case OBJECT_HEADER_FIELD_IS_TRACKED: \
            field_value = local_object.fields.is_tracked; \
            break; \
\
        case OBJECT_HEADER_FIELD_FLAGS: \
            field_value = local_object.fields.flags; \
            break; \
\
        case OBJECT_HEADER_FIELD_TOTAL_SIZE: \
            field_value = local_object.fields.total_size; \
            break; \
\
        case OBJECT_HEADER_FIELD_SLOT_COUNT: \
            field_value = local_object.fields.slot_count; \
            break; \
\
        default: \
            assert(false && "Bad flag value."); \
            /* Fall through. */ \
    } \
\
    field_value; \
})

// Helper macro to get size of object in OBJECT_SIZE_UNITs.
#define gc_object_get_size(gc, object) \
({ \
    size_t __object_total_size = gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_TOTAL_SIZE); \
    if (0 == __object_total_size) \
    { \
        assert(gc->garbage_bit == gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_GARBAGE) && "Not a fill object."); \
        __object_total_size = *((size_t *) (((ObjectHeader *) (object)) + 1)); \
    } \
    else \
    { \
        assert(0 != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_SLOT_COUNT) && "Usual object must have at least 1 slot."); \
    } \
    __object_total_size; \
})

// Helper macro to get size of object in OBJECT_SIZE_UNITs.
// Singlethreaded version.
#define gc_object_get_size_exclusive(gc, object) \
({ \
    size_t __object_total_size = ((ObjectHeader *) object)->fields.total_size; \
    if (0 == __object_total_size) \
    { \
        assert(gc->garbage_bit == gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_GARBAGE) && "Not a fill object."); \
        __object_total_size = *((size_t *) (((ObjectHeader *) (object)) + 1)); \
    } \
    else \
    { \
        assert(0 != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_SLOT_COUNT) && "Usual object must have at least 1 slot."); \
    } \
    __object_total_size; \
})

// Helper macro to set object slot exclusively.
#if !defined(NO_DEBUG)
#define object_set_slot_exclusive(gc, o, slot_index, value) \
({ \
    ObjectHeader *__o     = o, \
                 *__value = value; \
    size_t __slot_index = slot_index; \
    assert(__o && "Bad object pointer."); \
    assert(__slot_index < gc_object_get_header_field(__o, OBJECT_HEADER_FIELD_SLOT_COUNT) && "Bad slot index."); \
    ObjectHeader *__heap_used_part_boudary = gc_get_pool(gc) + gc_get_used_size(gc); \
    assert((NULL == __value || \
            (((void *) __value >= gc_get_pool(gc)) && (__value < __heap_used_part_boudary))) && \
           "Bad slot value."); \
    assert(0 == ((size_t) __value) % OBJECT_SLOT_SIZE && "Bad slot value."); \
    assert(__o < __heap_used_part_boudary && "Object is in freed part of the heap."); \
    assert(__value < __heap_used_part_boudary && "Object is in freed part of the heap."); \
    ObjectHeader **__slot = ((void *) __o) + OBJECT_SLOT_SIZE + __slot_index * OBJECT_SLOT_SIZE; \
    *__slot = __value; \
})
#else
#define object_set_slot_exclusive(gc, o, slot_index, value) \
({ \
    ObjectHeader **__slot = ((void *) (o)) + OBJECT_SLOT_SIZE + (size_t) (slot_index) * OBJECT_SLOT_SIZE; \
    *__slot = (ObjectHeader *) (value); \
})
#endif // !defined(NO_DEBUG).

// Helper function to set size of object in OBJECT_SIZE_UNITs.
size_t gc_object_set_size(GC *gc, ObjectHeader *object, size_t new_size);

// Moves forward protected_zone_end to include highest_scanned_object to protected area and protects heap.
void gc_protect_scanned_heap_pages(GC *gc, ObjectHeader *highest_scanned_object);

// Protects pages in range pool ... protected_zone_end.
void gc_protect_heap(GC *gc, void *new_protected_zone_end);

// Makes whole heap writeable and moves protected_zone_end to pool, thus indicating that nothing is protected.
void gc_unprotect_heap(GC *gc);

// Handles SIGSEGV caused by VM threads writing to protected objects.
// Using sigsegv_return, returns to gc_notify_object_modified(), which marks object as unscanned.
void gc_object_modification_signal_handler(int signal, siginfo_t *signal_info, void *unused);

// Marks all objects on a given page as reachable and unscanned.
void gc_mark_page_objects_as_unscanned(GC *gc, void *page);

// Locks page lock corresponding to page which contains given address. Returns address of the page.
void *gc_lock_heap_page(GC *gc, void *address);

// Unlocks page lock corresponding to page which contains given address
void gc_unlock_heap_page(GC *gc, void *address);

typedef enum RangeLockMode
{
    RANGE_LOCK_MODE_LOCK_ONLY        = 1, // Only lock/unlock corresponding mutex from page_locks.
    RANGE_LOCK_MODE_PROTECT_ONLY     = 2, // Only mprotect() using PROT_READ / (PROT_READ | PROT_WRITE).
    RANGE_LOCK_MODE_LOCK_AND_PROTECT = 3  // Do both operations.
} RangeLockMode;

// These 2 functions lock/unlock heap pages which contain range defined by address and length.
// Actual operations to be performed are determined by lock_mode.
// mprotect() calls aren't done for pages beyond protected_zone_end.
void gc_lock_heap_range(GC *gc, void *address, size_t length, RangeLockMode lock_mode, void *protected_zone_end);
void gc_unlock_heap_range(GC *gc, void *address, size_t length, RangeLockMode lock_mode, void *protected_zone_end);

// Helper function to determine whether all VM threads are sleeping.
bool gc_vm_is_sleeping(GC *gc);

// Helper function to determine whether VM waits for GC to collect.
bool gc_vm_is_waiting(GC *gc);

// Helper function to determine whether VM is still active.
bool gc_vm_is_active(GC *gc);

// These functions are executed only during VM stop.

// Ensures that 3-color invariant is valid.
// This function checks that scanned non-garbage objects do not point to garbage objects.
// If object with such reference is detected - it's marked as unscanned.
void gc_fix_object_graph(GC *gc);

// Marks objects referenced by external handles (REFERENCE_TRACKING_MODE_HANDLE) as reachable.
// Also marks garbage object referenced with REFERENCE_TRACKING_MODE_SINGLE_OBJECT - they were tracked
// after being marked.
void gc_mark_objects_as_reachable(GC *gc);

// Sets external references which point to garbage objects to NULL.
void gc_update_freed_tracked_references(GC *gc);

// Does actual collection.
size_t gc_collect(GC *gc);

// Finalizes garbage objects.
void gc_finalize_garbage(GC *gc);

// Turns G objects to F objects and glues together consecutive F objects.
void gc_defragment_garbage(GC *gc);

// Moves R objects to the beginning of the heap and updates free pointer.
size_t gc_compact(GC *gc);

// Returns pointer to first non-R object after reachable_block_start.
// Objects in range reachable_block_start ... <return_value> are reachable.
ObjectHeader *gc_find_reachable_block_end(GC *gc, ObjectHeader *reachable_block_start);

// If fill_object is followed by another F object, glues them together, thus keeping F objects defragmented.
bool gc_check_for_consecutive_free_blocks(GC *gc, ObjectHeader *fill_object);

// Cuts off leading part of fill_object of requested_size and produces new F object at the end.
// Returns size of new F object in the gap pointer.
ObjectHeader *gc_split(GC *gc, ObjectHeader *fill_object, size_t requested_size, size_t *gap);

// Moves (R) objects in range (object_block_start ... object_block_end)
// to fill_object (F). Does necessary split of fill_object.
void gc_move(GC *gc, ObjectHeader *object_block_start, ObjectHeader *object_block_end, ObjectHeader *fill_object);

// Marks given object as garbage (F) and glues it with following F object.
void gc_free_object(GC *gc, ObjectHeader *object, size_t new_size);

// Updates heap and external references to the objects, which were moved.
// Information about moved blocks and their shifts is stored in gc->reference_update_records.
// References, which fall into some range (object_block_start ... object_block_end) are moved
// by corresponding shift to the beginning of the heap.
void gc_update_references(GC *gc);

#if !defined(NO_DEBUG)
// Performs internal checks of GC state.
void gc_assert_heap(GC *gc, bool asserting_after_mark, bool asserting_after_collect);
#else
#define gc_assert_heap(gc, asserting_after_mark, asserting_after_collect)
#endif // !defined(NO_DEBUG).

// Gets protection bit for a given page.
bool gc_get_page_protection_bit(GC *gc, void *page);

// Sets protection bit for a given page.
void gc_set_page_protection_bit(GC *gc, void *page, bool new_value);

// Clears all page protection bits.
void gc_clear_memory_protection_bits(GC *gc);

// Initializes new GC object.
GC *gc_create(size_t pool_page_count, bool use_parallel_marking)
{
    GC *gc = NULL;
    check(pool_page_count, "Bad pool page count.");
    gc = calloc(1, sizeof(GC));
    check_mem(gc);

    gc->scanned_bit = false;
    gc->garbage_bit = false;
    gc->page_size = sysconf(_SC_PAGE_SIZE);

    gc->pool = aligned_alloc(gc->page_size, pool_page_count * gc->page_size);
    check_mem(gc->pool);
    memset(gc->pool, 0, pool_page_count * gc->page_size);
    gc->page_protection_flags = (void *) calloc(pool_page_count + CHAR_BIT - pool_page_count % CHAR_BIT, sizeof(char));
    check_mem(gc->page_protection_flags);

    gc->tracked_references = dynamic_array_create(sizeof(ExternalReference), 0);
    check_mem(gc->tracked_references);

    gc->free = (atomic_uintptr_t) gc->pool;
    gc->pool_size = pool_page_count * gc->page_size;
    gc->pool_page_count = pool_page_count;

    gc->thread_working = false;
    gc->thread = 0;

    gc->parallel_mark_boundary = NULL;

    gc->vm_working = true;
    gc->vm_thread_count = 0;
    gc->vm_running_thread_count = 0;
    gc->vm_waiting_thread_count = 0;

    pthread_mutexattr_t mutex_recursive_attributes;
    memset(&mutex_recursive_attributes, 0, sizeof(mutex_recursive_attributes));
    check_return(pthread_mutexattr_init(&mutex_recursive_attributes), "pthread_mutexattr_init() failed.");
    check_return(pthread_mutexattr_settype(&mutex_recursive_attributes, PTHREAD_MUTEX_RECURSIVE), "pthread_mutexatttr_settype() failed.");

    check_return(pthread_mutex_init(&gc->vm_mutex, &mutex_recursive_attributes), "pthread_mutex_init() failed.");

    check_return(pthread_cond_init(&gc->collect_completed_event, NULL), "pthread_cond_init() failed.");
    check_return(pthread_cond_init(&gc->vm_is_sleeping_event, NULL), "pthread_cond_init() failed.");
    check_return(pthread_cond_init(&gc->vm_is_running_event, NULL), "pthread_cond_init() failed.");

    check_return(pthread_mutex_init(&gc->alloc_mutex, NULL), "pthread_mutex_init() failed.");

    check_return(pthread_mutex_init(&gc->tracked_references_lock, NULL), "pthread_mutex_init() failed.");

    gc->page_locks = calloc(pool_page_count, sizeof(pthread_mutex_t));
    check_mem(gc->page_locks);

    check_return(pthread_mutex_init(&gc->mutation_mutex, &mutex_recursive_attributes), "pthread_mutex_init() failed.");

    for (size_t i = 0; i < pool_page_count; ++i)
    {
        check_return(pthread_mutex_init(&gc->page_locks[i], &mutex_recursive_attributes), "pthread_mutex_init() failed.");
    }

    gc->protected_zone_end = gc->pool;
    check_return(pthread_mutex_init(&gc->protected_zone_end_lock, &mutex_recursive_attributes), "pthread_mutex_init() failed.");

    check_return(pthread_mutexattr_destroy(&mutex_recursive_attributes), "pthread_mutexattr_destroy() failed.");

    gc->finalizer = NULL;
    gc->no_memory_callback = NULL;
    gc->collection_callback = NULL;
    gc->callback_data = NULL;

    gc->statistics.pool_size = gc->pool_size;

    gc->is_parallel_marking_enabled = use_parallel_marking;

    gc->reference_update_records = dynamic_array_create(sizeof(ReferenceUpdateRecord), 0);
    check_mem(gc->reference_update_records);

    gc->mark_delay = 15;

    return gc;
    error:
    if (gc)
    {
        if (gc->pool)
        {
            free(gc->pool);
        }
        if (gc->page_protection_flags)
        {
            free(gc->page_protection_flags);
        }
        if (gc->tracked_references)
        {
            dynamic_array_destroy(gc->tracked_references);
        }
        pthread_mutex_destroy(&gc->vm_mutex);
        pthread_cond_destroy(&gc->collect_completed_event);
        pthread_cond_destroy(&gc->vm_is_sleeping_event);
        pthread_cond_destroy(&gc->vm_is_running_event);
        pthread_mutex_destroy(&gc->alloc_mutex);
        pthread_mutex_destroy(&gc->mutation_mutex);
        pthread_mutex_destroy(&gc->tracked_references_lock);
        if (gc->page_locks)
        {
            for (size_t i = 0; i < pool_page_count; ++i)
            {
                pthread_mutex_destroy(&gc->page_locks[i]);
            }
            free(gc->page_locks);
        }
        pthread_mutexattr_destroy(&mutex_recursive_attributes);
        pthread_mutex_destroy(&gc->protected_zone_end_lock);
        if (gc->reference_update_records)
        {
            dynamic_array_destroy(gc->reference_update_records);
        }
        free(gc);
    }
    return NULL;
}

// Frees given GC object.
void gc_free(GC *gc)
{
    if (!gc)
    {
        return;
    }
    if (gc->pool)
    {
        free(gc->pool);
    }
    if (gc->page_protection_flags)
    {
        free(gc->page_protection_flags);
    }
    if (gc->tracked_references)
    {
        dynamic_array_destroy(gc->tracked_references);
    }
    pthread_mutex_destroy(&gc->vm_mutex);
    pthread_cond_destroy(&gc->collect_completed_event);
    pthread_cond_destroy(&gc->vm_is_sleeping_event);
    pthread_cond_destroy(&gc->vm_is_running_event);
    pthread_mutex_destroy(&gc->alloc_mutex);
    pthread_mutex_destroy(&gc->mutation_mutex);
    pthread_mutex_destroy(&gc->tracked_references_lock);
    if (gc->page_locks)
    {
        for (size_t i = 0; i < gc->pool_page_count; ++i)
        {
            pthread_mutex_destroy(&gc->page_locks[i]);
        }
        free(gc->page_locks);
    }
    pthread_mutex_destroy(&gc->protected_zone_end_lock);
    if (gc->reference_update_records)
    {
        dynamic_array_destroy(gc->reference_update_records);
    }
    free(gc);
}

// Starts GC thread.
bool gc_start(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    if (atomic_load(&gc->thread_working))
    {
        debug("GC thread is already started.");
        return false; // GC thread has already started.
    }

    gc->thread_working = true;
    check_return(pthread_create(&gc->thread, NULL, gc_thread, gc), "pthread_create() failed.");

    return true;
    error:
    gc->thread_working = false;
    return false;
}

// Asks GC thread to stop and waits till it's stopped.
void gc_stop(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    size_t tracked_reference_count = dynamic_array_count(gc->tracked_references);
    check(0 == tracked_reference_count,
          "Tracked references still present: %zu.",
          tracked_reference_count);

    size_t used_size = gc_get_used_size(gc);
    check(0 == used_size,
          "Used size not zero: %zu.",
          used_size);

    if (!atomic_load(&gc->thread_working) ||
        0 == gc->thread)
    {
        return; // GC thread is already stopped.
    }

    check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
    atomic_store(&gc->thread_working, false);
    check_return(pthread_cond_signal(&gc->vm_is_running_event), "pthread_cond_signal() failed.");
    debug("Sent vm_is_running signal (at full VM stop).");
    check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
    check_return(pthread_join(gc->thread, NULL), "pthread_join() failed.");
    gc->thread = 0;
    return;
    error:
    abort();
}

// Sets SIGSEGV to be handled by GC.
bool gc_set_notification_signal(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    struct sigaction new_sigsegv_action = {
        .sa_sigaction = gc_object_modification_signal_handler,
        .sa_flags     = SA_SIGINFO
    };
    check(0 == sigemptyset(&new_sigsegv_action.sa_mask), "sigemptyset() failed.");
    check(0 == sigaction(SIGSEGV, &new_sigsegv_action, &gc->old_sigsegv_action), "sigaction() failed.");

    return true;
    error:
    abort();
    return false;
}

// Restores original SIGSEGV handler.
void gc_unset_notification_signal(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    check(0 == sigaction(SIGSEGV, &gc->old_sigsegv_action, NULL), "sigaction() failed.");
    return;
    error:
    abort();
}

// Registers new_object_finalizer callback to be called after object is freed by GC to allow
// proper finalization. Retuns previous finalizer.
gc_object_finalizer gc_register_object_finalizer(GC *gc, gc_object_finalizer new_object_finalizer)
{
    assert(gc && "Bad GC pointer.");
    gc_object_finalizer old_finalizer = gc->finalizer;
    gc->finalizer = new_object_finalizer;
    return old_finalizer;
}

// Registers callback to be called when there's no enough memory.
// Returns previous callback.
// If callback returns true - GC will retry to do collection again.
gc_no_memory_callback gc_register_no_memory_callback(GC *gc, gc_no_memory_callback new_no_memory_callback)
{
    assert(gc && "Bad GC pointer.");
    gc_no_memory_callback old_no_memory_callback = gc->no_memory_callback;
    gc->no_memory_callback = new_no_memory_callback;
    return old_no_memory_callback;
}

// Registers callback to be called before and after collection (parameter called_before_collection indicates it).
// Returns previous callback.
gc_collection_callback gc_register_collection_callback(GC *gc, gc_collection_callback new_collection_callback)
{
    assert(gc && "Bad GC pointer.");
    gc_collection_callback old_collection_callback = gc->collection_callback;
    gc->collection_callback = new_collection_callback;
    return old_collection_callback;
}

// Sets data to pass to all callbacks. Returns old data.
void *gc_set_callback_data(GC *gc, void *new_callback_data)
{
    assert(gc && "Bad GC pointer.");
    void *old_callback_data = gc->callback_data;
    gc->callback_data = new_callback_data;
    return old_callback_data;
}

// GC worker thread.
// It's controlled by thread_working field of GC struct.
void *gc_thread(void *data)
{
    GC *gc = (GC *) data;

    while (atomic_load(&gc->thread_working))
    {
        check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
        atomic_store(&gc->gc_complete, false);
        check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
        check(0 == sched_yield(), "sched_yield() failed.");

        gc->statistics.peak_usage_size = max(gc->statistics.peak_usage_size,
                                             gc_get_used_size(gc));

        gc->statistics.last_collection_alive_objects = gc->statistics.alive_objects;
        gc->statistics.alive_objects = 0;
        gc->statistics.last_collection_used_size = gc->statistics.used_size;
        gc->statistics.used_size = 0;
        gc->statistics.last_collection_garbage_objects = gc->statistics.garbage_objects;
        gc->statistics.garbage_objects = 0;
        gc->statistics.last_collection_garbage_size = gc->statistics.garbage_size;
        gc->statistics.garbage_size = 0;
        gc->statistics.last_collection_sigsegv_count = gc->statistics.sigsegv_count;
        gc->statistics.sigsegv_count = 0;

        if (gc->is_parallel_marking_enabled)
        {
            // Do concurrent mark.
            debug("Mark started.");
            gc_mark(gc, true);
            debug("Mark completed.");

            #if !defined(NO_DEBUG)
            double usage_ratio = (double) gc_get_used_size(gc) / (double) gc->pool_size;
            debug("Usage ratio after marking: %lf", usage_ratio);
            #endif // !defined(NO_DEBUG).

            // Sleep VM.
            if (atomic_load(&gc->thread_working))
            {
                debug("VM pause started.");
                check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
                atomic_store(&gc->vm_working, false);
                debug("VM pause flag set.");

                while (atomic_load(&gc->thread_working) && gc_vm_is_active(gc) && !gc_vm_is_sleeping(gc))
                {
                    debug("Waiting for vm_is_sleeping signal.");
                    check_return(pthread_cond_wait(&gc->vm_is_sleeping_event, &gc->vm_mutex), "pthread_cond_wait() failed.");
                    debug("Got vm_is_sleeping signal.");
                }
                atomic_store(&gc->gc_collecting, true);
                check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
                debug("VM pause completed.");
            }
        }
        else // Just wait till VM wants GC to do collection.
        {
            gc->parallel_mark_boundary = gc->pool;

            if (atomic_load(&gc->thread_working))
            {
                debug("Waiting for VM to sleep started.");
                check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");

                while (atomic_load(&gc->thread_working) && gc_vm_is_active(gc) && !gc_vm_is_sleeping(gc))
                {
                    debug("Waiting for vm_is_sleeping signal.");
                    check_return(pthread_cond_wait(&gc->vm_is_sleeping_event, &gc->vm_mutex), "pthread_cond_wait() failed.");
                    debug("Got vm_is_sleeping signal.");
                }

                atomic_store(&gc->vm_working, false);
                debug("VM pause flag set.");
                atomic_store(&gc->gc_collecting, true);
                check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
                debug("Waiting for VM to sleep finished.");
            }
        }

        if (NULL != gc->collection_callback)
        {
            gc->collection_callback(gc, gc->callback_data, true);
        }

        struct timeval gc_start_time;
        check_return(gettimeofday(&gc_start_time, NULL), "gettimeofday() failed.");

        gc->statistics.peak_usage_size = max(gc->statistics.peak_usage_size,
                                             gc_get_used_size(gc));

        // Unprotect the heap.
        debug("parallel_mark_boundary: %p.",
              (void *) (gc->parallel_mark_boundary - gc->pool));
        gc_unprotect_heap(gc);

        #if !defined(NO_DEBUG)
        // Asserts that locks are free.
        for (size_t i = 0; i < gc->pool_page_count; ++i)
        {
            check_return(pthread_mutex_trylock(&gc->page_locks[i]), "pthread_mutex_trylock() failed.");
            check_return(pthread_mutex_unlock(&gc->page_locks[i]), "pthread_mutex_unlock() failed.");
        }
        #endif // !defined(NO_DEBUG).

        debug("Concurrently scanned part of heap: %2.4lf%%.",
              ((double) (gc->parallel_mark_boundary - gc->pool) / (double) (((void *) gc->free) - gc->pool)) * 100.0);

        // Do needed extra marking (hopefully small).
        gc->parallel_mark_boundary = (void *) atomic_load(&gc->free);

        // Try to collect at most 2 times.
        // Retry is performed when first collection didn't free any object which could mean that parallel marking
        // has overscaned the heap (too much objects are marked as R, but not all of them actually are).
        bool retry_available = true;
        while (true)
        {
            // Mark objects referenced by external handles (REFERENCE_TRACKING_MODE_HANDLE) as reachable.
            gc_mark_objects_as_reachable(gc);
            gc_fix_object_graph(gc);

            // Mark objects which were modified and those in unscanned part of the heap.
            gc_mark(gc, false);
            gc->scanned_bit ^= true;
            gc_assert_heap(gc, true, false);

            // Set references to freed objects to NULL.
            gc_update_freed_tracked_references(gc);

            // Do actual collection.
            size_t freed_size = gc_collect(gc);
            gc->garbage_bit ^= true;

            if (0 != freed_size || !retry_available || !gc_vm_is_waiting(gc))
            {
                break;
            }
            else if (retry_available)
            {
                gc->statistics.peak_usage_size = max(gc->statistics.peak_usage_size,
                                                     gc_get_used_size(gc));

                gc->statistics.last_collection_alive_objects = gc->statistics.alive_objects;
                gc->statistics.alive_objects = 0;
                gc->statistics.last_collection_used_size = gc->statistics.used_size;
                gc->statistics.used_size = 0;
                gc->statistics.last_collection_garbage_objects = gc->statistics.garbage_objects;
                gc->statistics.garbage_objects = 0;
                gc->statistics.last_collection_garbage_size = gc->statistics.garbage_size;
                gc->statistics.garbage_size = 0;

                retry_available = false;
                debug("Retrying collection in exclusive mode.");
            }
        }

        struct timeval gc_end_time;
        check_return(gettimeofday(&gc_end_time, NULL), "gettimeofday() failed.");
        gc->statistics.last_collection_time = (gc_end_time.tv_sec * 1000 * 1000 + gc_end_time.tv_usec) -
                                              (gc_start_time.tv_sec * 1000 * 1000 + gc_start_time.tv_usec);

        if (NULL != gc->collection_callback)
        {
            gc->collection_callback(gc, gc->callback_data, false);
        }

        // Awake VM.
        debug("VM awake started.");
        check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
        atomic_store(&gc->gc_collecting, false);
        atomic_store(&gc->vm_working, true);
        atomic_store(&gc->gc_complete, true);
        check_return(pthread_cond_broadcast(&gc->collect_completed_event), "pthread_cond_broadcast() failed.");
        debug("Broadcasted collect_completed_event.");
        check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
        check(0 == sched_yield(), "sched_yield() failed.");
        check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
        while (atomic_load(&gc->thread_working) && gc_vm_is_active(gc) && gc_vm_is_sleeping(gc))
        {
            debug("Waiting for vm_is_running signal.");
            check_return(pthread_cond_wait(&gc->vm_is_running_event, &gc->vm_mutex), "pthread_cond_wait() failed.");
            debug("Got vm_is_running signal.");

            if (gc_vm_is_waiting(gc))
            {
                break;
            }
        }
        check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
        debug("VM awake completed.");
    }

    return NULL;
    error:
    abort();
    return NULL;
}

// Allocates new object with given number of slots and raw bytes.
ObjectHeader *gc_alloc(GC *gc, bool is_root, unsigned flags, size_t slot_count, size_t byte_count)
{
    assert(gc && "Bad GC pointer.");

    if (0 == slot_count)
    {
        slot_count = 1;
    }

    size_t new_object_size = OBJECT_SLOT_SIZE +
                             slot_count * OBJECT_SLOT_SIZE +
                             byte_count +
                             (OBJECT_SLOT_SIZE - byte_count) % OBJECT_SLOT_SIZE; // Padded to OBJECT_SLOT_SIZE.

    assert((0 == new_object_size % OBJECT_SLOT_SIZE) && "Object should be aligned to OBJECT_SLOT_SIZE.");
    check(OBJECT_MAX_SIZE >= new_object_size, "New object is too big.");
    check(OBJECT_MAX_SLOT_COUNT >= slot_count, "New object has to much slots.");

    int trylock_result = tracing_mutex_trylock(&gc->alloc_mutex, "");
    if (EBUSY == trylock_result)
    {
        gc_vm_thread_fall_asleep(gc);
        check_return(tracing_mutex_lock(&gc->alloc_mutex, ""), "pthread_mutex_lock() failed.");
        gc_vm_thread_awoke(gc);
    }

    size_t free_pool_size = gc->pool_size - gc_get_used_size(gc);
    if (new_object_size > free_pool_size)
    {
        debug("No memory - collection needed. Needed: %zu, free: %zu.",
              new_object_size,
              free_pool_size);
        gc_wait_for_collect_completed(gc, false);

        free_pool_size = gc->pool_size - gc_get_used_size(gc);
        if (free_pool_size < new_object_size)
        {
            debug("Still no memory - triggering emergency collection.");
            gc_wait_for_collect_completed(gc, false);
            debug("Emergency collection end.");
            free_pool_size = gc->pool_size - gc_get_used_size(gc);
        }
    }

    while (new_object_size > free_pool_size &&
           NULL != gc->no_memory_callback &&
           gc->no_memory_callback(gc, gc->callback_data))
    {
        gc_wait_for_collect_completed(gc, false);
        free_pool_size = gc->pool_size - gc_get_used_size(gc);
    }

    ObjectHeader *new_object_location = NULL;
    if (new_object_size <= free_pool_size)
    {
        new_object_location = (ObjectHeader *) gc->free;
    }
    else
    {
        check_return(tracing_mutex_unlock(&gc->alloc_mutex, ""), "pthread_mutex_unlock() failed.");
        log_error("Out of memory.");
        return NULL;
    }

    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
    gc_lock_heap_range(gc, new_object_location, new_object_size, RANGE_LOCK_MODE_LOCK_AND_PROTECT, gc->protected_zone_end);
    memset(new_object_location, 0, new_object_size);

    ObjectHeader new_object_header = {
        .fields = {
            .is_root    = is_root,
            .is_scanned = gc->scanned_bit, // Create new object as unscanned.
            .is_garbage = gc->garbage_bit, // Create new object as garbage.
            .is_tracked = false,
            .flags      = flags,
            .total_size = new_object_size / OBJECT_SIZE_UNIT,
            .slot_count = slot_count,
        },
        .__pad__        = 0
    };

    *new_object_location = new_object_header;

    void *new_free = atomic_load(&gc->free) + new_object_size;
    atomic_store(&gc->free, new_free);
    gc_unlock_heap_range(gc, new_object_location, new_object_size, RANGE_LOCK_MODE_LOCK_AND_PROTECT, gc->protected_zone_end);
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");
    check_return(tracing_mutex_unlock(&gc->alloc_mutex, ""), "pthread_mutex_unlock() failed.");
    return new_object_location;

    error:
    abort();
    return NULL;
}

// Sleeps calling thread till collection is completed.
// If check_vm_working is true then falls asleep only if GC has requested VM to go to sleep.
// Otherwise, goes to sleep unconditionally (i.e. when there's no memory).
void gc_wait_for_collect_completed(GC *gc, bool check_vm_working)
{
    assert(gc && "Bad GC pointer.");

    if (check_vm_working)
    {
        check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
        bool is_wait_needed = !atomic_load(&gc->vm_working);
        check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
        if (!is_wait_needed)
        {
            return; // GC doesn't want VM to sleep, so continue.
        }
    }

    debug("Wait started.");
    check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
    atomic_fetch_add(&gc->vm_waiting_thread_count, 1);
    check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
    gc_vm_thread_fall_asleep(gc);
    check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
    while (true)
    {
        bool gc_vm_working = atomic_load(&gc->vm_working),
             gc_complete   = atomic_load(&gc->gc_complete);
        if (gc_vm_working && gc_complete)
        {
            break;
        }
        debug("Waiting for collect_completed_event. gc_vm_working: %u, gc_complete: %u.",
              gc_vm_working,
              gc_complete);
        check_return(pthread_cond_wait(&gc->collect_completed_event, &gc->vm_mutex), "pthread_cond_wait() failed.");
        debug("Got collect_completed_event.");
    }
    atomic_fetch_sub(&gc->vm_waiting_thread_count, 1);
    check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
    gc_vm_thread_awoke(gc);
    debug("Wait succeeded.");

    return;
    error:
    abort();
}

// TODO: Get rid of this global.
static GC *global_gc = NULL;

// Write barrier called by object_set_slot(). Does actual work.
void gc_notify_object_modified(GC *gc, ObjectHeader *object, ObjectHeader **slot, ObjectHeader *new_value)
{
    assert(gc && "Bad GC pointer.");
    assert(object && "Bad object pointer.");
    assert(slot && "Bad slot pointer.");

    assert((pthread_equal(pthread_self(), gc->thread) || !atomic_load(&gc->gc_collecting)) &&
           "Modification in sleep time.");

    if (!gc->is_parallel_marking_enabled)
    { // Just do write. There's no any memory protection and no notification is needed.
        *slot = new_value;
        return;
    }

    // Serialize access to gc->sigsegv_return.
    check_return(tracing_mutex_lock(&gc->mutation_mutex, ""), "pthread_mutex_lock() failed.");

    global_gc = gc;

    // Synchronize with gc_mark() to obtain actual gc->protected_zone_end.
    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");

    // This locking is needed because other thread may temporarily unlock same page to write to another object.
    // In this case, write will succeed which can lead to unnoticed write to scanned object.
    gc_lock_heap_range(gc, slot, OBJECT_SLOT_SIZE, RANGE_LOCK_MODE_LOCK_ONLY, gc->protected_zone_end);

    if (0 == sigsetjmp(gc->sigsegv_return, 1)) // Prepare jump buffer.
    {
        *slot = new_value; // Write is OK.

        #if !defined(NO_DEBUG)
        check(gc->scanned_bit == gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_SCANNED) ||
              !gc_get_page_protection_bit(gc, slot),
              "Write to scanned object didn't cause SEGFAULT. Object: %p, protected_zone_end: %p.",
              (void *) ((void *) object - gc->pool),
              (void *) (gc->protected_zone_end - gc->pool));
        #endif // !defined(NO_DEBUG).

        gc_unlock_heap_range(gc, slot, OBJECT_SLOT_SIZE, RANGE_LOCK_MODE_LOCK_ONLY, gc->protected_zone_end);
        check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");
        global_gc = NULL;
        check_return(tracing_mutex_unlock(&gc->mutation_mutex, ""), "pthread_mutex_unlock() failed.");
    }
    else // Write failed.
    {
        gc->statistics.sigsegv_count++;
        gc->statistics.total_sigsegv_count++;

        // Unprotect page containing slot.
        gc_lock_heap_range(gc, slot, OBJECT_SLOT_SIZE, RANGE_LOCK_MODE_PROTECT_ONLY, gc->protected_zone_end);
        check(((void *) slot + OBJECT_SLOT_SIZE) <= gc->protected_zone_end,
              "Slot %p wasn't unprotected. Protected_zone_end: %p.",
              (void *) ((void *) slot - gc->pool),
              (void *) (gc->protected_zone_end - gc->pool));
        *slot = new_value; // Do write exclusively.

        void *page     = (void *) ((size_t) slot & ~(gc->page_size - 1)),
             *page_end = page + gc->page_size;

        if (gc->protected_zone_end > page_end)
        { // Mark whole page as reachable & unscanned. Do not restore memory protection of the page.
            debug("Protected page was written. Unprotecting. Page: %p, written slot: %p.",
                  (void *) (page - gc->pool),
                  (void *) ((void *) slot - gc->pool));
            gc_set_page_protection_bit(gc, page, false);
            gc_mark_page_objects_as_unscanned(gc, page);
            gc_unlock_heap_range(gc, slot, OBJECT_SLOT_SIZE, RANGE_LOCK_MODE_LOCK_ONLY, gc->protected_zone_end);
        }
        else
        { // Mark object as unscanned. Restore memory protection of the page.
            if (gc->scanned_bit != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_SCANNED) &&     // If it's scanned
                NULL != new_value &&                                                                              // and there's a new value
                (gc->scanned_bit == gc_object_get_header_field_impl(new_value, OBJECT_HEADER_FIELD_IS_SCANNED) || // and new value is unscanned
                 gc->garbage_bit == gc_object_get_header_field_impl(new_value, OBJECT_HEADER_FIELD_IS_GARBAGE)))  // or it's garbage.
            {
                gc_update_object_header(gc,
                                        object,
                                        2,
                                        OBJECT_HEADER_FIELD_IS_SCANNED,
                                        (unsigned) gc->scanned_bit,
                                        OBJECT_HEADER_FIELD_IS_GARBAGE,
                                        (unsigned) (!gc->garbage_bit));

                debug("Scanned object was modified. Object: %p.",
                      (void *) ((void *) object - gc->pool));
            }

            // Restore protection.
            gc_unlock_heap_range(gc, slot, OBJECT_SLOT_SIZE, RANGE_LOCK_MODE_LOCK_AND_PROTECT, gc->protected_zone_end);
        }

        check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");
        global_gc = NULL;
        check_return(tracing_mutex_unlock(&gc->mutation_mutex, ""), "pthread_mutex_unlock() failed.");
    }

    return;
    error:
    abort();
}

// Marks all objects on a given page as reachable and unscanned.
void gc_mark_page_objects_as_unscanned(GC *gc, void *page)
{
    assert(gc && "Bad GC pointer.");
    assert(page && "Bad page address.");
    assert(page >= gc->pool && page < (gc->pool + gc->pool_size) &&  "Page not in pool.");
    assert(0 == ((size_t) page & (gc->page_size - 1)) && "Not a page address. Page: %p.");

    size_t marked_object_count = 0;

    void *page_end = min(page + gc->page_size, (void *) atomic_load(&gc->free));
    for (void *object_pointer = gc->pool;
         object_pointer < page_end;)
    {
        ObjectHeader *object = (ObjectHeader *) object_pointer;
        size_t object_size = gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
        void *object_end_pointer = object_pointer + object_size;

        if (object_end_pointer >= page)
        {
            if (object_pointer >= page)
            {
                object->fields.is_scanned = gc->scanned_bit;
                object->fields.is_garbage = !gc->garbage_bit;
            }
            else
            {
                gc_update_object_header(gc,
                                        object,
                                        2,
                                        OBJECT_HEADER_FIELD_IS_SCANNED,
                                        (unsigned) gc->scanned_bit,
                                        OBJECT_HEADER_FIELD_IS_GARBAGE,
                                        (unsigned) (!gc->garbage_bit));
            }

            marked_object_count++;
        }

        object_pointer += object_size;
    }

    debug("Marked %zu objects as unscanned.", marked_object_count);
}

// Handles SIGSEGV caused by VM threads writing to protected objects.
// Using sigsegv_return, returns to gc_notify_object_modified(), which marks object as unscanned.
void gc_object_modification_signal_handler(int signal, siginfo_t *signal_info, void *unused)
{
    (void) unused;

    check(SIGSEGV == signal, "Unexpected signal.");
    check(global_gc, "Bad GC pointer.");

    if (0 != pthread_equal(global_gc->thread, pthread_self()))
    {
        log_error("SIGSEGV occured in GC thread.");
        abort();
    }

    if (!(global_gc->pool <= signal_info->si_addr && signal_info->si_addr < (global_gc->pool + global_gc->pool_size)))
    {
        log_error("SIGSEGV occured not in pool.");
        abort();
    }

    siglongjmp(global_gc->sigsegv_return, 1); // Jump to handling.

    error:
    abort();
}

// Asks GC to update external reference to the object that reference is currently pointing to
// (or will be pointing to, in case of REFERENCE_TRACKING_MODE_HANDLE).
// Update means that reference is still valid after object is being moved or set to NULL when
// object is freed (for REFERENCE_TRACKING_MODE_WEAK).
bool gc_track_reference(GC *gc, ObjectHeader **reference, ReferenceTrackingMode tracking_mode)
{
    assert(gc && "Bad GC pointer.");
    check(reference, "Bad reference.");
    assert((REFERENCE_TRACKING_MODE_WEAK == tracking_mode ||
            REFERENCE_TRACKING_MODE_SINGLE_OBJECT == tracking_mode ||
            REFERENCE_TRACKING_MODE_HANDLE == tracking_mode) && "Bad tracking_mode value.");

    check_return(tracing_mutex_lock(&gc->tracked_references_lock, ""), "pthread_mutex_lock() failed.");

    ExternalReference reference_record = {
        .tracking_mode = tracking_mode,
        .reference     = reference
    };

    bool result = dynamic_array_push(gc->tracked_references, &reference_record);

    check_return(tracing_mutex_unlock(&gc->tracked_references_lock, ""), "pthread_mutex_unlock() failed.");

    if (REFERENCE_TRACKING_MODE_SINGLE_OBJECT == tracking_mode)
    {
        ObjectHeader *object = *reference;
        check(object, "Bad object pointer.");
        if (!gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_TRACKED))
        {
            gc_update_object_header(gc,
                                    object,
                                    1,
                                    OBJECT_HEADER_FIELD_IS_TRACKED,
                                    (unsigned) true);
        }
    }

    return result;
    error:
    abort();
    return false;
}

// Stops tracking given reference.
// If reference has mode REFERENCE_TRACKING_MODE_SINGLE_OBJECT and it's last reference to the object -
// resets is_tracked bit of pointed object to 0 making object collectable again.
void gc_untrack_reference(GC *gc, ObjectHeader **reference)
{
    assert(gc && "Bad GC pointer.");
    check(reference, "Bad reference.");

    check_return(tracing_mutex_lock(&gc->tracked_references_lock, ""), "pthread_mutex_lock() failed.");

    ExternalReference reference_record;
    bool reference_was_found = false;

    size_t tracked_reference_count = dynamic_array_count(gc->tracked_references);
    if (0 != tracked_reference_count)
    {
        // Track/untrack are likely to be used in LIFO order.
        for (ExternalReference *tracked_references_end   = dynamic_array_get_raw_data(gc->tracked_references), // Inclusive.
                               *tracked_reference_record = tracked_references_end + tracked_reference_count - 1;
             tracked_references_end <= tracked_reference_record;
             --tracked_reference_record)
        {
            if (reference == tracked_reference_record->reference)
            {
                reference_record = *tracked_reference_record;
                reference_was_found = true;
                dynamic_array_delete_at_position(gc->tracked_references, tracked_reference_record);
                tracked_reference_count--;
                break;
            }
        }

        if (reference_was_found &&
            REFERENCE_TRACKING_MODE_SINGLE_OBJECT == reference_record.tracking_mode)
        {
            ObjectHeader *object = *reference_record.reference;
            assert(object && "Bad object pointer."); // If we want to reset flag, it has to be set before, so
                                                     // object can't get collected.

            bool object_is_still_referenced = false;

            for (ExternalReference *tracked_reference_record = dynamic_array_get_raw_data(gc->tracked_references),
                                   *tracked_references_end   = tracked_reference_record + tracked_reference_count;
                 tracked_references_end != tracked_reference_record;
                 ++tracked_reference_record)
            {
                if (object == *(tracked_reference_record->reference) &&
                    REFERENCE_TRACKING_MODE_SINGLE_OBJECT == tracked_reference_record->tracking_mode)
                {
                    object_is_still_referenced = true;
                    break;
                }
            }

            if (!object_is_still_referenced) // If last tracked reference was deleted.
            {
                assert(gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_TRACKED) && "is_tracked is already reset.");
                gc_update_object_header(gc, object, 1, OBJECT_HEADER_FIELD_IS_TRACKED, (unsigned) false);
            }
        }
    }

    check_return(tracing_mutex_unlock(&gc->tracked_references_lock, ""), "pthread_mutex_unlock() failed.");

    return;
    error:
    abort();
}

// Returns pointer to start of the pool of objects.
void *gc_get_pool(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return gc->pool;
}

// Returns size of pool in bytes.
size_t gc_get_pool_size(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return gc->pool_size;
}

// Returns used size of pool in bytes.
size_t gc_get_used_size(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return (void *) atomic_load(&gc->free) - gc->pool;
}

// Marks objects referenced by external handles (REFERENCE_TRACKING_MODE_HANDLE) as reachable.
// Also marks garbage object referenced with REFERENCE_TRACKING_MODE_SINGLE_OBJECT - they were tracked
// after being marked.
void gc_mark_objects_as_reachable(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    bool gc_garbage_bit = gc->garbage_bit,
         gc_scanned_bit = gc->scanned_bit;
    size_t marked_object_count     = 0,
           tracked_reference_count = dynamic_array_count(gc->tracked_references);
    if (0 != tracked_reference_count)
    {
        for (ExternalReference *tracked_reference_record = dynamic_array_get_raw_data(gc->tracked_references),
                               *tracked_references_end   = tracked_reference_record + tracked_reference_count;
             tracked_reference_record != tracked_references_end;
             ++tracked_reference_record)
        {
            if ((REFERENCE_TRACKING_MODE_HANDLE != tracked_reference_record->tracking_mode &&
                 REFERENCE_TRACKING_MODE_SINGLE_OBJECT != tracked_reference_record->tracking_mode)||
                NULL == tracked_reference_record->reference ||
                NULL == *(tracked_reference_record->reference))
            {
                continue;
            }

            ObjectHeader *object = *(tracked_reference_record->reference);
            if (gc_garbage_bit == object->fields.is_garbage ||
                gc_scanned_bit == object->fields.is_scanned)
            {
                object->fields.is_garbage = !gc_garbage_bit;
                object->fields.is_scanned = gc_scanned_bit; // Old scanned means unscanned.
                marked_object_count++;
            }

            assert(object < (ObjectHeader *) gc->free && "Tracked reference points to freed part of heap.");
        }
    }

    debug("Marked object count: %zu.", marked_object_count);
}

// Sets external references which point to garbage objects to NULL.
void gc_update_freed_tracked_references(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    bool gc_garbage_bit = gc->garbage_bit;
    size_t tracked_reference_count = dynamic_array_count(gc->tracked_references);
    if (0 != tracked_reference_count)
    {
        for (ExternalReference *tracked_reference_record = dynamic_array_get_raw_data(gc->tracked_references),
                               *tracked_references_end   = tracked_reference_record + tracked_reference_count;
             tracked_references_end != tracked_reference_record;
             ++tracked_reference_record)
        {
            if (NULL == tracked_reference_record->reference ||
                NULL == *(tracked_reference_record->reference))
            {
                continue;
            }

            ObjectHeader *object = *(tracked_reference_record->reference);
            if (gc_garbage_bit == object->fields.is_garbage)
            {
                *(tracked_reference_record->reference) = NULL;
            }

            assert(object < (ObjectHeader *) gc->free && "Tracked reference points to freed part of heap.");
        }
    }
}

// Locks page lock corresponding to page which contains given address. Returns address of the page.
void *gc_lock_heap_page(GC *gc, void *address)
{
    assert(gc && "Bad GC pointer.");
    assert(address && "Bad address.");
    assert(gc->pool <= address && address < (gc->pool + gc->pool_size) && "Bad address.");
    size_t page_index = (address - gc->pool) / gc->page_size;
    assert(/*page_index >= 0 && */ page_index < gc->pool_page_count && "Bad page index.");
    // debug("Locking heap page #%zu.", page_index);
    check_return(tracing_mutex_lock(&gc->page_locks[page_index], "&gc->page_locks[%zu].", page_index), "pthread_mutex_lock() failed.");
    // debug("Locked heap page #%zu.", page_index);
    void *page_address = gc->pool + page_index * gc->page_size;
    assert(page_address + (size_t) address % gc->page_size == address);
    return page_address;
    error:
    abort();
    return NULL;
}

// Unlocks page lock corresponding to page which contains given address
void gc_unlock_heap_page(GC *gc, void *address)
{
    assert(gc && "Bad GC pointer.");
    assert(address && "Bad address.");
    assert(gc->pool <= address && address < (gc->pool + gc->pool_size) && "Bad address.");
    size_t page_index = (address - gc->pool) / gc->page_size;
    assert(/*page_index >= 0 && */ page_index < gc->pool_page_count && "Bad page index.");
    check_return(tracing_mutex_unlock(&gc->page_locks[page_index], "&gc->page_locks[%zu].", page_index), "pthread_mutex_unlock() failed.");
    // debug("Unlocked heap page #%zu.", page_index);
    return;
    error:
    abort();
}

// Locks heap pages which contain range defined by address and length.
// Actual operations to be performed are determined by lock_mode.
// mprotect() calls aren't done for pages beyond protected_zone_end.
void gc_lock_heap_range(GC *gc, void *address, size_t length, RangeLockMode lock_mode, void *protected_zone_end)
{
    assert(gc && "Bad GC pointer.");
    assert(address && "Bad address.");
    assert(length && "Bad length.");
    assert(gc->pool <= address && (address + length) <= (gc->pool + gc->pool_size) && "Bad address.");
    assert(RANGE_LOCK_MODE_LOCK_ONLY == lock_mode ||
           RANGE_LOCK_MODE_PROTECT_ONLY == lock_mode ||
           RANGE_LOCK_MODE_LOCK_AND_PROTECT == lock_mode);

    bool lock_needed      = 0 != (RANGE_LOCK_MODE_LOCK_ONLY & lock_mode),
         unprotect_needed = 0 != (RANGE_LOCK_MODE_PROTECT_ONLY & lock_mode);

    #if !defined(NO_DEBUG)
    void *original_address = address;
    size_t original_length = length;
    #endif // !defined(NO_DEBUG).

    length += (size_t) address % gc->page_size;
    address -= (size_t) address % gc->page_size;
    if (0 != length % gc->page_size)
    {
        length += gc->page_size - length % gc->page_size;
    }

    assert(0 == (size_t) address % gc->page_size);
    assert(0 == length % gc->page_size);
    assert(address <= original_address &&
           (original_address + original_length) <= (address + length));

    while (0 != length)
    {
        // debug("Locking: %p.", address);

        if (lock_needed)
        {
            gc_lock_heap_page(gc, address);
        }

        // debug("Locked: %p.", address);

        if (unprotect_needed && (address + gc->page_size) <= protected_zone_end)
        {
            check(0 == mprotect(address, gc->page_size, PROT_READ | PROT_WRITE), "mprotect() failed.");
        }

        address += gc->page_size;
        length -= gc->page_size;
    }

    return;
    error:
    abort();
}

// Unlocks heap pages which contain range defined by address and length.
// Actual operations to be performed are determined by lock_mode.
// mprotect() calls aren't done for pages beyond protected_zone_end.
void gc_unlock_heap_range(GC *gc, void *address, size_t length, RangeLockMode lock_mode, void *protected_zone_end)
{
    assert(gc && "Bad GC pointer.");
    assert(address && "Bad address.");
    assert(length && "Bad length.");
    assert(gc->pool <= address && (address + length) <= (gc->pool + gc->pool_size) && "Bad address.");
    assert(RANGE_LOCK_MODE_LOCK_ONLY == lock_mode ||
           RANGE_LOCK_MODE_PROTECT_ONLY == lock_mode ||
           RANGE_LOCK_MODE_LOCK_AND_PROTECT == lock_mode);

    bool unlock_needed  = 0 != (RANGE_LOCK_MODE_LOCK_ONLY & lock_mode),
         protect_needed = 0 != (RANGE_LOCK_MODE_PROTECT_ONLY & lock_mode);

    length += (size_t) address % gc->page_size;
    address -= (size_t) address % gc->page_size;
    if (0 != length % gc->page_size)
    {
        length += gc->page_size - length % gc->page_size;
    }

    assert(0 == (size_t) address % gc->page_size);
    assert(0 == length % gc->page_size);

    address += length - gc->page_size;

    while (0 != length)
    {
        // debug("Unlocking: %p.", address);

        if (protect_needed && (address + gc->page_size) <= protected_zone_end)
        {
            check(0 == mprotect(address, gc->page_size, PROT_READ), "mprotect() failed.");
        }

        if (unlock_needed)
        {
            gc_unlock_heap_page(gc, address);
        }

        address -= gc->page_size;
        length -= gc->page_size;
    }

    return;
    error:
    abort();
}

// Does actual collection.
size_t gc_collect(GC *gc)
{
    #if !defined(NO_DEBUG)
    double usage_ratio = (double) gc_get_used_size(gc) / (double) gc->pool_size;
    debug("Collect start. usage_ratio: %lf", usage_ratio);
    #endif // !defined(NO_DEBUG).

    gc->statistics.last_collected_garbage_objects = 0;
    gc->statistics.last_collected_size = 0;
    gc->statistics.last_collection_move_count = 0;

    gc_assert_heap(gc, true, false);
    debug("Finalization started.");
    gc_finalize_garbage(gc);
    debug("Finalization completed.");
    debug("Defragment started.");
    gc_defragment_garbage(gc);
    debug("Defragment completed.");
    gc_assert_heap(gc, false, false);
    debug("Compact started.");
    size_t freed_size = gc_compact(gc);
    assert(freed_size == gc->statistics.last_collected_size);
    debug("Compact completed.");
    gc_assert_heap(gc, false, true);

    #if !defined(NO_DEBUG)
    usage_ratio = (double) gc_get_used_size(gc) / (double) gc->pool_size;
    debug("Collect end. usage_ratio: %lf", usage_ratio);
    #endif // !defined(NO_DEBUG).
    return freed_size;
}

// Does marking by walking through object graph and calling gc_scan_object() on non-garbage or root objects.
// In case of parallel marking protects pages with scanned objects.
void gc_mark(GC *gc, bool is_concurrent_mark)
{
    assert(gc && "Bad GC pointer.");

    size_t reachable_object_count = 0;

    for (void *object_pointer = gc->pool;;)
    {
        if (is_concurrent_mark)
        {
            check(0 == usleep(gc->mark_delay * 1000), "usleep() failed.");
        }

        gc->parallel_mark_boundary = (void *) atomic_load(&gc->free);

        if (object_pointer >= gc->parallel_mark_boundary)
        {
            if (is_concurrent_mark)
            {
                void *new_parallel_mark_boundary = NULL;

                do
                {
                    check(0 == sched_yield(), "thread_yield() failed.");
                    new_parallel_mark_boundary = (void *) atomic_load(&gc->free);
                }
                while (!gc_vm_is_sleeping(gc) && object_pointer >= new_parallel_mark_boundary);

                if (new_parallel_mark_boundary == gc->parallel_mark_boundary ||
                    gc_vm_is_sleeping(gc))
                {
                    break;
                }
                else
                {
                    check(gc->parallel_mark_boundary < new_parallel_mark_boundary, "Parallel mark boundary wasn't pushed.");
                    gc->parallel_mark_boundary = new_parallel_mark_boundary;
                }
            }
            else
            {
                break;
            }
        }

        ObjectHeader *object = (ObjectHeader *) object_pointer;
        size_t object_size = gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
        void *object_end_pointer = object_pointer + object_size;
        bool is_scanned = gc->scanned_bit != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_SCANNED);

        // Protect this object.
        check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
        void *protected_zone_end = gc->protected_zone_end;
        check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");
        if (is_concurrent_mark && object_end_pointer > protected_zone_end)
        {
            debug("Invoking protection. object: %p, protected_zone_end: %p.",
                  (void *) ((void *) object - gc->pool),
                  (void *) (protected_zone_end - gc->pool));
            gc_protect_scanned_heap_pages(gc, object);
        }

        if (is_scanned)
        {
            object_pointer += object_size; // Move to next object.
        }
        else
        {
            if (gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_ROOT) ||
                gc->garbage_bit != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_GARBAGE) ||
                gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_TRACKED))
            {
                gc_update_object_header(gc,
                                        object,
                                        2,
                                        OBJECT_HEADER_FIELD_IS_SCANNED,
                                        (unsigned) (!gc->scanned_bit),
                                        OBJECT_HEADER_FIELD_IS_GARBAGE,
                                        (unsigned) (!gc->garbage_bit));

                object_pointer = min(object, gc_scan_object(gc, object));

                reachable_object_count++;
                gc->statistics.alive_objects++;
                gc->statistics.used_size += gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
            }
            else
            {
                gc_update_object_header(gc,
                                        object,
                                        2,
                                        OBJECT_HEADER_FIELD_IS_SCANNED,
                                        (unsigned) (!gc->scanned_bit),
                                        OBJECT_HEADER_FIELD_IS_GARBAGE,
                                        (unsigned) gc->garbage_bit);

                object_pointer += object_size;

                gc->statistics.garbage_objects++;
                gc->statistics.garbage_size += gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
            }
        }

        if (is_concurrent_mark && gc_vm_is_sleeping(gc))
        {
            break;
        }
    }

    debug("Reachable objects: %zu.", reachable_object_count);

    return;
    error:
    abort();
}

// Ensures that 3-color invariant is valid.
// This function checks that scanned non-garbage objects do not point to garbage objects.
// If object with such reference is detected - it's marked as unscanned.
void gc_fix_object_graph(GC *gc)
{
    size_t fix_count = 0;

    for (void *object_pointer = gc->pool; object_pointer < (void *) gc->free;)
    {
        ObjectHeader *object = (ObjectHeader *) object_pointer;
        size_t object_size = gc_object_get_size_exclusive(gc, object) * OBJECT_SIZE_UNIT;

        if (gc->garbage_bit != object->fields.is_garbage)
        {
            size_t object_slot_count = object->fields.slot_count;
            for (size_t slot = 0; slot < object_slot_count; ++slot)
            {
                ObjectHeader *referenced_object = object_get_slot_impl(gc, object, slot);
                if (NULL != referenced_object && gc->garbage_bit == referenced_object->fields.is_garbage)
                {
                    object->fields.is_scanned = gc->scanned_bit;
                    fix_count++;

                    // debug("Fix. Object: %p, referenced object: %p.",
                    //       (void *) ((void *) object - gc->pool),
                    //       (void *) ((void *) referenced_object - gc->pool));
                    break;
                }
            }
        }

        object_pointer += object_size;
    }

    debug("Number of fixes: %zu.", fix_count);
}

// Does actual scanning of single object.
// Marks referenced objects of this object as non garbage and not scanned yet.
// Returns smallest (closest to the beginning of pool) object to be scanned.
// This approach results in extra-scanning job, but doesn't require additional memory for list of not yet scanned
// objects.
ObjectHeader *gc_scan_object(GC *gc, ObjectHeader *object)
{
    ObjectHeader *result = object;

    size_t object_slot_count = gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_SLOT_COUNT);
    for (size_t slot = 0; slot < object_slot_count; ++slot)
    {
        ObjectHeader *referenced_object = object_get_slot_impl(gc, object, slot);
        if (NULL == referenced_object ||
            object == referenced_object)
        {
            continue;
        }

        if (gc->garbage_bit != gc_object_get_header_field_impl(referenced_object, OBJECT_HEADER_FIELD_IS_GARBAGE) &&
            gc_object_get_header_field_impl(referenced_object, OBJECT_HEADER_FIELD_IS_SCANNED) != gc->scanned_bit)
        {
            continue;
        }

        assert(referenced_object < (ObjectHeader *) atomic_load(&gc->free) && "Referenced object is in freed part of heap.");

        gc_update_object_header(gc,
                                referenced_object,
                                2,
                                OBJECT_HEADER_FIELD_IS_GARBAGE,
                                (unsigned) (!gc->garbage_bit),
                                OBJECT_HEADER_FIELD_IS_SCANNED,
                                (unsigned) gc->scanned_bit); // Old scanned bit means unscanned.

        result = min(result, referenced_object);
    }

    return result;
}

// Moves forward protected_zone_end to include highest_scanned_object to protected area and protects heap.
void gc_protect_scanned_heap_pages(GC *gc, ObjectHeader *highest_scanned_object)
{
    assert(gc && "Bad GC pointer.");
    assert(highest_scanned_object && "Bad highest_scanned_object pointer.");

    void *protected_zone_end = ((void *) highest_scanned_object) + gc_object_get_size(gc, highest_scanned_object) * OBJECT_SIZE_UNIT;
    size_t protected_zone_size = protected_zone_end - gc->pool;
    if (0 != protected_zone_size % gc->page_size)
    {
        protected_zone_end -= protected_zone_size % gc->page_size;
        protected_zone_end += gc->page_size;
    }

    check(protected_zone_end > gc->pool && protected_zone_end <= (gc->pool + gc->pool_size),
          "Bad protected_zone_end: %p, object: %p.",
          (void *) (protected_zone_end - gc->pool),
          (void *) ((void *) highest_scanned_object - gc->pool));

    gc_protect_heap(gc, protected_zone_end);

    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");

    #if !defined(NO_DEBUG)
    // Assert that each scanned object is protected.
    for (void *scan_pointer = gc->pool; scan_pointer < gc->parallel_mark_boundary;)
    {
        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        if (gc->scanned_bit != gc_object_get_header_field_impl(object, OBJECT_HEADER_FIELD_IS_SCANNED))
        {
            check(scan_pointer < gc->protected_zone_end,
                  "Found scanned object which isn't protected. parallel_mark_boundary: %p, highest_scanned_object: %p, object: %p, is_garbage: %u, protected_zone_end: %p, free: %p.",
                  (void *) (gc->parallel_mark_boundary - gc->pool),
                  (void *) ((void *) highest_scanned_object - gc->pool),
                  (void *) (scan_pointer - gc->pool),
                  gc->garbage_bit == object->fields.is_garbage,
                  (void *) (gc->protected_zone_end - gc->pool),
                  (void *) ((void *) atomic_load(&gc->free) - gc->pool));
        }

        scan_pointer += gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
    }
    #endif // !defined(NO_DEBUG).
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");

    return;
    error:
    abort();
}

// Protects pages in range pool ... protected_zone_end.
void gc_protect_heap(GC *gc, void *new_protected_zone_end)
{
    // debug("Started.");
    assert(gc && "Bad GC pointer.");
    assert(NULL != new_protected_zone_end && "Bad new_protected_zone_end pointer.");

    check(0 == (new_protected_zone_end - gc->pool) % gc->page_size, "Protected zone isn't page-aligned.");
    check(gc->pool != new_protected_zone_end, "Nothing is protected.");

    check_return(tracing_mutex_lock(&gc->mutation_mutex, ""), "pthread_mutex_lock() failed.");
    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
    gc_lock_heap_range(gc, gc->pool, new_protected_zone_end - gc->pool, RANGE_LOCK_MODE_LOCK_ONLY, NULL);

    assert(gc->protected_zone_end < new_protected_zone_end);
    gc->protected_zone_end = new_protected_zone_end;

    debug("protected_zone_end: %p.", (void *) (gc->protected_zone_end - gc->pool));

    gc_set_page_protection_bit(gc, gc->protected_zone_end - gc->page_size, true);
    check(0 == mprotect(gc->protected_zone_end - gc->page_size, gc->page_size, PROT_READ), "mprotect() failed.");
    check(0 == mprotect(gc->protected_zone_end, gc->pool_size - (gc->protected_zone_end - gc->pool), PROT_READ | PROT_WRITE), "mprotect() failed.");

    gc_unlock_heap_range(gc, gc->pool, new_protected_zone_end - gc->pool, RANGE_LOCK_MODE_LOCK_ONLY, NULL);
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");
    check_return(tracing_mutex_unlock(&gc->mutation_mutex, ""), "pthread_mutex_unlock() failed.");

    // debug("Finished.");
    return;
    error:
    abort();
}

// Makes whole heap writeable and moves protected_zone_end to pool, thus indicating that nothing is protected.
void gc_unprotect_heap(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
    gc_clear_memory_protection_bits(gc);
    check(0 == mprotect(gc->pool, gc->pool_size, PROT_READ | PROT_WRITE), "mprotect() failed.");
    gc->protected_zone_end = gc->pool;
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");

    debug("Heap unprotected.");
    return;
    error:
    abort();
}

// Finalizes garbage objects.
void gc_finalize_garbage(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    if (NULL == gc->finalizer)
    {
        return; // No finalizer registered.
    }

    for (void *scan_pointer = gc->pool; scan_pointer < (void *) gc->free;)
    {
        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        size_t object_size = gc_object_get_size_exclusive(gc, object) * OBJECT_SIZE_UNIT;

        if (gc->garbage_bit == object->fields.is_garbage)
        {
            gc->finalizer(gc, gc->callback_data, object);
        }

        scan_pointer += object_size;
    }
}

// Turns G objects to F objects and glues together consecutive F objects.
void gc_defragment_garbage(GC *gc)
{
    void *scan_pointer = gc->pool;
    ObjectHeader *previous_fill_object = NULL; // For joining F objects.

    while (scan_pointer < (void *) gc->free)
    {
        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        size_t object_size = gc_object_get_size_exclusive(gc, object);

        check(0 != object->fields.total_size || gc->garbage_bit == object->fields.is_garbage,
              "Bad object size. Object: %p.",
              (void *) (((void *) object) - gc->pool));

        if (gc->garbage_bit != object->fields.is_garbage)
        { // It's R.
            previous_fill_object = NULL;
        }
        else
        {
            gc->statistics.last_collected_garbage_objects++;
            gc->statistics.last_collected_size += object_size * OBJECT_SIZE_UNIT;

            object->fields.slot_count = 0; // G becomes F.
            if (NULL != previous_fill_object)
            { // Merge previous F with this F.
                size_t previous_fill_object_size = gc_object_get_size_exclusive(gc, previous_fill_object),
                       merged_object_size        = previous_fill_object_size + object_size;
                gc_object_set_size(gc, previous_fill_object, merged_object_size);
            }
            else
            {
                gc_object_set_size(gc, object, object_size);
                previous_fill_object = object;
            }
        }

        scan_pointer += object_size * OBJECT_SIZE_UNIT;
    }

    return;
    error:
    abort();
    return;
}

// Moves R objects to the beginning of the heap and updates free pointer.
size_t gc_compact(GC *gc)
{
    size_t new_used_size = 0;
    void *scan_pointer = gc->pool,
         *fill_pointer = NULL;

    while (scan_pointer < (void *) gc->free)
    {
        /* double total_size   = (void *) gc->free - gc->pool,
         *        scanned_size = scan_pointer - gc->pool;
         * debug("Compaction: %2.4lf%%", scanned_size / total_size * 100.0);
         */

        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        size_t object_size = gc_object_get_size_exclusive(gc, object);

        if (gc->garbage_bit == object->fields.is_garbage)
        {
            if (NULL == fill_pointer)
            {
                fill_pointer = scan_pointer;
            }

            scan_pointer += object_size * OBJECT_SIZE_UNIT; // Move to next object.
            continue;
        }

        // It's R.
        ObjectHeader *reachable_block_end = gc_find_reachable_block_end(gc, object);
        size_t reachable_block_size = (void *) reachable_block_end - (void *) object;

        new_used_size += reachable_block_size;

        if (NULL == fill_pointer)
        { // It's already compacted. Nothing to do here.

            // Still, remeber it with 0 shift for gc_update_references() not to search too long.
            ReferenceUpdateRecord reference_update_record = {
                .shift       = 0,
                .block_end   = reachable_block_end
            };
            check(dynamic_array_push(gc->reference_update_records, &reference_update_record), "dynamic_array_push() failed.");

            scan_pointer += reachable_block_size; // Move to next object.
            continue;
        }

        gc_move(gc, object, reachable_block_end, fill_pointer);
        gc->statistics.last_collection_move_count++;

        // Advance scan_pointer.
        fill_pointer += reachable_block_size; // Now it points to F after sequence of R objects.
        object_size = gc_object_get_size_exclusive(gc, fill_pointer);
        scan_pointer = fill_pointer + object_size * OBJECT_SIZE_UNIT; // Move to next object.
    }

    gc_update_references(gc);

    size_t freed_size = gc_get_used_size(gc) - new_used_size;
    #if !defined(NO_DEBUG)
    log_info("Freed: %zu.", freed_size);
    #endif // !defined(NO_DEBUG).

    if (NULL == fill_pointer)
    {
        fill_pointer = scan_pointer;
        assert(0 == freed_size && "Asserting GC state.");
    }

    assert((fill_pointer < ((void *) gc->free) || 0 == freed_size) && "No compaction occured.");
    gc->free = (atomic_uintptr_t) fill_pointer;
    return freed_size;
    error:
    abort();
    return 0;
}

// Returns pointer to first non-R object after reachable_block_start.
// Objects in range reachable_block_start ... <return_value> are reachable.
ObjectHeader *gc_find_reachable_block_end(GC *gc, ObjectHeader *reachable_block_start)
{
    assert(gc && "Bad GC pointer.");
    assert(reachable_block_start && "Bad reachable_block_start pointer.");

    check(gc->garbage_bit != reachable_block_start->fields.is_garbage,
          "Reachable object is garbage. Object: %p",
          (void *) ((void *) reachable_block_start - gc->pool));

    while ((void *) reachable_block_start < (void *) gc->free &&
           gc->garbage_bit != reachable_block_start->fields.is_garbage)
    {
        size_t object_size = gc_object_get_size_exclusive(gc, reachable_block_start) * OBJECT_SIZE_UNIT;
        reachable_block_start = (void *) reachable_block_start + object_size;
    }

    return reachable_block_start;
    error:
    abort();
    return NULL;
}

// If fill_object is followed by another F object, glues them together, thus keeping F objects defragmented.
bool gc_check_for_consecutive_free_blocks(GC *gc, ObjectHeader *fill_object)
{
    bool result = false; // F blocks merged?

    check(gc->garbage_bit == fill_object->fields.is_garbage,
          "F object should have a is_garbage flag. Object: %p.",
          (void *) (((void *) fill_object) - gc->pool));

    check(0 == fill_object->fields.slot_count,
          "F object should have 0 slots. Object: %p.",
          (void *) (((void *) fill_object) - gc->pool));

    for (size_t merges = 0;; ++merges)
    {
        assert(1 >= merges && "2 F objects in a row.");
        size_t fill_object_size = gc_object_get_size_exclusive(gc, fill_object);
        void *next_object_address = ((void *) fill_object) + fill_object_size * OBJECT_SIZE_UNIT;
        if (next_object_address >= (void *) gc->free)
        {
            return result;
        }

        ObjectHeader *next_object = (ObjectHeader *) next_object_address;
        if (gc->garbage_bit == next_object->fields.is_garbage)
        { // Do merge.
            size_t next_object_size   = gc_object_get_size_exclusive(gc, next_object),
                   merged_object_size = fill_object_size + next_object_size;
            gc_object_set_size(gc, fill_object, merged_object_size);
            result = true;
            check(0 == next_object->fields.total_size && 0 == next_object->fields.slot_count,
                  "Bad next F object: %p.",
                  (void *) (next_object_address - gc->pool));
        }
        else
        {
            return result;
        }
    }

    error:
    abort();
    return false;
}

// Helper function to get size of object in bytes.
#if defined(NO_DEBUG)
#undef gc_object_get_size_in_bytes
size_t gc_object_get_size_in_bytes(ObjectHeader *object)
#define gc_object_get_size_in_bytes(gc, object) gc_object_get_size_in_bytes(object)
#else
size_t gc_object_get_size_in_bytes(GC *gc, ObjectHeader *object)
#endif // defined(NO_DEBUG).
{
    return gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT;
}

// Helper function which atomically gets given header field of given object.
inline unsigned gc_object_get_header_field(ObjectHeader *object, int field)
{
    return gc_object_get_header_field_impl(object, field);
}

// Helper function to set size of object in OBJECT_SIZE_UNITs.
size_t gc_object_set_size(GC *gc, ObjectHeader *object, size_t new_size)
{
    assert(object && "Bad object pointer.");
    assert(new_size * OBJECT_SIZE_UNIT >= 2 * OBJECT_SLOT_SIZE && "Bad new size.");

    if (gc->garbage_bit == object->fields.is_garbage)
    {
        assert(0 == object->fields.slot_count && "Not a fill object.");
        object->fields.total_size = 0;
        *((size_t *) (object + 1)) = new_size;
    }
    else
    {
        assert(new_size * OBJECT_SIZE_UNIT <= OBJECT_MAX_SIZE && "Object is too big.");
        object->fields.total_size = new_size;
    }

    return new_size;
}

// Updates header of given object.
// Takes necessary locks and temporarily unprotects the object in case of parallel execution with VM.
// Possible header fields to change are defined in enum ObjectHeaderFlag. Next function parameter after
// field to change is an unsigned with new value of the flag. Several pairs of flag and new value may be specified.
// Example: gc_update_object_header(gc, object, OBJECT_HEADER_FIELD_IS_SCANNED, (unsigned) true);
// Example: gc_update_object_header(gc, object, OBJECT_HEADER_FIELD_IS_SCANNED, (unsigned) true, OBJECT_HEADER_FIELD_IS_GARBAGE, (unsigned) false);
void gc_update_object_header(GC *gc, ObjectHeader *object, size_t number_of_flags,  ...)
{
    va_list arguments;
    va_start(arguments, number_of_flags);

    // debug("Started.");
    assert(gc && "Bad GC pointer.");
    assert(object && "Bad object pointer.");
    assert(number_of_flags > 0 && "Bad number_of_flags value.");

    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
    void *protected_zone_end = gc->protected_zone_end;
    RangeLockMode object_page_lock_mode = gc_get_page_protection_bit(gc, object)
                                              ? RANGE_LOCK_MODE_LOCK_AND_PROTECT
                                              : RANGE_LOCK_MODE_LOCK_ONLY;

    if ((void *) object < protected_zone_end)
    {
        gc_lock_heap_range(gc, object, OBJECT_SLOT_SIZE, object_page_lock_mode, protected_zone_end);
    }

    for (size_t i = 0; i < number_of_flags; ++i)
    {
        int flag_to_change = va_arg(arguments, int);
        unsigned new_flag_value = va_arg(arguments, unsigned);

        switch (flag_to_change)
        {
            case OBJECT_HEADER_FIELD_IS_ROOT:
                object->fields.is_root = new_flag_value;
                break;

            case OBJECT_HEADER_FIELD_IS_SCANNED:
                object->fields.is_scanned = new_flag_value;
                break;

            case OBJECT_HEADER_FIELD_IS_GARBAGE:
                object->fields.is_garbage = new_flag_value;
                break;

            case OBJECT_HEADER_FIELD_IS_TRACKED:
                object->fields.is_tracked = new_flag_value;
                break;

            case OBJECT_HEADER_FIELD_FLAGS:
                object->fields.flags = new_flag_value;
                break;

            default:
                sentinel("Bad flag_to_change value: %d.", flag_to_change);
                break;
        }
    }

    if ((void *) object < protected_zone_end)
    {
        gc_unlock_heap_range(gc, object, OBJECT_SLOT_SIZE, object_page_lock_mode, protected_zone_end);
    }
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_unlock() failed.");

    va_end(arguments);

    // debug("Finished.");
    return;
    error:
    abort();
}

// Locks object's memory and allows direct modifications of its raw data.
void gc_lock_object(GC *gc, ObjectHeader *object)
{
    assert(gc && "Bad GC pointer.");
    assert(object && "Bad object pointer.");

    check_return(tracing_mutex_lock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");
    RangeLockMode object_page_lock_mode = gc_get_page_protection_bit(gc, object)
                                              ? RANGE_LOCK_MODE_LOCK_AND_PROTECT
                                              : RANGE_LOCK_MODE_LOCK_ONLY;
    gc_lock_heap_range(gc,
                       object,
                       gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT,
                       object_page_lock_mode,
                       gc->protected_zone_end);

    return;
    error:
    abort();
}

// Unlocks object's memory.
void gc_unlock_object(GC *gc, ObjectHeader *object)
{
    assert(gc && "Bad GC pointer.");
    assert(object && "Bad object pointer.");

    int object_page_lock_mode = gc_get_page_protection_bit(gc, object)
                                    ? RANGE_LOCK_MODE_LOCK_AND_PROTECT
                                    : RANGE_LOCK_MODE_LOCK_ONLY;
    gc_unlock_heap_range(gc,
                         object,
                         gc_object_get_size(gc, object) * OBJECT_SIZE_UNIT,
                         object_page_lock_mode,
                         gc->protected_zone_end);
    check_return(tracing_mutex_unlock(&gc->protected_zone_end_lock, ""), "pthread_mutex_lock() failed.");

    return;
    error:
    abort();
}

// Cuts off leading part of fill_object of requested_size and produces new F object at the end.
// Returns size of new F object in the gap pointer.
ObjectHeader *gc_split(GC *gc, ObjectHeader *fill_object, size_t requested_size, size_t *gap)
{
    assert(fill_object && "Bad fill_object pointer.");
    assert(gap && "Bad gap pointer.");

    size_t fill_object_total_size = gc_object_get_size_exclusive(gc, fill_object) * OBJECT_SIZE_UNIT;

    check(0 != fill_object_total_size,
          "Bad fill_object size. Object: %p.",
          (void *) (((void *) fill_object) - gc->pool));
    check(0 == fill_object->fields.slot_count,
          "F object with slots. Object: %p.",
          (void *) (((void *) fill_object) - gc->pool));
    check(requested_size <= fill_object_total_size,
          "Given fill_object is too small. Size: %zu, requested: %zu.",
          fill_object_total_size,
          requested_size);

    if (requested_size == fill_object_total_size)
    {
        *gap = 0;
        return NULL;
    }

    check(0 == (fill_object_total_size - requested_size) % OBJECT_SLOT_SIZE,
          "Difference in size must be a multiple of OBJECT_SLOT_SIZE. fill_object: %p, requested_size: %zu.",
          (void *) (((void *) fill_object) - gc->pool),
          requested_size);

    gc_object_set_size(gc, fill_object, requested_size / OBJECT_SIZE_UNIT);

    ObjectHeader *tail_object = ((void *) fill_object) + requested_size;

    if (fill_object_total_size - requested_size < 2 * OBJECT_SLOT_SIZE)
    {
        *gap = fill_object_total_size - requested_size;
        return tail_object;
    }

    *gap = 0;

    memset(tail_object, 0, sizeof(ObjectHeader));
    tail_object->fields.is_root    = false;
    tail_object->fields.is_scanned = gc->scanned_bit;
    tail_object->fields.is_garbage = gc->garbage_bit;

    gc_object_set_size(gc, tail_object, (fill_object_total_size - requested_size) / OBJECT_SIZE_UNIT);

    check(fill_object_total_size == ((gc_object_get_size_exclusive(gc, fill_object) + gc_object_get_size_exclusive(gc, tail_object)) * OBJECT_SIZE_UNIT),
          "Size of splitted objects doesn't match original size of the block. fill_object: %p, requested_size: %zu.",
          (void *) (((void *) fill_object) - gc->pool),
          requested_size);

    return tail_object;

    error:
    abort();
    return NULL;
}

// Moves (R) objects in range (object_block_start ... object_block_end)
// to fill_object (F). Does necessary split of fill_object.
void gc_move(GC *gc, ObjectHeader *object_block_start, ObjectHeader *object_block_end, ObjectHeader *fill_object)
{
    size_t object_block_size = (void *) object_block_end - (void *) object_block_start,
           fill_object_size  = gc_object_get_size_exclusive(gc, fill_object) * OBJECT_SIZE_UNIT;

    if (fill_object_size < object_block_size)
    {
        fill_object_size += object_block_size; // Overlap.
    }

    void *object_start = object_block_start,
         *object_end   = object_block_end,
         *fill_start   = fill_object,
         *fill_end     = ((void *) fill_object) + fill_object_size;

    #if !defined(NO_DEBUG)
    bool is_in_front  = fill_end < object_start,     // FFFF***RRR.
         is_behind    = object_end <= fill_start;    // RRR***FFFF or RRRFFFF.
    #endif // !defined(NO_DEBUG).

    bool is_touch     = fill_end == object_start,    // FFFFRRR.
         is_overlap   = fill_start < object_start && // FFRRR.
                        object_start < fill_end &&
                        fill_end == object_end;

    assert((is_in_front ^
            is_touch ^
            is_overlap ^
            is_behind) &&
           "Bad mutual position of object and fill_object.");
    assert(!is_in_front && !is_behind && "Can't happen.");

    if (is_touch)
    {
        size_t gap = 0;
        ObjectHeader *tail_object = gc_split(gc, fill_object, object_block_size, &gap);
        memcpy(fill_start, object_start, object_block_size);
        gc_free_object(gc, object_start, object_block_size);
        if (NULL != tail_object)
        {
            if (0 == gap)
            {
                gc_check_for_consecutive_free_blocks(gc, tail_object);
            }
            else
            {
                size_t tail_object_size = gc_object_get_size_exclusive(gc, object_start);
                assert(0 == gap % OBJECT_SLOT_SIZE && "Bad gap size.");
                memcpy(tail_object, object_start, sizeof(ObjectHeader));
                gc_object_set_size(gc, tail_object, tail_object_size + gap / OBJECT_SIZE_UNIT);
            }
        }
    }
    else if (is_overlap)
    {
        memmove(fill_start, object_start, object_block_size);
        ObjectHeader *tail_object = fill_start + object_block_size;
        gc_free_object(gc, tail_object, fill_object_size - object_block_size);
    }
    else
    {
        sentinel("Shouldn't be here.");
    }

    ReferenceUpdateRecord reference_update_record = {
        .shift       = object_start - fill_start,
        .block_end   = object_end
    };
    check(dynamic_array_push(gc->reference_update_records, &reference_update_record), "dynamic_array_push() failed.");
    gc_assert_heap(gc, false, false);
    return;

    error:
    abort();
    return;
}

// Marks given object as garbage (F) and glues it with following F object.
void gc_free_object(GC *gc, ObjectHeader *object, size_t new_size)
{
    size_t object_size = 0 != new_size ? new_size : gc_object_get_size_exclusive(gc, object);
    memset(object, 0, sizeof(ObjectHeader));
    object->fields.is_root    = false;
    object->fields.is_scanned = gc->scanned_bit;
    object->fields.is_garbage = gc->garbage_bit;
    if (0 != new_size)
    {
        gc_object_set_size(gc, object, object_size / OBJECT_SIZE_UNIT);
    }
    else
    {
        gc_object_set_size(gc, object, object_size);
    }

    gc_check_for_consecutive_free_blocks(gc, object);

    assert(0 == object->fields.slot_count && "F object should have 0 slots.");
    assert(0 == object->fields.total_size && "Bad F object size.");
}

// Updates heap and external references to the objects, which were moved.
// Information about moved blocks and their shifts is stored in gc->reference_update_records.
// References, which fall into some range (object_block_start ... object_block_end) are moved
// by corresponding shift to the beginning of the heap.
void gc_update_references(GC *gc)
{
    size_t reference_update_record_count = dynamic_array_count(gc->reference_update_records);
    debug("Reachable block count: %zu.", reference_update_record_count);
    if (0 == reference_update_record_count)
    {
        return;
    }

    ReferenceUpdateRecord *reference_update_records_start = dynamic_array_get_raw_data(gc->reference_update_records),
                          *reference_update_records_end   = reference_update_records_start + reference_update_record_count;

    #if !defined(NO_DEBUG)
    { // Assert that info about moved blocks is sorted in the ascending order.
        void *block_end = gc->pool;
        for (ReferenceUpdateRecord *reference_update_record = reference_update_records_start;
             reference_update_records_end != reference_update_record;
             ++reference_update_record)
        {
            assert(block_end < reference_update_record->block_end);
            block_end = reference_update_record->block_end;
        }
    }
    #endif // !defined(NO_DEBUG).

    void *gc_free = (void *) gc->free;
    bool gc_garbage_bit = gc->garbage_bit;

    // Update heap references.
    for (void *scan_pointer = gc->pool; scan_pointer < gc_free;)
    {
        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        size_t object_size = gc_object_get_size_exclusive(gc, object) * OBJECT_SIZE_UNIT;

        #if !defined(NO_DEBUG)
        check(0 != object->fields.total_size || gc_garbage_bit == object->fields.is_garbage,
              "Bad object size. Object: %p.",
              (void *) (((void *) object) - gc->pool));
        #endif // !defined(NO_DEBUG).

        if (gc_garbage_bit == object->fields.is_garbage)
        {
            scan_pointer += object_size;
            continue;
        }

        size_t object_slot_count = object->fields.slot_count;
        for (size_t slot = 0; slot < object_slot_count; ++slot)
        {
            void *slot_value = object_get_slot_impl(gc, object, slot);

            for (ReferenceUpdateRecord *reference_update_record = reference_update_records_start;
                 reference_update_records_end != reference_update_record;
                 ++reference_update_record)
            {
                if (slot_value < reference_update_record->block_end)
                {
                    size_t shift = reference_update_record->shift;
                    if (0 != shift)
                    {
                        object_set_slot_exclusive(gc, object, slot, slot_value - shift);
                    }
                    break;
                }
            }
        }

        scan_pointer += object_size;
    }

    // Update tracked external references.
    size_t tracked_reference_count = dynamic_array_count(gc->tracked_references);
    if (0 != tracked_reference_count)
    {
        for (ExternalReference *tracked_reference_record = dynamic_array_get_raw_data(gc->tracked_references),
                               *tracked_references_end   = tracked_reference_record + tracked_reference_count;
             tracked_references_end != tracked_reference_record;
             ++tracked_reference_record)
        {
            void *reference_value = *(tracked_reference_record->reference);

            for (ReferenceUpdateRecord *reference_update_record = reference_update_records_start;
                 reference_update_records_end != reference_update_record;
                 ++reference_update_record)
            {
                if (reference_value < reference_update_record->block_end)
                {
                    size_t shift = reference_update_record->shift;
                    if (0 != shift)
                    {
                        *(tracked_reference_record->reference) = reference_value - shift;
                    }
                    break;
                }
            }
        }
    }

    dynamic_array_clear(gc->reference_update_records);
    return;
    #if !defined(NO_DEBUG)
    error:
    abort();
    #endif // defined(NO_DEBUG).
}

#if !defined(NO_DEBUG)
// Performs internal checks of GC state.
void gc_assert_heap(GC *gc, bool asserting_after_mark, bool asserting_after_collect)
{
    for (void *scan_pointer     = gc->pool,
              *previous_pointer = NULL;
         scan_pointer < (void *) gc->free;)
    {
        ObjectHeader *object = (ObjectHeader *) scan_pointer;
        void *object_pool_address = (void *) (scan_pointer - gc->pool);

        if (asserting_after_mark && gc->garbage_bit != object->fields.is_garbage)
        {
            // Assert 3-color invariant.
            size_t object_slot_count = object->fields.slot_count;
            for (size_t slot = 0; slot < object_slot_count; ++slot)
            {
                ObjectHeader *slot_value = object_get_slot_impl(gc, object, slot);
                if (NULL != slot_value)
                {
                    check(gc->garbage_bit != slot_value->fields.is_garbage,
                          "Non-garbage points to garbage. Object: %p, garbage: %p, free: %p.",
                          object_pool_address,
                          (void *) ((void *) slot_value - gc->pool),
                          (void *) ((void *) gc->free - gc->pool));
                }
            }
        }

        if (gc->garbage_bit == object->fields.is_garbage)
        {
            check(!object->fields.is_root,
                  "Garbage root. Object: %p.",
                  object_pool_address);

            check(!object->fields.is_tracked,
                  "Garbage tracked. Object: %p.",
                  object_pool_address);
        }

        if (object->fields.is_root)
        {
            check(gc->garbage_bit != object->fields.is_garbage,
                  "Root garbage. Object: %p.",
                  object_pool_address);
        }

        check(object->fields.is_scanned == gc->scanned_bit,
              "Unscanned object: %p.",
              object_pool_address);

        #if defined(__x86_64__)
        check(0 == object->__pad__,
              "Probably not an object. Object: %p.",
              object_pool_address);
        #endif // defined(__x86_64__).

        check(0 != object->fields.total_size || gc->garbage_bit == object->fields.is_garbage,
              "Bad object size. Object: %p.",
              object_pool_address);

        if (gc->garbage_bit == object->fields.is_garbage && !asserting_after_mark)
        {
            check(0 == object->fields.slot_count && 0 == object->fields.total_size,
                  "Bad fill object: %p. Slot count: %u, total_size: %u.",
                  object_pool_address,
                  object->fields.slot_count,
                  object->fields.total_size);
        }

        if (gc->garbage_bit != object->fields.is_garbage)
        {
            size_t object_slot_count = object->fields.slot_count;
            for (size_t slot = 0; slot < object_slot_count; ++slot)
            {
                object_get_slot_impl(gc, object, slot);
            }
        }

        if (asserting_after_collect)
        {
            check(gc->garbage_bit != object->fields.is_garbage,
                  "Garbage after collect. Object: %p.",
                  object_pool_address);
        }

        ObjectHeader *previous_object = NULL;
        if (NULL != previous_pointer)
        {
            previous_object = (ObjectHeader *) previous_pointer;
        }

        if (gc->garbage_bit == object->fields.is_garbage && !asserting_after_mark)
        {
            check(0 == object->fields.slot_count,
                  "F objects should have 0 slots. Object: %p.",
                  object_pool_address);

            if (NULL != previous_object)
            {
                check(gc->garbage_bit != previous_object->fields.is_garbage,
                      "Garbage before garbage detected. Previous: %p, current: %p.",
                      (void *) (previous_pointer - gc->pool),
                      object_pool_address);
            }
        }

        previous_pointer = scan_pointer;
        scan_pointer += gc_object_get_size_exclusive(gc, object) * OBJECT_SIZE_UNIT;
    }

    if (asserting_after_collect)
    {
        // Assert that all tracked external references point to existing live objects.
        size_t tracked_reference_count = dynamic_array_count(gc->tracked_references);
        if (0 != tracked_reference_count)
        {
            for (ExternalReference *tracked_reference_record = dynamic_array_get_raw_data(gc->tracked_references),
                                   *tracked_references_end   = tracked_reference_record + tracked_reference_count;
                 tracked_references_end != tracked_reference_record;
                 ++tracked_reference_record)
            {
                if (NULL == tracked_reference_record->reference ||
                    NULL == *(tracked_reference_record->reference))
                {
                    continue;
                }

                bool object_found = false;
                for (void *scan_pointer     = gc->pool;
                     scan_pointer < (void *) gc->free;)
                {
                    ObjectHeader *object = (ObjectHeader *) scan_pointer;
                    if (object == *(tracked_reference_record->reference))
                    {
                        assert(gc->garbage_bit != object->fields.is_garbage && "Tracked reference points to garbage object.");
                        object_found = true;
                        break;
                    }
                    scan_pointer += gc_object_get_size_exclusive(gc, object) * OBJECT_SIZE_UNIT;
                }

                if (!object_found)
                {
                    assert(false && "Tracked object not found in heap.");
                }
            }
        }
    }

    return;
    error:
    assert(false);
}
#endif // !defined(NO_DEBUG).

// Function for VM. Notifies GC that new VM thread was born.
void gc_vm_register_thread(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    atomic_fetch_add(&gc->vm_thread_count, 1);
}

// Function for VM. Notifies GC that VM thread died.
void gc_vm_unregister_thread(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    assert(atomic_load(&gc->vm_thread_count));
    atomic_fetch_sub(&gc->vm_thread_count, 1);
}

// Function for VM. Notifies GC that existing VM thread awoke.
void gc_vm_thread_awoke(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
    while (!atomic_load(&gc->vm_working))
    {
        debug("Waiting for collect_completed_event.");
        check_return(pthread_cond_wait(&gc->collect_completed_event, &gc->vm_mutex), "pthread_cond_wait() failed.");
        debug("Got collect_completed_event.");
    }

    if (0 == atomic_fetch_add(&gc->vm_running_thread_count, 1))
    { // It's the first thread to wake up.
        check_return(pthread_cond_signal(&gc->vm_is_running_event), "pthread_cond_signal() failed.");
        debug("Sent vm_is_running signal.");
    }
    check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
    check(0 == sched_yield(), "sched_yield() failed.");
    return;
    error:
    abort();
}

// Function for VM. Notifies GC that existing VM thread went to sleep.
void gc_vm_thread_fall_asleep(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    check_return(tracing_mutex_lock(&gc->vm_mutex, ""), "pthread_mutex_lock() failed.");
    assert(atomic_load(&gc->vm_running_thread_count));
    if (1 == atomic_fetch_sub(&gc->vm_running_thread_count, 1))
    { // It's the last running thread.
        check_return(pthread_cond_signal(&gc->vm_is_sleeping_event), "pthread_cond_signal() failed.");
        debug("Sent vm_is_sleeping signal.");
    }
    check_return(tracing_mutex_unlock(&gc->vm_mutex, ""), "pthread_mutex_unlock() failed.");
    check(0 == sched_yield(), "sched_yield() failed.");
    return;
    error:
    abort();
}

// Is collection requested by GC.
bool gc_is_collection_needed(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return !atomic_load(&gc->vm_working);
}

// Helper function to determine whether all VM threads are sleeping.
bool gc_vm_is_sleeping(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return 0 == atomic_load(&gc->vm_running_thread_count);
}

// Helper function to determine whether VM waits for GC to collect.
bool gc_vm_is_waiting(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return 0 != atomic_load(&gc->vm_waiting_thread_count);
}

// Helper function to determine whether VM is still active.
bool gc_vm_is_active(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    return 0 != atomic_load(&gc->vm_thread_count);
}

// Returns pointer to GCStatistics struct for given GC.
GCStatistics *gc_get_statistics(GC *gc)
{
    assert(gc && "Bad GC pointer.");

    gc->statistics.last_collection_used_ratio =
        (double) gc->statistics.last_collection_used_size / (double) gc->statistics.pool_size;

    gc->statistics.last_collection_garbage_ratio =
        (double) gc->statistics.last_collection_garbage_size / (double) gc->statistics.pool_size;

    gc->statistics.last_collected_ratio =
        (double) gc->statistics.last_collected_size / (double) gc->statistics.pool_size;

    gc->statistics.peak_usage_ratio =
        (double) gc->statistics.peak_usage_size / (double) gc->statistics.pool_size;

    gc->statistics.current_usage_size = gc_get_used_size(gc);

    gc->statistics.current_usage_ratio =
        (double) gc->statistics.current_usage_size / (double) gc->statistics.pool_size;

    return &gc->statistics;
}

// Enables or disables parallel marking. Returns previous value.
bool gc_set_parallel_marking(GC *gc, bool enable_parallel_marking)
{
    assert(gc && "Bad GC pointer.");
    bool old_value = gc->is_parallel_marking_enabled;
    gc->is_parallel_marking_enabled = enable_parallel_marking;
    return old_value;
}

// Gets protection bit for a given page.
bool gc_get_page_protection_bit(GC *gc, void *page)
{
    assert(gc && "Bad GC pointer.");
    assert(page >= gc->pool && page < (gc->pool + gc->pool_size) && "Page not in a pool.");

    size_t page_index = (size_t) page;
    page_index -= page_index % gc->page_size;
    page_index -= (size_t) gc->pool;
    page_index /= gc->page_size;

    size_t page_byte_index = page_index / CHAR_BIT,
           page_bit_index  = page_index % CHAR_BIT;
    char page_bitmask = 1 << page_bit_index;
    return 0 != (((char *) gc->page_protection_flags)[page_byte_index] & page_bitmask);
}

// Sets protection bit for a given page.
void gc_set_page_protection_bit(GC *gc, void *page, bool new_value)
{
    assert(gc && "Bad GC pointer.");
    assert(page >= gc->pool && page < (gc->pool + gc->pool_size) && "Page not in a pool.");

    size_t page_index = (size_t) page;
    page_index -= page_index % gc->page_size;
    page_index -= (size_t) gc->pool;
    page_index /= gc->page_size;

    size_t page_byte_index = page_index / CHAR_BIT,
           page_bit_index  = page_index % CHAR_BIT;
    char page_bitmask = 1 << page_bit_index;
    if (new_value)
    {
        ((char *) gc->page_protection_flags)[page_byte_index] |= page_bitmask;
    }
    else
    {
        ((char *) gc->page_protection_flags)[page_byte_index] &= ~page_bitmask;
    }
}

// Clears all page protection bits.
void gc_clear_memory_protection_bits(GC *gc)
{
    assert(gc && "Bad GC pointer.");
    size_t page_protection_flags_length = (gc->pool_page_count + CHAR_BIT - gc->pool_page_count % CHAR_BIT) * sizeof(char);
    memset(gc->page_protection_flags, 0, page_protection_flags_length);
}
