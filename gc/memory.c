// memory.c - object access.

#include <assert.h>
#include <stdlib.h>

#include "debug.h"

#define GC_EXPORT_INTERNALS
#include "gc.h"

// Obtains content of given slot of the object.
#if defined(NO_DEBUG)
#undef object_get_slot
inline ObjectHeader *object_get_slot(ObjectHeader *o, size_t slot_index)
#else
inline ObjectHeader *object_get_slot(GC *gc, ObjectHeader *o, size_t slot_index)
#endif // !defined(NO_DEBUG).
{
    return object_get_slot_impl(gc, o, slot_index);
}

// Sets given slot of the object to new value.
inline ObjectHeader *object_set_slot(GC *gc, ObjectHeader *o, size_t slot_index, ObjectHeader *value)
{
    assert(o && "Bad object pointer.");
    assert(slot_index < gc_object_get_header_field(o, OBJECT_HEADER_FIELD_SLOT_COUNT) && "Bad slot index.");
    #if !defined(NO_DEBUG)
    ObjectHeader *heap_used_part_boudary = gc_get_pool(gc) + gc_get_used_size(gc);
    #endif // !defined(NO_DEBUG).
    assert((NULL == value ||
            (((void *) value >= gc_get_pool(gc)) && (value < heap_used_part_boudary))) &&
           "Bad slot value.");
    assert(0 == ((size_t) value) % OBJECT_SLOT_SIZE && "Bad slot value.");
    assert(o < heap_used_part_boudary && "Object is in freed part of the heap.");
    assert(value < heap_used_part_boudary && "Object is in freed part of the heap.");

    ObjectHeader **slot = ((void *) o) + OBJECT_SLOT_SIZE + slot_index * OBJECT_SLOT_SIZE;
    if (*slot != value)
    {
        gc_notify_object_modified(gc, o, slot, value);
    }
    return value;
}

// Gets pointer to given byte of raw data of the object.
inline void *object_get_byte(ObjectHeader *o, size_t byte_index)
{
    assert(o && "Bad object pointer.");
    #if !defined(NO_DEBUG)
    size_t raw_data_size = gc_object_get_header_field(o, OBJECT_HEADER_FIELD_TOTAL_SIZE) * OBJECT_SIZE_UNIT -
                           OBJECT_SLOT_SIZE -
                           gc_object_get_header_field(o, OBJECT_HEADER_FIELD_SLOT_COUNT) * OBJECT_SLOT_SIZE;
    #endif // !defined(NO_DEBUG).
    assert(byte_index < raw_data_size && "Bad byte index.");

    return ((void *) o) +
           OBJECT_SLOT_SIZE +
           gc_object_get_header_field(o, OBJECT_HEADER_FIELD_SLOT_COUNT) * OBJECT_SLOT_SIZE +
           byte_index;
}

// Sets all slots of the object to the given value.
inline ObjectHeader *object_fill_slots(GC *gc, ObjectHeader *o, ObjectHeader *value)
{
    assert(o && "Bad object pointer.");
    size_t object_slot_count = gc_object_get_header_field(o, OBJECT_HEADER_FIELD_SLOT_COUNT);
    for (size_t i = 0; i < object_slot_count; ++i)
    {
        object_set_slot(gc, o, i, value);
    }
    return o;
}
