// dynamic_array.c - dynamic arrays support.

#include <assert.h>
#include <stdlib.h>

#include "debug.h"
#include "dynamic_array.h"

typedef struct DynamicArray
{
    size_t element_size;
    size_t element_count;
    size_t array_capacity;
    void *data;
} DynamicArray;

#if !defined(NO_DEBUG)
static void __dynamic_array_assert(const DynamicArray *a);
#else
#define __dynamic_array_assert(a)
#endif // !defined(NO_DEBUG).

DynamicArray *dynamic_array_create(size_t element_size, size_t array_capacity)
{
    assert(element_size > 0 && "Bad element_size.");
    // assert(array_capacity >= 0 && "Bad array_capacity.");

    DynamicArray *a = (DynamicArray *) calloc(1, sizeof(DynamicArray));
    check_mem(a);

    a->data = calloc(array_capacity, element_size);
    check_mem(a->data);

    a->element_size = element_size;
    a->element_count = 0;
    a->array_capacity = array_capacity;

    return a;

    error:
    if (a && a->data)
    {
        free(a->data);
    }
    if (a)
    {
        free(a);
    }
    return NULL;
}

void dynamic_array_destroy(DynamicArray *a)
{
    assert(a && "Nothing to destroy.");
    assert(a->data && "Nothing to destroy.");
    free(a->data);
    free(a);
}

void *dynamic_array_get(const DynamicArray *a, size_t i)
{
    __dynamic_array_assert(a);
    // assert(i >= 0 && i < a->array_capacity && "Index out of range.");
    assert(i < a->array_capacity && "Index out of range.");
    return &(((char *) a->data)[i * a->element_size]);
}

const void *dynamic_array_set(DynamicArray *a, size_t i, const void *data)
{
    __dynamic_array_assert(a);
    // assert(i >= 0 && i < a->array_capacity && "Index out of range.");
    assert(i < a->array_capacity && "Index out of range.");
    assert(data && "Bad data pointer.");
    memcpy(a->data + i * a->element_size, data, a->element_size);
    return data;
}

bool dynamic_array_push(DynamicArray *a, const void *data)
{
    __dynamic_array_assert(a);
    assert(data && "Bad data pointer.");

    if (a->element_count < a->array_capacity)
    {
        dynamic_array_set(a, a->element_count++, data);
        return true;
    }

    void *new_data = realloc(a->data, (1 + a->element_count) * a->element_size);
    check_mem(new_data);

    a->data = new_data;
    a->array_capacity++;
    dynamic_array_set(a, a->element_count++, data);
    return true;

    error:
    return false;
}

void *dynamic_array_pop(DynamicArray *a)
{
    __dynamic_array_assert(a);
    assert(a->element_count && "Nothing to pop.");

    return &(((char *) a->data)[--a->element_count * a->element_size]);
}

bool dynamic_array_insert_at(DynamicArray *a, size_t i, const void *data)
{
    __dynamic_array_assert(a);
    // assert(i >= 0 && i <= a->element_count && "Index out of range.");
    assert(i <= a->element_count && "Index out of range.");
    assert(data && "Bad data pointer.");

    if (i == a->element_count)
    {
        return dynamic_array_push(a, data);
    }

    if (a->element_count < a->array_capacity)
    {
        memmove(a->data + i * a->element_size,
                a->data + (i + 1) * a->element_size,
                (a->element_count - i) * a->element_size);
        dynamic_array_set(a, i, data);
        a->element_count++;
        return true;
    }

    void *new_data = realloc(a->data, (1 + a->element_count) * a->element_size);
    check_mem(new_data);

    a->data = new_data;

    memmove(a->data + i * a->element_size,
            a->data + (i + 1) * a->element_size,
            (a->element_count - i) * a->element_size);
    dynamic_array_set(a, i, data);

    a->array_capacity++;
    a->element_count++;
    return true;

    error:
    return false;
}

void dynamic_array_delete_at(DynamicArray *a, size_t i)
{
    __dynamic_array_assert(a);
    // assert(i >= 0 && i < a->element_count && "Index out of range.");
    assert(i < a->element_count && "Index out of range.");
    a->element_count--;
    if (i == a->element_count)
    {
        memset(a->data + i * a->element_size, 0, a->element_size);
        return;
    }

    memmove(a->data + i * a->element_size, a->data + (i + 1) * a->element_size, (a->element_count - i) * a->element_size);
    memset(a->data + a->element_count * a->element_size, 0, a->element_size);
}

void dynamic_array_delete_at_position(DynamicArray *a, void *position)
{
    __dynamic_array_assert(a);
    assert(a->data <= position && position < (a->data + a->element_count * a->element_size) && "Position out of range.");
    size_t i = (position - a->data) / a->element_size;
    dynamic_array_delete_at(a, i);
}

void dynamic_array_clear(DynamicArray *a)
{
    __dynamic_array_assert(a);
    a->element_count = 0;
}

void *dynamic_array_get_raw_data(const DynamicArray *a)
{
    __dynamic_array_assert(a);
    return a->data;
}

size_t dynamic_array_count(const DynamicArray *a)
{
    __dynamic_array_assert(a);
    return a->element_count;
}

#if !defined(NO_DEBUG)
static void __dynamic_array_assert(const DynamicArray *a)
{
    assert(a && "Bad array.");
    assert(a->data && "Bad array.");
    assert(a->element_size && "Bad array.");
}
#endif // !defined(NO_DEBUG).
