// dynamic_array.h - dynamic arrays support.

#pragma once

#include <stdbool.h>

typedef struct DynamicArray DynamicArray;

DynamicArray *dynamic_array_create(size_t element_size, size_t element_count);
#define DYNAMIC_ARRAY_CREATE(element_type, element_count) dynamic_array_create(sizeof(element_type), (element_count))
void dynamic_array_destroy(DynamicArray *a);
void *dynamic_array_get(const DynamicArray *a, size_t i);
#define DYNAMIC_ARRAY_GET(element_type, a, i) (element_type dynamic_array_get((a), (i)))
const void *dynamic_array_set(DynamicArray *a, size_t i, const void *data);
bool dynamic_array_push(DynamicArray *a, const void *data);
void *dynamic_array_pop(DynamicArray *a);
#define DYNAMIC_ARRAY_POP(element_type, a) (element_type dynamic_array_pop(a))
bool dynamic_array_insert_at(DynamicArray *a, size_t i, const void *data);
void dynamic_array_delete_at(DynamicArray *a, size_t i);
void dynamic_array_delete_at_position(DynamicArray *a, void *position);
void dynamic_array_clear(DynamicArray *a);
void *dynamic_array_get_raw_data(const DynamicArray *a);
size_t dynamic_array_count(const DynamicArray *a);
