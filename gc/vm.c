// vm.c - simulates VM behaviour.

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

#include "debug.h"
#include "gc.h"

void *vm_thread(void *is_main_thread);

#if defined(SYNCHRONIZED_TRACING)
pthread_mutex_t tracing_lock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
#endif // defined(SYNCHRONIZED_TRACING).

GC *gc = NULL;

ObjectHeader *root = NULL;
const size_t root_slot_count = 16;

int main()
{
    bool use_parallel_marking = NULL == getenv("GC_NO_PARALLEL_MARKING");
    gc = gc_create(16, use_parallel_marking);
    gc_start(gc);
    gc_set_notification_signal(gc);

    srand(time(NULL));

    gc_vm_register_thread(gc);
    gc_vm_thread_awoke(gc);
    root = gc_alloc(gc, true, 0, root_slot_count, 0);
    gc_track_reference(gc, &root, REFERENCE_TRACKING_MODE_WEAK);

    const size_t vm_thread_count = 4;
    pthread_t vm_threads[vm_thread_count];
    for (size_t i = 0; i < vm_thread_count; ++i)
    {
        check_return(pthread_create(&vm_threads[i], NULL, vm_thread, NULL), "pthread_create() failed.");
    }

    vm_thread((void *) true);

    debug("Waiting for other threads started.");
    gc_vm_thread_fall_asleep(gc);
    for (size_t i = 0; i < vm_thread_count; ++i)
    {
        check_return(pthread_join(vm_threads[i], NULL), "pthread_join() failed.");
    }
    gc_vm_thread_awoke(gc);
    debug("Waiting for other threads finished.");

    gc_untrack_reference(gc, &root);
    gc_update_object_header(gc, root, 1, OBJECT_HEADER_FIELD_IS_ROOT, (unsigned) false);
    debug("VM full stop start.");
    while (0 != gc_get_used_size(gc))
    {
        gc_wait_for_collect_completed(gc, false);
    }
    gc_vm_thread_fall_asleep(gc);
    gc_vm_unregister_thread(gc);
    debug("VM full stop end.");
    gc_unset_notification_signal(gc);
    gc_stop(gc);
    gc_free(gc);

    #if defined(SYNCHRONIZED_TRACING)
    check(0 == pthread_mutex_destroy(&tracing_lock), "pthread_mutex_destroy() failed.");
    #endif // defined(SYNCHRONIZED_TRACING).
    return 0;

    error:
    abort();
    return -1;
}

void *vm_thread(void *is_main_thread)
{
    if (!is_main_thread)
    {
        gc_vm_register_thread(gc);
        gc_vm_thread_awoke(gc);
    }

    gc_wait_for_collect_completed(gc, true);

    for (size_t i               = 0,
                root_slot_index = 0;
         i < 16384;
         ++i)
    {
        ObjectHeader *object1 = gc_alloc(gc, false, 0, rand() % 4 + 1, 0);
        gc_track_reference(gc, &object1, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
        object_set_slot(gc, root, root_slot_index, object1);
        root_slot_index = (root_slot_index + 1) % root_slot_count;
        ObjectHeader *object2 = gc_alloc(gc, false, 0, rand() % 4 + 1, 0);
        gc_track_reference(gc, &object2, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
        object_set_slot(gc, object1, 0, object2);
        gc_untrack_reference(gc, &object1);
        gc_untrack_reference(gc, &object2);

        ObjectHeader *not_garbage_object = NULL,
                     *garbage_object     = NULL;
        for (void *scan_pointer = gc_get_pool(gc);
             scan_pointer < (void *) (gc_get_pool(gc) + gc_get_used_size(gc));)
        {
            ObjectHeader *object = (ObjectHeader *) scan_pointer;
            size_t object_size = gc_object_get_size_in_bytes(gc, object);
            assert(gc_object_get_header_field(object, OBJECT_HEADER_FIELD_TOTAL_SIZE));

            if (root != object)
            {
                if (!gc_object_get_header_field(object, OBJECT_HEADER_FIELD_IS_GARBAGE) && NULL == not_garbage_object)
                {
                    not_garbage_object = object;
                    gc_track_reference(gc, &not_garbage_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
                }
                else if (NULL == garbage_object)
                {
                    garbage_object = object;
                    gc_track_reference(gc, &garbage_object, REFERENCE_TRACKING_MODE_SINGLE_OBJECT);
                }
            }

            if (NULL != not_garbage_object && NULL != garbage_object)
            {
                break;
            }

            scan_pointer += object_size;
        }

        if (NULL != not_garbage_object && NULL != garbage_object)
        {
            object_set_slot(gc, not_garbage_object, 0, garbage_object);
        }

        if (NULL != not_garbage_object)
        {
            gc_untrack_reference(gc, &not_garbage_object);
        }

        if (NULL != garbage_object)
        {
            gc_untrack_reference(gc, &garbage_object);
        }

        gc_wait_for_collect_completed(gc, true);
    }

    if (!is_main_thread)
    {
        gc_vm_thread_fall_asleep(gc);
        gc_vm_unregister_thread(gc);
    }
    debug("VM thread stopped.");
    return NULL;
}

uint64_t pthread_getthreadid(pthread_t *arg)
{
    pthread_t thread = arg ? *arg : pthread_self();
    uint64_t result = *((uint64_t *) (&thread));
    return result;
}
