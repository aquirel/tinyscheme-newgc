// debug.h - error handling facilities.

#pragma once

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include <pthread.h>

uint64_t pthread_getthreadid(pthread_t *arg);

#if defined(SYNCHRONIZED_TRACING)
#include <assert.h>

extern pthread_mutex_t tracing_lock;

#define TRACING_LOCK assert(0 == pthread_mutex_lock(&tracing_lock))
#define TRACING_UNLOCK assert(0 == pthread_mutex_unlock(&tracing_lock))

#else

#define TRACING_LOCK
#define TRACING_UNLOCK

#endif // defined(SYNCHRONIZED_TRACING).

#if defined(NO_DEBUG)
#define debug(MESSAGE, ...)
#else
#define debug(MESSAGE, ...) \
do { \
    TRACING_LOCK; \
    fprintf(stderr, "[DEBUG] [%" PRIx64 "] (%s at %s:%d): " MESSAGE "\n", pthread_getthreadid(NULL), __func__, __FILE__, __LINE__, ## __VA_ARGS__); \
    TRACING_UNLOCK; \
} while(0)
#endif // defined(NO_DEBUG).

#define clean_errno() (0 == errno ? "None" : strerror(errno))
#define log_error(MESSAGE, ...) \
do { \
    TRACING_LOCK; \
    fprintf(stderr, "[ERROR] [%" PRIx64 "] (%s at %s:%d: errno: %s): " MESSAGE "\n", pthread_getthreadid(NULL), __func__, __FILE__, __LINE__, clean_errno(), ## __VA_ARGS__); \
    TRACING_UNLOCK; \
} while(0)
#define log_warning(MESSAGE, ...) \
do { \
    TRACING_LOCK; \
    fprintf(stderr, "[WARN] [%" PRIx64 "] (%s at %s:%d: errno: %s): " MESSAGE "\n", pthread_getthreadid(NULL), __func__, __FILE__, __LINE__, clean_errno(), ## __VA_ARGS__); \
    TRACING_UNLOCK; \
} while(0)
#define log_info(MESSAGE, ...) \
do { \
    TRACING_LOCK; \
    fprintf(stderr, "[INFO] [%" PRIx64 "] (%s at %s:%d): " MESSAGE "\n", pthread_getthreadid(NULL), __func__, __FILE__, __LINE__, ## __VA_ARGS__); \
    TRACING_UNLOCK; \
} while(0)
#define check(CONDITION, MESSAGE, ...) do { if (!(CONDITION)) { log_error(MESSAGE, ## __VA_ARGS__); errno = 0; goto error; } } while(0)
#define sentinel(MESSAGE, ...) do { log_error(MESSAGE, ## __VA_ARGS__); errno = 0; goto error; } while (0)
#define check_mem(CONDITION) check((CONDITION), "Out of memory.")
#define check_debug(CONDITION, MESSAGE, ...) do { if (!(CONDITION)) { debug(MESSAGE, ## __VA_ARGS__); errno = 0; goto error; } } while(0)
#define check_return(CONDITION, MESSAGE, ...) do { { errno = (CONDITION); check(0 == errno, MESSAGE, ## __VA_ARGS__); } } while(0)

#if defined(MUTEX_TRACING)
#define tracing_mutex_lock(MUTEX, MESSAGE, ...) \
({ \
    if ('\0' != *(MESSAGE)) \
    { \
        log_info("Locking " MESSAGE, ## __VA_ARGS__); \
    } \
    else \
    { \
        log_info("Locking " #MUTEX "."); \
    } \
    int result = pthread_mutex_lock(MUTEX); \
    if ('\0' != *(MESSAGE)) \
    { \
        log_info("Locked " MESSAGE, ## __VA_ARGS__); \
    } \
    else \
    { \
        log_info("Locked " #MUTEX "."); \
    } \
    result; \
})

#define tracing_mutex_trylock(MUTEX, MESSAGE, ...) \
({ \
    if ('\0' != *(MESSAGE)) \
    { \
        log_info("Try locking " MESSAGE, ## __VA_ARGS__); \
    } \
    else \
    { \
        log_info("Try locking " #MUTEX "."); \
    } \
    int result = pthread_mutex_trylock(MUTEX); \
    if (EBUSY != result) \
    { \
        if ('\0' != *(MESSAGE)) \
        { \
            log_info("Try locked " MESSAGE, ## __VA_ARGS__); \
        } \
        else \
        { \
            log_info("Try locked " #MUTEX "."); \
        } \
    } \
    else \
    { \
        if ('\0' != *(MESSAGE)) \
        { \
            log_info("Try lock busy " MESSAGE, ## __VA_ARGS__); \
        } \
        else \
        { \
            log_info("Try lock busy " #MUTEX "."); \
        } \
    } \
    result; \
})

#define tracing_mutex_unlock(MUTEX, MESSAGE, ...) \
({ \
    int result = pthread_mutex_unlock(MUTEX); \
    if ('\0' != *(MESSAGE)) \
    { \
        log_info("Unlocked " MESSAGE, ## __VA_ARGS__); \
    } \
    else \
    { \
        log_info("Unlocked " #MUTEX "."); \
    } \
    result; \
})

#else

#define tracing_mutex_lock(MUTEX, MESSAGE, ...) pthread_mutex_lock(MUTEX)
#define tracing_mutex_trylock(MUTEX, MESSAGE, ...) pthread_mutex_trylock(MUTEX)
#define tracing_mutex_unlock(MUTEX, MESSAGE, ...) pthread_mutex_unlock(MUTEX)

#endif // defined(MUTEX_TRACING).
