/* scheme-private.h */

#ifndef _SCHEME_PRIVATE_H
#define _SCHEME_PRIVATE_H

#include "scheme.h"
#include <pthread.h>

#include "gc/dynamic_array.h"

/* Ugly internals.
 * Of interest only to FFI users.
 */

#ifdef __cplusplus
extern "C"
{
#endif

enum scheme_port_kind
{
    port_free    = 0,
    port_file    = 1,
    port_string  = 2,
    port_srfi6   = 4,
    port_input   = 16,
    port_output  = 32,
    port_saw_EOF = 64
};

typedef struct port
{
    unsigned char kind;
    union
    {
        struct
        {
            FILE *file;
            int closeit;

            #if SHOW_ERROR_LINE
            int curr_line;
            char *filename;
            #endif
        } stdio;

        struct
        {
            char *start;
            char *past_the_end;
            char *curr;
        } string;
    } rep;
} port;

// Managed part of scheme object.
/* We use 4 registers. */
#define SCHEME_ARGS              0  /* register for arguments of function */
#define SCHEME_ENVIR             1  /* stack register for current environment */
#define SCHEME_CODE              2  /* register for current code */
#define SCHEME_DUMP              3  /* stack register for next evaluation */

#define SCHEME_NIL               4  /* special cell representing empty cell */
#define SCHEME_HASH_T            5  /* special cell representing #t */
#define SCHEME_HASH_F            6  /* special cell representing #f */
#define SCHEME_EOF_OBJ           7  /* special cell representing end-of-file object */

#define SCHEME_OBLIST            8  /* pointer to symbol table */
#define SCHEME_GLOBAL_ENV        9  /* pointer to global environment */
#define SCHEME_C_NEST            10 /* stack for nested calls from C */

/* global pointers to special symbols */
#define SCHEME_LAMBDA            11 /* pointer to syntax lambda */
#define SCHEME_QUOTE             12 /* pointer to syntax quote */
#define SCHEME_QQUOTE            13 /* pointer to symbol quasiquote */
#define SCHEME_UNQUOTE           14 /* pointer to symbol unquote */
#define SCHEME_UNQUOTESP         15 /* pointer to symbol unquote-splicing */
#define SCHEME_FEED_TO           16 /* => */
#define SCHEME_COLON_HOOK        17 /* *colon-hook* */
#define SCHEME_ERROR_HOOK        18 /* *error-hook* */
#define SCHEME_SHARP_HOOK        19 /* *sharp-hook* */
#define SCHEME_COMPILE_HOOK      20 /* *compile-hook* */
#define SCHEME_INPORT            21
#define SCHEME_OUTPORT           22
#define SCHEME_SAVE_INPORT       23
#define SCHEME_LOADPORT          24
#define SCHEME_VALUE             25

#define SCHEME_OBJECT_SLOT_COUNT 26

// Raw part of scheme object.
struct scheme
{
    GC *gc;

    func_alloc malloc;
    func_dealloc free;

    /* return code */
    int retcode;
    int tracing;
    int interactive_repl;   /* are we in an interactive REPL? */

    #define MAXFIL 64
    port load_stack[MAXFIL];    /* Stack of open files for port -1 (LOADing) */
    int nesting_stack[MAXFIL];
    int file_i;
    int nesting;

    char gc_verbose;        /* if gc_verbose is not zero, print gc status */
    char no_memory;         /* Whether mem. alloc. has failed */

    #define LINESIZE 1024
    char linebuff[LINESIZE];
    #define STRBUFFSIZE 256
    char strbuff[STRBUFFSIZE];

    FILE *tmpfp;
    int tok;
    int print_flag;
    int op;

    void *ext_data;         /* For the benefit of foreign functions */
    long gensym_cnt;

    struct scheme_interface *vptr;

    pthread_t thread;
    bool is_working;
    bool was_joined;

    struct scheme *global_scheme;
    pthread_mutex_t global_lock;

    DynamicArray *vm_threads; // Contains pointers to other VM threads.
};

/* operator code */
enum scheme_opcodes
{
    #define _OP_DEF(A,B,C,D,E,OP) OP,
    #include "opdefines.h"
    OP_MAXDEFINED
};

#if defined(NO_DEBUG)
int is_string(pointer p); // Safe.
#define is_string(gc, p) is_string(p)
#else
int is_string(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

char *string_value(pointer p);

#if defined(NO_DEBUG)
int is_number(pointer p); // Safe.
#define is_number(gc, p) is_number(p)
#else
int is_number(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

num nvalue(pointer p);

#if defined(NO_DEBUG)
long ivalue(pointer p);
#define ivalue(gc, p) ivalue(p)
double rvalue(pointer p);
#define rvalue(gc, p) rvalue(p)
int is_integer(pointer p);
#define is_integer(gc, p) is_integer(p)
int is_real(pointer p);
#define is_real(gc, p) is_real(p)
#else
int is_integer(GC *gc, pointer p);
int is_real(GC *gc, pointer p);
long ivalue(GC *gc, pointer p);
double rvalue(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
int is_character(pointer p); // Safe.
#define is_character(gc, p) is_character(p)
#else
int is_character(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

long charvalue(pointer p);

#if defined(NO_DEBUG)
int is_vector(pointer p); // Safe.
#define is_vector(gc, p) is_vector(p)
#else
int is_vector(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
int is_port(pointer p); // Safe.
#define is_port(gc, p) is_port(p)
#else
int is_port(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
int is_pair(pointer p); // Safe.
#define is_pair(gc, p) is_pair(p)
#else
int is_pair(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
pointer pair_car(pointer p);
#define pair_car(gc, p) pair_car(p)
#else
pointer pair_car(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
pointer pair_cdr(pointer p);
#define pair_cdr(gc, p) pair_cdr(p)
#else
pointer pair_cdr(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

pointer set_car(GC *gc, pointer p, pointer q);
pointer set_cdr(GC *gc, pointer p, pointer q);

#if defined(NO_DEBUG)
int is_symbol(pointer p); // Safe.
#define is_symbol(gc, p) is_symbol(p)
#else
int is_symbol(GC *gc, pointer p); // Safe.
#endif // defined(NO_DEBUG).

#if defined(NO_DEBUG)
char *symname(pointer p);
#define symname(gc, p) symname(gc)
#else
char *symname(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

int hasprop(pointer p);

int is_syntax(pointer p);
int is_proc(pointer p);
int is_foreign(pointer p);

#if defined(NO_DEBUG)
char *syntaxname(pointer p);
#define syntaxname(gc, p) syntaxname(p)
#else
char *syntaxname(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

int is_closure(pointer p);
#ifdef USE_MACRO
int is_macro(pointer p);
#endif

#if defined(NO_DEBUG)
pointer closure_code(pointer p);
pointer closure_env(pointer p);
#define closure_code(gc, p) closure_code(p)
#define closure_env(gc, p) closure_env(p)
#else
pointer closure_code(GC *gc, pointer p);
pointer closure_env(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

int is_continuation(pointer p);
int is_promise(pointer p);

#if defined(NO_DEBUG)
int is_environment(pointer p);
#define is_environment(gc, p) is_environment(p)
#else
int is_environment(GC *gc, pointer p);
#endif // defined(NO_DEBUG).

int is_immutable(pointer p);
void setimmutable(GC *gc, pointer p);

#ifdef __cplusplus
}
#endif
#endif
